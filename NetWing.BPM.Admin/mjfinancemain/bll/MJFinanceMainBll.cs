using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJFinanceMainBll
    {
        public static MJFinanceMainBll Instance
        {
            get { return SingletonProvider<MJFinanceMainBll>.Instance; }
        }

        public int Add(MJFinanceMainModel model)
        {
            return MJFinanceMainDal.Instance.Insert(model);
        }

        public int Update(MJFinanceMainModel model)
        {
            return MJFinanceMainDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJFinanceMainDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJFinanceMainDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
