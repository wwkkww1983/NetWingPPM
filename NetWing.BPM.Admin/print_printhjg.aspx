﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_printhjg.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.print_printhjg" %>

<!DOCTYPE html>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>聚源达后加工单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            //{
            //    name: '导出Excel',
            //    callback: function () {
            //        ajaxExcel();
            //        return false;
            //    }
            //},
            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="print_content">
             <!--全局变量-->
              
            <!--接件单名-->
            <p class="MsoNormal" align="center" style="text-align: center;">
                <img width="61" height="34" src="/JydWodrmb/img/dyt.png" /><b><span style="font-family: 宋体; font-size: 25pt;"><span>昆明聚源达彩印包装有限公司后加工单</span></span></b><b><span style="font-family: 宋体; font-size: 18pt;"></span></b>
            </p>
            <!--日期-->
            <p class="MsoNormal" align="center" style="text-align: center;">
                <b><span style="font-family: 宋体; font-size: 11pt;"><span>后加工场：</span></span></b>
                <b><u><span style="font-family: 宋体; font-size: 11pt;">&nbsp;<%=myws %></span></u></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b><span style="font-family: 宋体; font-size: 11pt;"><span>下单人：<%=myxd %></span><%--&nbsp;&nbsp;&nbsp;--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>后加工收货人：<%=mysh %></span> </b><br/>
                    
					<%if(myxdrq == null){%>
					
					<%}else{%>
             <b><span>开单日期：<%=DateTime.Parse(myxdrq).ToString("yyyy-MM-dd")%></span></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<%}%>
					
					<%if(myjhrq == null){%>
					
					<%}else{%>
					
                    <%if (!string.IsNullOrEmpty(myjhrq))
                        {%>
                    <b><span>交货日期：<%=DateTime.Parse(myjhrq).ToString("yyyy-MM-dd") %></span></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <%}else{%>
                    <b><span>交货日期：<%=myjhrq%></span></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <%} %>
					
					
					
					
					<%}%>
            </p>
          


            <div align="center">
                <table class="MsoTableGrid" border="1" cellspacing="0" style="border-collapse: collapse; width: 493.5500pt; border: none;">
                    <tbody>
                        <!--首行名称-->
                        <tr>
                            <td width="90" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 9pt;">印刷品名</span></b><b><span style="font-family: 宋体; font-size: 9pt;"></span></b>
                                </p>
                            </td>
                            <td width="98" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 9pt;">接件单号</span></b><b><span style="font-family: 宋体; font-size: 9pt;"></span></b>
                                </p>
                            </td>
                           
                            <td width="39" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 9pt;">加工尺寸</span></b><b><span style="font-family: 宋体; font-size: 9pt;"></span></b>
                                </p>
                            </td>
                             <td width="48" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 9pt;">加工数量</span></b><b><span style="font-family: 宋体; font-size: 9pt;"></span></b>
                                </p>
                            </td>
                            
                            <td width="64" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 9pt;">单价</span></b><b><span style="font-family: 宋体; font-size: 9pt;"></span></b>
                                </p>
                            </td>
                            <td width="44" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 9pt;">金额</span></b><b><span style="font-family: 宋体; font-size: 9pt;"></span></b>
                                </p>
                            </td>
                            
                            
                        </tr>
                        <!--数值-->

                         <%foreach (DataRow dr in dt.Rows)
                             { %>
                        <tr>
                             <td width="90" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 10.5pt;"><%=yspmc %></span></b>
                                </p>
                            </td>
                            <td width="98" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 10.5pt;"><%=orderid %></span></b>
                                </p>
                            </td>
                           
                            <td width="39" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 10.5pt;"><%=mycc %></span></b>
                                </p>
                            </td>
                             <td width="48" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 10.5pt;"><%=mynum %></span></b>
                                </p>
                            </td>
                           
                            <td width="64" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 10.5pt;"><%=mydj %></span></b>
                                </p>
                            </td>
                           <td width="44" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <b><span style="font-family: 宋体; font-size: 10.5pt;"><%=myysf %></span></b>
                                </p>
                            </td>
                             
                           
                        </tr>
                        <!--工艺-->
                       <%-- <tr>
                        </tr>--%>
                       <!--注释-->
                       
                         <%}%>

                       <!--合计-->
                        <tr>
                            <td width="618" valign="top" colspan="12" style="border: 1.0000pt solid windowtext;">

                                <p class="MsoNormal" align="justify" style="text-align: justify;">
                                    <span style="font-family: 宋体; font-size: 10.5pt;">金额：</span>
                                    <span style="font-family: 宋体; font-size: 10.5pt;"><%=myysf %>元</span>
                                    <span style="font-family: 宋体; font-size: 10.5pt;"></span>
                                </p>
                               
                                <p class="MsoNormal" align="justify" style="text-align: justify;">
                                    <span style="font-family: 宋体; font-size: 10.5pt;">加工项目：</span>
                                    <span style="font-family: 宋体; font-size: 10.5pt;"><%=myzzxm %></span>
                                    <span style="font-family: 宋体; font-size: 10.5pt;"></span>
                                </p>
                                <p class="MsoNormal" align="justify" style="text-align: justify;">
                                    <span style="font-family: 宋体; font-size: 10.5pt;">加工备注：</span>
                                    <span style="font-family: 宋体; font-size: 10.5pt;"><%=wsjgbz %></span>
                                    <span style="font-family: 宋体; font-size: 10.5pt;"></span>
                                </p>
                            </td>
                        </tr>
                       
                        
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
