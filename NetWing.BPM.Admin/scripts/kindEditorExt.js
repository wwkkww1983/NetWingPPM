﻿/**
* Author : ____′↘夏悸
* Easyui KindEditor的简单扩展.
* 有了这个之后,你就可以像使用Easyui组件的方式使用KindEditor了
* 前提是你需要导入KindEditor的核心js和相关样式. 本插件也需要在Easyui.min和KindEditor之后导入.
* 呵呵..没做深入扩展了,简单实现了一下功能,架子已经搭好.有需要的筒子可以在这基础上扩展.
**/
(function ($, K) {
    if (!K)
        throw "KindEditor未定义!";

    function create(target) {
        var opts = $.data(target, 'kindeditor').options;
        KindEditor.basePath = '/Editor/kindeditor-4.1.7/';
        var editor = K.create(target, opts);
        $.data(target, 'kindeditor').options.editor = editor;
    }

    $.fn.kindeditor = function (options, param) {
        if (typeof options == 'string') {
            var method = $.fn.kindeditor.methods[options];
            if (method) {
                return method(this, param);
            }
        }
        options = options || {};
        return this.each(function () {
            var state = $.data(this, 'kindeditor');
            if (state) {
                $.extend(state.options, options);
            } else {
                state = $.data(this, 'kindeditor', {
                    options: $.extend({}, $.fn.kindeditor.defaults, $.fn.kindeditor.parseOptions(this), options)
                });
            }
            create(this);
        });
    }

    $.fn.kindeditor.parseOptions = function (target) {
        return $.extend({}, $.parser.parseOptions(target, []));
    };

    $.fn.kindeditor.methods = {
        editor: function (jq) {
            return $.data(jq[0], 'kindeditor').options.editor;
        }
    };

    $.fn.kindeditor.defaults = {
        resizeType: 1,
        allowPreviewEmoticons: false,
        allowImageUpload: true,
        uploadJson: '/Editor/kindeditor-4.1.7/asp.net/upload_json.ashx',
        fileManagerJson: '/Editor/kindeditor-4.1.7/asp.net/file_manager_json.ashx',
        items: [
			'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'image', 'link'],
        afterBlur: function () {
            this.sync(); //这个是必须的,如果你要覆盖afterChange事件的话,请记得最好把这句加上.
        }
//        afterCreate: function(){this.sync();}
        //经测试，下面这行代码可有可无，不影响获取textarea的值
        //afterCreate: function(){this.sync();}
        //下面这行代码就是关键的所在，当失去焦点时执行 this.sync();
        //afterBlur: function () { this.sync(); }
    };
    $.parser.plugins.push("kindeditor");
})(jQuery, KindEditor);