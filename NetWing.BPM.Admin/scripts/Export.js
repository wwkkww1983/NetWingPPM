﻿function ExportExcel(gridid, actionURL, fields) {
    this.grid = $('#' + gridid);
    this.fields = fields; /*|| this.grid.datagrid('options').columns[0];*/
    this.actionURL = actionURL;
    console.log("id值" + grid);
    console.log(grid);
}

ExportExcel.prototype = {
    go: function () {
        var self = this;
        var hDialog = top.$.hDialog({
            content: '<p><b>请选择要导出的字段：</b>&nbsp;&nbsp;&nbsp;&nbsp;<input style="vertical-align:middle" type="checkbox" checked id="__check_all" /><label style="vertical-align:middle" for="__check_all">全选</label></p><ul id="field_down"></ul><ul class="checkbox" id="field_list"></ul>', width: 400, height: 300, title: '导出Excel数据',
            submit: function () {
                var selectedFields = '';
                top.$('#field_list :checked').each(function () {
                    var v = $(this).val();
                    selectedFields += v.split('|')[0] + ' as ' + v.split('|')[1] + ",";//这个是中文表头
                    //selectedFields += v.split('|')[0] + ",";
                });
                if (selectedFields != '')
                    selectedFields = selectedFields.substr(0, selectedFields.length - 1);
                else
                    selectedFields = " * ";

                var actionURL = self.actionURL;

                var where = $('body').data('where');
                if (!where)
                    where = "";
                //window.open('' + actionURL + '?action=export&fields=' + selectedFields + '&filters=' + where);
                //window.open('' + actionURL + '?json={"action":"export"}');
                var url = actionURL + '?json={"action":"export"}&fields='+selectedFields+'';
                $.ajax({
                    type: "POST",
                    url: url,
                    success: function (d) {
                        var k = JSON.parse(d);
                        top.$.messager.confirm({
                            title: '导出成功！',
                            msg: '该Excel文件可作为导入模板使用,作为导入模板使用时,可不填KeyId/Id等字段,点确认开始下载?',
                            ok: '下载',
                            fn: function (r) {
                                if (r) {
                                    //alert('ok');
                                    location.href = "/upload/excel/" + k.filename + "";
                                }
                            }
                        });
                        //alert(k.filename);
                        //$("#field_down").html("");
                        //$("#field_down").append("<a href='/upload/excel/" + k.filename + "' target='_blank'>下载Excel</a>");
                    }
                });

            }
        });

        //[{ 'title': 'keyid', 'field': 'keyid' }, { 'title': '按钮名称', 'field': 'ButtonText' }, { 'title': '权限标识', 'field': 'ButtonTag' }, { 'title': '排序', 'field': 'Sortnum' }];
        var lis = '';
        jQuery.each(this.fields, function (i, n) {
            if (n.field != 'ck') {//ck是选择字段在这里排除
                lis += '<li><input type="checkbox" checked style="vertical-align:middle" value="' + n.field + '|' + n.title + '" id="' + n.title + '"  ><label style="vertical-align:middle" for="' + n.field + '">' + n.title + '</label></li>';
            }

        });

        top.$('#field_list').empty().append(lis);
        top.$('#__check_all').click(function () {
            //alert("全选");
            var flag = $(this).is(":checked");
            alert(flag);
            top.$('#field_list :checkbox').attr('checked', flag);
        });
    }
}

