﻿function ImportExcel(gridid, actionURL, fields) {
    this.grid = $('#' + gridid);

    this.fields = fields || this.grid.datagrid('options').columns[0];
    this.actionURL = actionURL;
		
}

ImportExcel.prototype = {
    go: function () {
        var self = this;
        var hDialog = top.$.hDialog({
            content: '<div id="uploader" class="wu-example"><!--用来存放文件信息--><div id="thelist"  class="uploader-list"></div><div class="btns"><div id="picker">选择文件</div></div></div>',
            width: 400,
            height: 300,
            title: '导入Excel数据到数据库',
            submit: function () {

            },
            onOpen: function () {
                //alert(actionURL);
     
                var uploader = WebUploader.create({
                    //初始化Web Uploader
                    // swf文件路径 这里用html5方式上传因此注释
                    //swf: BASE_URL + '/js/Uploader.swf',

                    // 文件接收服务端。
                    server: actionURL+'?json={"action":"inport"}',

                    // 选择文件的按钮。可选。
                    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                    pick: { id: top.$('#picker'), multiple: false },// //只能选择一个文件上传
                    // 选完文件后，是否自动上传。
                    auto: true,
                    //限制只能上传一个文件
                    //fileNumLimit: 1,
                    // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
                    resize: false,
                    accept: {
                        title: 'Excel文件',
                        extensions: 'xls,xlsx',
                        mimeTypes: 'vnd.ms-excel'
                    }
                });

                // 文件上传成功  
                uploader.on('uploadSuccess', function (file,response) {
                    top.$.messager.alert(response.msg, '成功导入' + response.result + '条');
                });
            }
            //
        });


    }
}

