﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;
using NetWing.Common;

namespace NetWing.BPM.Admin.sys.ashx
{
    /// <summary>
    /// ConfigHandler 的摘要说明
    /// </summary>
    public class ConfigHandler : IHttpHandler,IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            var json = context.Request["json"];
            var action = context.Request["action"];
            var systemName = context.Request["systemName"];
            var website = context.Request["website"];
            var copyRight = context.Request["copyRight"];
            var syslogo = context.Request["syslogo"];
            switch (action)
            {
                case "js":
                    User u = UserDal.Instance.Get(SysVisitor.Instance.UserId);
                    string cj = u.ConfigJson;
                    if (string.IsNullOrEmpty(cj))
                        context.Response.Write("var sys_config ={\"theme\":{\"title\":\"默认皮肤\",\"name\":\"default\",\"selected\":true},\"showType\":\"Accordion\",\"gridRows\":20}");
                    else
                    {
                        string js = "var sys_config = " + cj;
                        context.Response.Write(js);
                    }
                    break;
                case "siteconfig":
                    systemName = NetWing.Common.ConfigHelper.GetValue("systemName");
                    website = NetWing.Common.ConfigHelper.GetValue("website");
                    copyRight = NetWing.Common.ConfigHelper.GetValue("copyRight");
                    syslogo = NetWing.Common.ConfigHelper.GetValue("syslogo");
                    string r = "{\"systemName\":\""+systemName+ "\",\"website\":\""+ website + "\",\"copyRight\":\""+ copyRight + "\",\"syslogo\":\""+syslogo+"\"}";
                    context.Response.Write(r);
                    break;
                default:
                    int k = UserDal.Instance.UpdateUserConfig(SysVisitor.Instance.UserId, json);
                    SysVisitor.Instance.CurrentUser.ConfigJson = json;
                    if (!string.IsNullOrEmpty(systemName))//系统名称不为空择设置系统名称
                    {
                        NetWing.Common.ConfigHelper.SetAppSetting("systemName", systemName);
                    }
                    if (!string.IsNullOrEmpty(website))//网站名称
                    {
                        NetWing.Common.ConfigHelper.SetAppSetting("website", website);
                    }
                    if (!string.IsNullOrEmpty(copyRight))//版权所有
                    {
                        NetWing.Common.ConfigHelper.SetAppSetting("copyRight", copyRight);
                    }
                    if (!string.IsNullOrEmpty(syslogo))//版权所有
                    {
                        NetWing.Common.ConfigHelper.SetAppSetting("syslogo", syslogo);
                    }

                    context.Response.Write(k);
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}