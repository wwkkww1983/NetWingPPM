﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using NetWing.BPM.Core;
using NetWing.Common;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Model;
using Omu.ValueInjecter;

namespace NetWing.BPM.Admin.sys.ashx
{
    /// <summary>
    /// openHandler 的摘要说明
    /// </summary>
    public class openHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            string action = context.Request["action"];
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "deps":
                        context.Response.Write(DepartmentBll.Instance.GetDepartmentTreegridData().Replace("KeyId", "id").Replace("DepartmentName", "text"));
                        break;
                    default:
                        break;
                }


            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}