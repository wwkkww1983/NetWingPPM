﻿<%@ Page Language="C#" MasterPageFile="../Site1.Master" AutoEventWireup="true" CodeBehind="CommBook.aspx.cs" Inherits="NetWing.BPM.Admin.sys.CommBook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src='ashx/RoleHandler.ashx?json={"action":"btnColumns"}'></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div id="layout">
        <div region="west" iconCls="icon-chart_organisation" split="true" title="部门树" style="width:220px;padding: 5px" collapsible="false">
            <ul id="deptree"></ul>
        </div>
        <div region="center" title="员工通讯录" iconCls="icon-users" style="padding: 2px; overflow: hidden">
            <div id="toolbar">
               <%=base.BuildToolbar() %>
                <div class="datagrid-btn-separator"></div>
            </div>
            <table id="userGrid" toolbar="#toolbar"></table>

        </div>
    </div>
    
    <script type="text/javascript" src="../scripts/Linqjs/linq.min.js"></script>
    <script type="text/javascript" src="../scripts/Linqjs/linq.jquery.js"></script>
    <script type="text/javascript" src="js/commbook.js?s=134"></script>
</asp:Content>
