using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJFound")]
	[Description("")]
	public class MJFoundModel
	{
				/// <summary>
		/// ID
		/// </summary>
		[Description("ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 姓名
		/// </summary>
		[Description("姓名")]
		public string username { get; set; }		
		/// <summary>
		/// 手机
		/// </summary>
		[Description("手机")]
		public string mobile { get; set; }		
		/// <summary>
		/// 房间号
		/// </summary>
		[Description("房间号")]
		public string room_id { get; set; }		
		/// <summary>
		/// 丢失时间
		/// </summary>
		[Description("丢失时间")]
		public DateTime losstime { get; set; }		
		/// <summary>
		/// 部门
		/// </summary>
		[Description("部门")]
		public string dep { get; set; }		
		/// <summary>
		/// 部门ID
		/// </summary>
		[Description("部门ID")]
		public int depid { get; set; }		
		/// <summary>
		/// 丢失物品登记
		/// </summary>
		[Description("丢失物品登记")]
		public string ts_context { get; set; }		
		/// <summary>
		/// 处理状态
		/// </summary>
		[Description("处理状态")]
		public string cl_state { get; set; }		
		/// <summary>
		/// 操作员
		/// </summary>
		[Description("操作员")]
		public string cz_user { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
		/// <summary>
		/// 系统时间
		/// </summary>
		[Description("系统时间")]
		public DateTime add_time { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}