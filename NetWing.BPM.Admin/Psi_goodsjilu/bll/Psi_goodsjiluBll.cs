using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_goodsjiluBll
    {
        public static Psi_goodsjiluBll Instance
        {
            get { return SingletonProvider<Psi_goodsjiluBll>.Instance; }
        }

        public int Add(Psi_goodsjiluModel model)
        {
            return Psi_goodsjiluDal.Instance.Insert(model);
        }

        public int Update(Psi_goodsjiluModel model)
        {
            return Psi_goodsjiluDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_goodsjiluDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_goodsjiluDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
