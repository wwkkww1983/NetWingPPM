using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJDevice_child")]
	[Description("设备明细管理")]
	public class MJDevice_childModel
	{
				/// <summary>
		/// ID
		/// </summary>
		[Description("ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 品名
		/// </summary>
		[Description("品名")]
		public string dcname { get; set; }		
		/// <summary>
		/// 设备分类
		/// </summary>
		[Description("设备分类")]
		public string dcclass { get; set; }		
		/// <summary>
		/// 编号
		/// </summary>
		[Description("编号")]
		public string dcnumber { get; set; }		
		/// <summary>
		/// 状态
		/// </summary>
		[Description("状态")]
		public string state { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
		/// <summary>
		/// 系统时间
		/// </summary>
		[Description("系统时间")]
		public DateTime add_time { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}