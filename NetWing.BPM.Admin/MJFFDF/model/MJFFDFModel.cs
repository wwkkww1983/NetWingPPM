using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJFFDF")]
	[Description("房费电费管控表")]
	public class MJFFDFModel
	{
				/// <summary>
		/// 系统编号
		/// </summary>
		[Description("系统编号")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 房间ID
		/// </summary>
		[Description("房间ID")]
		public int roomid { get; set; }		
		/// <summary>
		/// 房号
		/// </summary>
		[Description("房号")]
		public string roomno { get; set; }		
		/// <summary>
		/// 年
		/// </summary>
		[Description("年")]
		public int theyear { get; set; }		
		/// <summary>
		/// 1月房费
		/// </summary>
		[Description("1月房费")]
		public decimal f1 { get; set; }		
		/// <summary>
		/// 2月房费
		/// </summary>
		[Description("2月房费")]
		public decimal f2 { get; set; }		
		/// <summary>
		/// 3月房费
		/// </summary>
		[Description("3月房费")]
		public decimal f3 { get; set; }		
		/// <summary>
		/// 4月房费
		/// </summary>
		[Description("4月房费")]
		public decimal f4 { get; set; }		
		/// <summary>
		/// 5月房费
		/// </summary>
		[Description("5月房费")]
		public decimal f5 { get; set; }		
		/// <summary>
		/// 6月房费
		/// </summary>
		[Description("6月房费")]
		public decimal f6 { get; set; }		
		/// <summary>
		/// 7月房费
		/// </summary>
		[Description("7月房费")]
		public decimal f7 { get; set; }		
		/// <summary>
		/// 8月房费
		/// </summary>
		[Description("8月房费")]
		public decimal f8 { get; set; }		
		/// <summary>
		/// 9月房费
		/// </summary>
		[Description("9月房费")]
		public decimal f9 { get; set; }		
		/// <summary>
		/// 10月房费
		/// </summary>
		[Description("10月房费")]
		public decimal f10 { get; set; }		
		/// <summary>
		/// 11月房费
		/// </summary>
		[Description("11月房费")]
		public decimal f11 { get; set; }		
		/// <summary>
		/// 12月房费
		/// </summary>
		[Description("12月房费")]
		public decimal f12 { get; set; }		
		/// <summary>
		/// 1月电费
		/// </summary>
		[Description("1月电费")]
		public decimal d1 { get; set; }		
		/// <summary>
		/// 2月电费
		/// </summary>
		[Description("2月电费")]
		public decimal d2 { get; set; }		
		/// <summary>
		/// 3月电费
		/// </summary>
		[Description("3月电费")]
		public decimal d3 { get; set; }		
		/// <summary>
		/// 4月电费
		/// </summary>
		[Description("4月电费")]
		public decimal d4 { get; set; }		
		/// <summary>
		/// 5月电费
		/// </summary>
		[Description("5月电费")]
		public decimal d5 { get; set; }		
		/// <summary>
		/// 6月电费
		/// </summary>
		[Description("6月电费")]
		public decimal d6 { get; set; }		
		/// <summary>
		/// 7月电费
		/// </summary>
		[Description("7月电费")]
		public decimal d7 { get; set; }		
		/// <summary>
		/// 8月电费
		/// </summary>
		[Description("8月电费")]
		public decimal d8 { get; set; }		
		/// <summary>
		/// 9月电费
		/// </summary>
		[Description("9月电费")]
		public decimal d9 { get; set; }		
		/// <summary>
		/// 10月电费
		/// </summary>
		[Description("10月电费")]
		public decimal d10 { get; set; }		
		/// <summary>
		/// 11月电费
		/// </summary>
		[Description("11月电费")]
		public decimal d11 { get; set; }		
		/// <summary>
		/// 12月电费
		/// </summary>
		[Description("12月电费")]
		public decimal d12 { get; set; }		
		/// <summary>
		/// 部门所有者
		/// </summary>
		[Description("部门所有者")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}