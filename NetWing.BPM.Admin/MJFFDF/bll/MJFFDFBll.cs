using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJFFDFBll
    {
        public static MJFFDFBll Instance
        {
            get { return SingletonProvider<MJFFDFBll>.Instance; }
        }

        public int Add(MJFFDFModel model)
        {
            return MJFFDFDal.Instance.Insert(model);
        }

        public int Update(MJFFDFModel model)
        {
            return MJFFDFDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJFFDFDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJFFDFDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
