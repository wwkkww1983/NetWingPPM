﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using Senparc.Weixin.MP.CommonAPIs;



namespace NetWing.BPM.Admin.weixin
{
    public partial class auth2getcode : System.Web.UI.Page
    {
        public void Page_Load(Object src, EventArgs e)
        {
            //思路：用两个页面来完成一个页面auth2getcode获取微信code
            //      另外一个页面auth2page
            //方法:status=getcode 取得code status=oauth授权
            string appid = NetWing.Common.ConfigHelper.GetValue("AppID");
            string secret = NetWing.Common.ConfigHelper.GetValue("AppSecret");
            //snsapi_base 不需要用户点同意，直接跳转到授权后的页面，只能用于获取openid，不能获取用户基本信息
            //snsapi_userinfo 会征求用户同意，授权后，可以获取用户基本信息
            string url = OAuthApi.GetAuthorizeUrl(appid, NetWing.Common.ConfigHelper.GetValue("website") + "/weixin/auth2page.aspx", "oauth", OAuthScope.snsapi_userinfo);
            Response.Redirect(url);
        }



    }

}