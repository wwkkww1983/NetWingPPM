using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJFixBll
    {
        public static MJFixBll Instance
        {
            get { return SingletonProvider<MJFixBll>.Instance; }
        }

        public int Add(MJFixModel model)
        {
            return MJFixDal.Instance.Insert(model);
        }

        public int Update(MJFixModel model)
        {
            return MJFixDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJFixDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJFixDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
