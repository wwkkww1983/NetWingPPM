using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydorderhoujiagongBll
    {
        public static JydorderhoujiagongBll Instance
        {
            get { return SingletonProvider<JydorderhoujiagongBll>.Instance; }
        }

        public int Add(JydorderhoujiagongModel model)
        {
            return JydorderhoujiagongDal.Instance.Insert(model);
        }

        public int Update(JydorderhoujiagongModel model)
        {
            return JydorderhoujiagongDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydorderhoujiagongDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydorderhoujiagongDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
