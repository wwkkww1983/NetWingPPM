﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="wxts.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.wxts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.min.js"></script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button({
            name: '打印',
            focus: true,
            callback: function () {
                printWin();
            }
        }, {
                name: '取消'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
        <div id="content">
            <style>
* {
	margin: 0px;
	padding: 0px;
}
html {
	overflow-x: hidden;
}
body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
	font-family: '微软雅黑';
}
input, button, textarea, select, optgroup, option {
	font-family: inherit;
	font-size: 100%;
	font-style: inherit;
	font-weight: inherit;
}
ul, li, dl, dt, dd, ol {
	display: block;
	list-style: none;
}
img {
	border: 0;
}
.clear {
	clear: both;
	height: 0;
}
.clear-fix:after {
	content: ".";
	display: block;
	height: 0;
	font-size: 0;
	clear: both;
	visibility: hidden;
}
a {
	text-decoration: none;
	outline: 0;
}
.big_box {
	width: 100%;
	float: left;
 cellSpacing=0 cellPadding=0 width=600 border=0
}
.center_box {
	width: 600px;
	overflow: auto;
	height: 900px;
	margin: 0 auto;
	background: #fff;
	border: #e5e5e5 1px solid;
	margin-top: 10px;
	padding: 0 97px 0 97px;
	max-height: 1123px;
}
.title {
	text-align: center;
	font-size: 46px;
	letter-spacing: 50px;
	font-family: "宋体";
	margin-top: 50px;
}
.center {
	width: 600px;
	margin: 0 auto;
	margin-top: 60px;
}
.center h1 {
	font-size: 30px;
	font-weight: normal;
}
.center p {
	text-indent: 2em;
	line-height: 46px;
	display: block;
	font-size: 24px;
}
.center span {
	text-align: center;
	display: block;
	margin-top: 100px;
	font-size: 34px;
	letter-spacing: 10px;
}
.last_text {
	text-align: center;
	display: block;
	margin-top: 150px;
}
.last_text span {
	font-size: 30px;
}
.last_text p {
	font-size: 24px;
	margin-top: 10px;
	line-height: 34px;
}
</style>
            <div class="big_box">
                <br />
                <br />
<center><h1>Me+国际青年公寓业主手册</h1></center>
                <div class="title">温馨提示</div>
                <div class="center">
                    <h1>亲爱的业主：</h1>
                    <p>我们业主之家微信群是提供给业主之间交友、交流创业，也是为了业主与公寓方交流沟通的平台，请业主之间互加微信千万慎重处理，切记业主之间勿相互借钱财及有价证券！ （以前业主之间已屡次发生借钱不还的事件）</p>
<p>本群不允许粗话脏话，不允许发布广告推销，不允许发布不实言论，违反本群规定者移出本群！</p>
                    <span>切  记  切  记    ！！！</span>
                </div>
                <!--<div class="last_text">
                    <span>Me+国际青年公寓</span>
                    <p>
                        云南米佳公寓酒店管理有限公司<br>
                        昆明久久房地产信息咨询有限公司<br>
		云南君临企业管理有限公司
                    </p>
                </div>-->
            </div>

        </div>
    </form>
</body>
</html>
