﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="yzsc.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.yzsc" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.min.js"></script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button({
            name: '打印',
            focus: true,
            callback: function () {
                printWin();
            }
        }, {
                name: '取消'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content">
            <style>
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: inherit;
                    font-size: 100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    float: left;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    width: 600px;
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }

                .title {
                    text-align: center;
                    font-size: 46px;
                    letter-spacing: 50px;
                    font-family: "宋体";
                    margin-top: 50px;
                }

                .center {
                    margin: 0 auto;
                    margin-top: 60px;
                }

                    .center h1 {
                        font-size: 30px;
                        font-weight: normal;
                    }

                    .center p {
                        text-indent: 2em;
                        line-height: 46px;
                        display: block;
                        font-size: 24px;
                    }

                    .center span {
                        text-align: center;
                        display: block;
                        margin-top: 100px;
                        font-size: 34px;
                        letter-spacing: 10px;
                    }

                .last_text {
                    text-align: center;
                    display: block;
                    margin-top: 150px;
                }

                    .last_text span {
                        font-size: 30px;
                    }

                    .last_text p {
                        font-size: 24px;
                        margin-top: 10px;
                        line-height: 34px;
                    }

                .center_box h2 {
                    text-align: center;
                    font-size: 46px;
                    font-family: "宋体";
                    margin-top: 30px;
                }

                .center_box h3 {
                    text-align: center;
                    font-size: 26px;
                    font-family: "宋体";
                    margin-top: 10px;
                }

                .name {
                    text-align: left;
                    display: block;
                    line-height: 30px;
                    margin-top: 15px;
                    font-size: 18px;
                }

                .text_center {
                    width: 100%;
                    margin: 0 auto;
                    text-align: left;
                }

                    .text_center b {
                        height: 34px;
                        line-height: 34px;
                        display: block;
                    }

                    .text_center p {
                        font-size: 14px;
                        line-height: 24px;
                    }

                .he_footer {
                    width: 100%;
                    float: left;
                }

                    .he_footer span {
                        float: left;
                        font-weight: 900;
                    }

                    .he_footer em {
                        float: right;
                        font-weight: 900;
                        font-style: normal;
                    }

                .header {
                    width: 100%;
                    display: block;
                    margin: 0 auto;
                    text-align: center;
                }

                    .header img {
                        width: 100%;
                        display: block;
                        text-align: center;
                        margin: 0 auto;
                    }

                .center_box h4 {
                    text-align: center;
                    font-size: 42px;
                }

                    .center_box h4 span {
                        color: #ea7d31;
                    }

                .text_center01 {
                    width: 100%;
                    margin: 0 auto;
                    text-align: left;
                    margin-top: 10px;
                }

                    .text_center01 b {
                        height: 34px;
                        line-height: 34px;
                        display: block;
                    }

                    .text_center01 p, .text_center01 h4 {
                        font-size: 12px !important;
                        line-height: 16.5px;
font-weight:normal;
                    }

                    .text_center01 b span {
                        color: #ea7d31;
                    }

                    .text_center01 h2 {
                        font-size: 28px;
                        font-weight: normal;
                        text-align: left;
                    }

                .table {
                    width: 100%;
                    margin: 0 auto;
text-align:center;
                }
.table tr td{font-size: 12px !important;
                        line-height: 16.5px;}

.table tr{height:16.5px; line-height:16.5px;}


                .table01 {
                    width: 100%;
                }

                    .table01 tr {
                        width: 100%;
                    }

                        .table01 tr td {
                            width: 25%;
                        }

                .text_footer {
                    text-align: center;
                    margin-top: 15px;
                    font-weight: 600;
                }
.kongge{width:100%;height:100px;display:block; border:#000 1px solid; float:left;}
            </style>
            <div class="big_box">
                <br /><br /><br />
                <div class="header">
                    <img src="/mjprint/img/photo.png">
                </div><br /><br />
                <center><h1>Me+国际青年公寓业主手册</h1></center>
                <div class="text_center01">
                    <b>尊贵的业主：</b>
                    <p style="text-indent: 2em;">
                        非常欢迎您入住Me+国际青年公寓! 本公寓是集住宿、健身、娱乐、休闲、交友创业为一体的综合性国际青年公寓。
        首先向各位业主介绍下公寓概况：
                    </p>
                    <b>一．Me+国际青年公寓概况</b>
                    <p>1、本公寓为业主设立有免费健身中心、乒乓球、台球、拳击训练台、茶吧、摇椅吧、钢琴吧、古筝吧、乐器吧、商务中心、卡拉ok等。</p>
                    <p>2、本公寓设有自助售货机，免费咖啡机及免费奶茶机，周末免费提供水果、糕点等。</p>
                    <p>3、本公寓为保证业主能有安静的休息环境，处于对业主人身及财产安全考虑及促进年轻人交流创业，以下人员本公寓不接待敬请谅解：45周岁以上、带8岁以下小孩、夜场工作人员、不爱交朋友、饲养大小型宠物。</p>
                    <p>4、为确保安全，进入本公寓院落及楼宇场所及乘坐电梯必须刷IC卡及房间IC卡预付费供电系统。</p>
                    <p>5、业主不得在本公寓范围任何地方丢弃垃圾，所有生活垃圾必须自行带到公寓指定的垃圾桶，生活垃圾不允许摆放房间在门口、楼层过道及楼层垃圾桶（楼层垃圾桶仅仅为丢弃零碎小东西设立），绝对不允许将方便面及油污杂物丢弃在楼层垃圾桶，否则Me+国际青年公寓有权按违约解除合同。</p>
                    <p>6、本公寓不允许饲养任何大小型宠物，不得在本公寓开设任何性质公司及进行任何商务活动，例如传销直销、开网店、设立厂商办事处等。</p>
                    <p>7、本公寓每个房间均标的有独立卫生间、厨房、冰箱、洗衣机、网络电视机、宽带（自费20M—200M宽带）、热水器、电磁炉、烧水壶、三门大衣柜、个性化沙发、茶几、席梦思、垫被芯、盖被芯、枕芯，业主拎包入住即可。</p>
                    <p>8、本公寓主楼顶配有约500平米左右休闲中心：有摇椅、凉亭、动力摇椅、鸟巢吊椅、吊床、秋千椅、沙滩椅、帐篷等，免费WIFI、免费咖啡为业主免费提供休闲，副楼楼顶约200平米左右休闲中心：有摇椅、凉亭、动力摇椅、鸟巢吊椅、吊床、秋千椅、帐篷等，免费WIFI、免费咖啡为业主免费提供休闲。</p>
                    <p>9、健身中心有75寸大屏幕彩电或者投影仪及沙发、茶几、免费咖啡奶茶、训练拳击台、钢琴吧、台球、乒乓球及400平米休息厅均免费向业主开放。</p>
                    <p>10、本公寓设有电瓶车出租，按月计费：180元/月包含租赁充电保管服务，丢失按价赔偿，自备电瓶车按80元/月含充电及保管服务，自备自行车20元/月（在本公寓收取保管费物品丢失本公寓赔偿）。</p>
                    <p>11、本公寓设有内部机动车停车场按月支付。</p>
                    <p>12、本公寓就餐及用电，均采用预付费形式，入住业主用购电卡在本公寓商务中心购电充值，租赁期满，剩余电费余额退还业主。</p>
                    <p>13、本公寓业主有亲朋好友到访必须在门卫用身份证登记，并且要与业主电话联系方可进入，否则不予进入本公寓 .</p>
                    <p>14、乙方每月在房租到期前7天以电话、短信或者微信的形式与甲方工作人员确认是否续租，房租到期前5天为最后确认时间，确认后又不再续租的业主，除正常收取房屋日租金以外还将以每天100元的违约金扣除违约费用；乙方每月房租须提前五天预交，每延迟一天按照100元/天扣除违约金。
直到按本合同违约处理。 </p>
                    <p>15、本公寓设立商务中心可以接受业主收发传真（收费）、扫描（收费）、收发邮件、打印（收费）、复印（收费）、照相（收费）等服务，商务中心可以接受房间保洁、洗烫和私人租车等有偿收费服务预约等。</p>
                    <p>
                        16、本公寓为业主备有各种急用药、针线包、血压仪，各位业主急需时可以到商务中心领取使用。
                    </p>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    <b style="text-align:center;font-size:20px; margin:15px 0 15px 0;">Me+国际青年公寓入住须知</b>
                    <h4>1、业主在入住前必须仔细检查房间设施情况，如有损坏请书面填写备忘录。</h4>
                    <h4>2、业主入住期间对房屋所有设施及公共配套设施造成损坏的须按价赔偿。</h4>
<h4>3. 公共部分设施造成损失按以上价格赔偿，以下没有价格的设施按实际损失赔偿。</h4>

                    <!--表格-->
                    <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#000;" class="table">
         
                        <%int k = 1; %>
                        <%foreach (DataRow dr in dt.Rows){%>
                                                
                        <tr>
                            <td><%=k%></td>
                            <td><%=dr["title"].ToString() %></td>
                            <td><%=dr["unit"].ToString() %></td>
                            <td><%=dr["cost"].ToString() %></td>
                            <td><%=dr["note"].ToString() %>&nbsp;</td>
                        </tr>
                        <%k = k + 1; %>
                      <%} %>
                    </table>
                    <table class="table" style="text-align:left;">
                        <tr width="100%">
                            <td width="34%">Me+国际青年公寓代表人：<br>
                                日期：</td>
                            <td width="14%"></td>
                            <td width="34%">业主签字：<br>
                                日期：</td>
                            <td width="14%"></td>
                        </tr>
                    </table>


                </div>

            </div>
        </div>
    </form>
</body>
</html>
