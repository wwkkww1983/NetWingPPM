﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;

namespace NetWing.BPM.Admin.mjprint
{

    public partial class zfht : System.Web.UI.Page
    {
        public bool showprint = false;//显示打印按钮
        public string mainKeyId = "";
        public DataTable dtmain = null;
        public DataTable dtdetail = null;
        public DataTable dtyj = null;//押金
        public DataRow depDr = null;//获得部门信息
        public DataRow oDr=null;
        public DataRow roomDr = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            showprint = true;//显示合同可以打印
            this.opt.Visible = false;//默认显示用户确认信息
            mainKeyId = Request["KeyId"];
            string sqlmain = "select * from MJOrder where KeyId=" + mainKeyId + "";
            oDr = SqlEasy.ExecuteDataRow(sqlmain);
            dtmain = SqlEasy.ExecuteDataTable(sqlmain);
            depDr = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + dtmain.Rows[0]["depid"].ToString()+"");
            //得到用户房租信息
            string sqldetail = "select * from MJOrderDetail where MJOrderid=" + mainKeyId + " and serviceid=3 order by KeyId asc";
            dtdetail = SqlEasy.ExecuteDataTable(sqldetail);
            //处理是否显示客户确认标识
            DataRow dtdr = dtdetail.Rows[0];
            if (dtdr["htstate"].ToString()=="1")
            {
                showprint = true;
                this.opt.Visible = false;//隐藏客户确认信息
            }
            string sqlyj = "select * from MJOrderDetail where MJOrderid=" + mainKeyId + " and serviceid=5 order by KeyId asc";
            dtyj = SqlEasy.ExecuteDataTable(sqlyj);

            roomDr = SqlEasy.ExecuteDataRow("select * from MJRooms where keyid="+ oDr["roomid"].ToString() + "");

        }

        protected void cussure_Click(object sender, EventArgs e)
        {
            showprint = true;//显示打印窗口
            //更新房间状态
            DataRow mdr = dtmain.Rows[0];//只可能是一行
            DataRow ddr = dtdetail.Rows[0];//只可能是一行
            //string sql = "update MJRooms set state='租赁中',add_time='" + ddr["add_time"] + "',up_time='" + ddr["add_time"] + "',exp_time='"+ddr["exp_time"]+"' where keyid="+mdr["roomid"].ToString()+" ";
            //SqlEasy.ExecuteNonQuery(sql);
            //更新订单明细表签订合同标识是否已确定合同标识0没打1已经打了
            string htsql = "update MJOrderDetail set htstate=1 where keyid="+ddr["keyid"]+"";
            SqlEasy.ExecuteNonQuery(htsql);

            this.opt.Visible = false;//把客户确认隐藏

        }
    }
}