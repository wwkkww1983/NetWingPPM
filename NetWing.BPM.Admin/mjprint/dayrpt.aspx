﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="dayrpt.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.dayrpt" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        table {
            border: #95b8e7 1px solid;
            border-collapse: collapse;
            border-spacing: 0;
            text-align: center;
            font: normal 12px "\5FAE\8F6F\96C5\9ED1";
            color: #444;
        }

            table th {
                height: 28px;
                line-height: 28px;
                background: -webkit-linear-gradient(#eff5fe,#e0ecff);
                background: -moz-linear-gradient(#eff5fe,#e0ecff);
                background: -o-linear-gradient(#eff5fe,#e0ecff);
                background: linear-gradient(#eff5fe,#e0ecff);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eff5fe', endColorstr='#e0ecff', GradientType=0);
                border-bottom: #95b8e7 1px solid;
                border-right: #95b8e7 1px dotted;
            }

            table td {
                height: 28px;
                line-height: 28px;
                border: #95b8e7 1px dotted;
                padding: 0px;
            }

            table tr:nth-child(odd) {
                background: #F4F4F4;
            }

            table tr:hover {
                background: #e2edff;
            }
    </style>
    <div id="tt" class="easyui-tabs" style="width: 100%; height: 760px;">
        <div title="月退房数" style="padding: 20px; display: none;">
            <table>
                <tr>
                    <td>序号</td>
                    <td>年份</td>
                    <td>月份</td>
                    <td>退房数</td>
                </tr>
                <%
                    int j = 1;
                    foreach (DataRow mdr in mdt.Rows)
                    {%>
                <tr>
                    <td style="width: 80px;"><%=j %></td>
                    <td style="width: 120px;"><%=mdr["theyear"].ToString() %></td>
                    <td style="width: 120px;"><%=mdr["themonth"].ToString() %></td>
                    <td style="width: 80px;"><%=mdr["tuinum"].ToString() %></td>
                </tr>
                <%
                        j = j + 1;
                    } %>
                <tr>
                    <td colspan="3">总退房</td>
                    <td><%=mdt.Compute("sum(tuinum)", "TRUE")   %></td>
                </tr>
            </table>

        </div>
        <div title="日退房数" style="padding: 20px; display: none;">

            <table>
                <tr>
                    <td>序号</td>
                    <td>日期</td>
                    <td>退房数</td>
                </tr>
                <%
                    int i = 1;
                    foreach (DataRow rdr in rdt.Rows)
                    {%>
                <tr>
                    <td style="width: 80px;"><%=i %></td>
                    <td style="width: 120px;"><%=rdr["thedate"].ToString() %></td>
                    <td style="width: 80px;"><%=rdr["tuinum"].ToString() %></td>
                </tr>
                <%
                        i = i + 1;
                    } %>
                <tr>
                    <td colspan="2">总退房</td>
                    <td><%=rdt.Compute("sum(tuinum)", "TRUE")   %></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

