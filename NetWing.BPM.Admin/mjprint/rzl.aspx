﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="rzl.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.rzl" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        table {
            border: #95b8e7 1px solid;
            border-collapse: collapse;
            border-spacing: 0;
            text-align: center;
            font: normal 12px "\5FAE\8F6F\96C5\9ED1";
            color: #444;
        }

            table th {
                height: 28px;
                line-height: 28px;
                background: -webkit-linear-gradient(#eff5fe,#e0ecff);
                background: -moz-linear-gradient(#eff5fe,#e0ecff);
                background: -o-linear-gradient(#eff5fe,#e0ecff);
                background: linear-gradient(#eff5fe,#e0ecff);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eff5fe', endColorstr='#e0ecff', GradientType=0);
                border-bottom: #95b8e7 1px solid;
                border-right: #95b8e7 1px dotted;
            }

            table td {
                height: 28px;
                line-height: 28px;
                border: #95b8e7 1px dotted;
                padding: 0px;
            }

            table tr:nth-child(odd) {
                background: #F4F4F4;
            }

            table tr:hover {
                background: #e2edff;
            }
    </style>
    <div id="tt" class="easyui-tabs" style="width: 100%; height: 760px;">
        <%foreach (DataRow roomdr in roomdt.Rows)
            {%>
        <div title="<%=roomdr["roomnumber"].ToString() %>" style="padding: 20px; display: none;">
           
            <table border="1">
                <tr>
                    <td style="width:80px;">序号</td>
                    <td style="width:80px;">姓名</td>
                    <td style="width:120px;">年度开始日期</td>
                    <td style="width:120px;">入住日期</td>
                    <td style="width:120px;">到期日期</td>
                    <td style="width:80px;">合同入住天数</td>
                    <td style="width:80px;">实际入住天数</td>
                    <td style="width:120px;">截止当前本年度天数</td>
                    <td style="width:120px;">全年入住天数差</td>
                    <td style="width:80px;">月入住率</td>
                    <td style="width:80px;">状态</td>
                </tr>
                <%
                    DataTable odt = getOdt(roomdr["keyid"].ToString());
                    string status = "正常";
                    DateTime nowtime = DateTime.Now;
                    DateTime yearstar = DateTime.Parse("1900-01-01");//年度开始日期
                    DateTime tuidate = DateTime.Parse("1900-01-01");//退房日期
                    int sjts = 0;//实际入住天数
                    int i = 1;//数字初始开始
                    decimal rzl = 0;//初始入住率1
                    foreach (DataRow dr in odt.Rows)
                    {
                        if (i == 1)
                        {
                            yearstar = DateTime.Parse(dr["orderstart_time"].ToString());
                        }
                        DateTime stime = DateTime.Parse(dr["orderstart_time"].ToString());//入住日期
                        DateTime etime = DateTime.Parse(dr["orderend_time"].ToString());//到期日期
                        TimeSpan ht = etime.Subtract(stime);//合同天数差
                        //如果退房日期为空则退房日期等于合同日期
                        if (!string.IsNullOrEmpty(dr["istuidate"].ToString()))
                        {
                            tuidate = DateTime.Parse(dr["istuidate"].ToString());
                        }
                        else
                        {
                            tuidate = etime;
                        }
                        //计算实际入住天数
                        TimeSpan sj = tuidate.Subtract(stime);
                        sjts = sjts + sj.Days;
                        DateTime h = DateTime.Now;//用来算截止当前本年度天数
                        TimeSpan nowyear = h.Subtract(yearstar);//截止当日本年度天数
                        if (dr["orderstatus"].ToString() == "-1")//如果房间已退则当前时间为退房时间
                        {
                            h = tuidate;
                            rzl = decimal.Round(decimal.Parse(sjts.ToString()) / nowyear.Days, 2) * 100;
                            status = "正常";
                        }
                        else
                        {
                            status = "未结算";
                            h = nowtime;
                            rzl = 0;
                        }



                %>
                <tr>
                    <td><%=i %></td>
                    <td><%=dr["users"].ToString() %></td>
                    <td><%=yearstar %></td>
                    <td><%=dr["orderstart_time"].ToString() %></td>
                    <td><%=dr["orderend_time"].ToString() %></td>
                    <td><%=ht.Days %></td>
                    <td><%=sjts %></td>
                    <!--实际入住天数-->
                    <td><%=nowyear.Days %></td>
                    <!--截止当日本年度天数-->
                    <td><%=sjts-nowyear.Days %></td>
                    <!--全年入住天数差-->
                    <td><%=rzl %>%</td>
                    <!--月入住率-->
                    <td style="width:80px;"><%=status %></td><!---->
                </tr>
                <%
                        rzl = 0;//清空入住率
                        i = i + 1;
                    } %>

                 <tr>
                    <td colspan="11" style="width:80px;">计算公式：入住率=(实际入住天数/截止当日本年度天数)*100;
    全年入住天数差=实际入住天数-截止当日本年度天数;未结算状态则入住率是0%</td>
                    
         
                </tr>
            </table>
        </div>
        <%} %>
    </div>






    
</asp:Content>
