using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJDayRpt")]
	[Description("财务日报表")]
	public class MJDayRptModel
	{
				/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 当日收入
		/// </summary>
		[Description("当日收入")]
		public decimal dayMoneyIn { get; set; }		
		/// <summary>
		/// 当日收入明细
		/// </summary>
		[Description("当日收入明细")]
		public string dayMoneyInInfo { get; set; }		
		/// <summary>
		/// 当日支出
		/// </summary>
		[Description("当日支出")]
		public decimal dayMoneyOut { get; set; }		
		/// <summary>
		/// 当日支出明细
		/// </summary>
		[Description("当日支出明细")]
		public string dayMoneyOutInfo { get; set; }		
		/// <summary>
		/// 收入合计
		/// </summary>
		[Description("收入合计")]
		public decimal totalIn { get; set; }		
		/// <summary>
		/// 支出合计
		/// </summary>
		[Description("支出合计")]
		public decimal totalOut { get; set; }		
		/// <summary>
		/// 余额合计
		/// </summary>
		[Description("余额合计")]
		public decimal totalBalance { get; set; }		
		/// <summary>
		/// 利润（扣除定金、押金）
		/// </summary>
		[Description("利润（扣除定金、押金）")]
		public decimal totalProfit { get; set; }		
		/// <summary>
		/// 截止上日实际余款
		/// </summary>
		[Description("截止上日实际余款")]
		public decimal nowBalance { get; set; }		
		/// <summary>
		/// 当日现金余额
		/// </summary>
		[Description("当日现金余额")]
		public decimal nowCashBalance { get; set; }		
		/// <summary>
		/// 截止当日实际余款
		/// </summary>
		[Description("截止当日实际余款")]
		public decimal nowDayBalance { get; set; }		
		/// <summary>
		/// 电瓶车停车数量
		/// </summary>
		[Description("电瓶车停车数量")]
		public int bicycleCar { get; set; }		
		/// <summary>
		/// 自行车停车数量
		/// </summary>
		[Description("自行车停车数量")]
		public int bikeCar { get; set; }		
		/// <summary>
		/// 截止当日预定人数
		/// </summary>
		[Description("截止当日预定人数")]
		public int bookNum { get; set; }		
		/// <summary>
		/// 预定最迟大概入住时间
		/// </summary>
		[Description("预定最迟大概入住时间")]
		public DateTime inLashTime { get; set; }		
		/// <summary>
		/// 截止当日空房数
		/// </summary>
		[Description("截止当日空房数")]
		public int freeRoom { get; set; }		
		/// <summary>
		/// 截止当日空房明细
		/// </summary>
		[Description("截止当日空房明细")]
		public string freeRoomInfo { get; set; }		
		/// <summary>
		/// 截止当日空房原因
		/// </summary>
		[Description("截止当日空房原因")]
		public string freeRoomWhat { get; set; }		
		/// <summary>
		/// 当日退房数
		/// </summary>
		[Description("当日退房数")]
		public int quitRoomNum { get; set; }		
		/// <summary>
		/// 当日退房明细
		/// </summary>
		[Description("当日退房明细")]
		public string quitRoomInfo { get; set; }		
		/// <summary>
		/// 是否安排入住
		/// </summary>
		[Description("是否安排入住")]
		public string isIn { get; set; }		
		/// <summary>
		/// 签月度合同房间数
		/// </summary>
		[Description("签月度合同房间数")]
		public int monthNum { get; set; }		
		/// <summary>
		/// 签季度合同房间数
		/// </summary>
		[Description("签季度合同房间数")]
		public int quarterNum { get; set; }		
		/// <summary>
		/// 签半年合同房间数
		/// </summary>
		[Description("签半年合同房间数")]
		public int halfYearNum { get; set; }		
		/// <summary>
		/// 签年度合同房间数
		/// </summary>
		[Description("签年度合同房间数")]
		public int yearNum { get; set; }		
		/// <summary>
		/// 在住房间合计数
		/// </summary>
		[Description("在住房间合计数")]
		public int allNum { get; set; }		
		/// <summary>
		/// 逾期交房租明细
		/// </summary>
		[Description("逾期交房租明细")]
		public string overdueInfo { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string note { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 数据所属部门
		/// </summary>
		[Description("数据所属部门")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }

        [Description("总入住率")]
        public decimal zrzl { get; set; }


        [Description("房间均价")]
        public decimal fjjj { get; set; }

        [Description("重复入住天数")]
        public int rezhuday { get; set; }
        [Description("重复入住明细")]
        public string rezhumx { get; set; }
        [Description("汇款合计")]
        public decimal hkhj { get; set; }
        [Description("汇款明细")]
        public string hkmx { get; set; }
        [Description("填报人")]
        public string rptman { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}