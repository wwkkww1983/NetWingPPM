var actionURL = '/MJDayRpt/ashx/MJDayRptHandler.ashx';
var formurl = '/MJDayRpt/html/MJDayRpt.html';
//定义要隐藏的字段用于动态显示（主要是为了方便开发人员调试）
var hiddenFields = new Array("KeyId", "depid", "ownner");
$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
    //标准显示
    $('#a_view').click(function () {
        for (var i = 0; i < hiddenFields.length; i++) {
            var field = hiddenFields[i];
            $("#list").datagrid('hideColumn', field);//加载完吧所有的列都隐藏
        }
    });
    //全部显示
    $('#a_allview').click(function () {
        for (var i = 0; i < hiddenFields.length; i++) {
            var field = hiddenFields[i];
            $("#list").datagrid('showColumn', field);//加载完吧所有的列都隐藏
        }
    });


    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });









});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "数据列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '系统ID', field: 'KeyId', sortable: true, width: '', hidden: false },
                { title: '填报时间', field: 'add_time', sortable: true, width: '200px', hidden: false },
                { title: '填报人', field: 'rptman', sortable: true, width: '100px', hidden: false },
                { title: '当日支出', field: 'dayMoneyOut', sortable: true, width: '', hidden: false },
                { title: '当日收入', field: 'dayMoneyIn', sortable: true, width: '', hidden: false },
                //{ title: '当日收入明细', field: 'dayMoneyInInfo', sortable: true, width: '', hidden: false },
                //{ title: '当日支出明细', field: 'dayMoneyOutInfo', sortable: true, width: '', hidden: false },
                { title: '收入合计', field: 'totalIn', sortable: true, width: '', hidden: false },
                { title: '支出合计', field: 'totalOut', sortable: true, width: '', hidden: false },
                { title: '余额合计', field: 'totalBalance', sortable: true, width: '', hidden: false },
                { title: '利润（扣除定金、押金）', field: 'totalProfit', sortable: true, width: '', hidden: false },
                { title: '截止上日实际余款', field: 'nowBalance', sortable: true, width: '', hidden: false },
                { title: '当日现金余额', field: 'nowCashBalance', sortable: true, width: '', hidden: false },
                { title: '截止当日实际余款', field: 'nowDayBalance', sortable: true, width: '', hidden: false },
                { title: '电瓶车停车数量', field: 'bicycleCar', sortable: true, width: '', hidden: false },
                { title: '自行车停车数量', field: 'bikeCar', sortable: true, width: '', hidden: false },
                { title: '截止当日预定人数', field: 'bookNum', sortable: true, width: '', hidden: false },
                { title: '预定最迟大概入住时间', field: 'inLashTime', sortable: true, width: '', hidden: false },
                { title: '截止当日空房数', field: 'freeRoom', sortable: true, width: '', hidden: false },
                //{ title: '截止当日空房明细', field: 'freeRoomInfo', sortable: true, width: '', hidden: false },
                { title: '截止当日空房原因', field: 'freeRoomWhat', sortable: true, width: '', hidden: false },
                { title: '当日退房数', field: 'quitRoomNum', sortable: true, width: '', hidden: false },
                { title: '当日退房明细', field: 'quitRoomInfo', sortable: true, width: '', hidden: false },
                { title: '是否安排入住', field: 'isIn', sortable: true, width: '', hidden: false },
                { title: '签月度合同房间数', field: 'monthNum', sortable: true, width: '', hidden: false },
                { title: '签季度合同房间数', field: 'quarterNum', sortable: true, width: '', hidden: false },
                { title: '签半年合同房间数', field: 'halfYearNum', sortable: true, width: '', hidden: false },
                { title: '签年度合同房间数', field: 'yearNum', sortable: true, width: '', hidden: false },
                { title: '在住房间合计数', field: 'allNum', sortable: true, width: '', hidden: false },
                //{ title: '逾期交房租明细', field: 'overdueInfo', sortable: true, width: '', hidden: false },
                //{ title: '备注', field: 'note', sortable: true, width: '', hidden: false },

                //{ title: '更新时间', field: 'up_time', sortable: true, width: '', hidden: false },
                { title: '数据所属部门', field: 'depid', sortable: true, width: '', hidden: false },
                { title: '数据所有者', field: 'ownner', sortable: true, width: '', hidden: false },

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //console.log("加载完隐藏不需要显示的列");
                for (var i = 0; i < hiddenFields.length; i++) {
                    var field = hiddenFields[i];
                    $("#list").datagrid('hideColumn', field);//加载完吧所有的列都隐藏
                }
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}
//模块初始化
function systeminit() {
    top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
    //当日收入明细初始化
    top.$("#txt_dayMoneyInInfo").next().hide();
    top.$("#srmx")
        .jsoneditor({
            schema: {
                "type": "array",
                "options": {
                    "disable_collapse": true
                },
                "format": "table",//table  grid tabs
                "title": "当日收入明细",
                //"uniqueItems": true,
                "items": {
                    "type": "object",
                    "title": "明细",
                    "properties": {
                        "房号": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "姓名": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "电话": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "备注": {
                            "type": "string",
                            "options": {
                                "input_width": "120px"
                            },
                        },
                        "金额(元)": {
                            "type": "integer",
                            "options": {
                                "input_width": "60px"
                            },
                        }
                    }
                },
            },
            theme: 'html'
        })
        .on('ready', function () {
            //// Get the value
            //var value = $(this).jsoneditor('value');

            //value.name = "John Smith";

            //// Set the value
            //$(this).jsoneditor('value', value);
        });
    //当日收入明细初始化

    top.$("#txt_freeRoomInfo").next().hide();    //截止当日空房明细    top.$("#kfmx")
        .jsoneditor({
            schema: {
                "type": "array",
                "options": {
                    "disable_collapse": true
                },
                "format": "table",//table  grid tabs
                "title": "截止当日空房明细",
                //"uniqueItems": true,
                "items": {
                    "type": "object",
                    "title": "明细",
                    "properties": {
                        "房号": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "空房原因": {
                            "type": "string",
                            "options": {
                                "input_width": "150px"
                            },
                        },
                        "空房天数": {
                            "type": "integer",
                            "options": {
                                "input_width": "60px"
                            },
                        },

                        "备注": {
                            "type": "string",
                            "options": {
                                "input_width": "150px"
                            },
                        },

                    }
                },
            },
            theme: 'html'
        })
        .on('ready', function () {
            //// Get the value
            //var value = $(this).jsoneditor('value');

            //value.name = "John Smith";

            //// Set the value
            //$(this).jsoneditor('value', value);
        });    //截止当日空房明细    //重复入住明细    top.$("#txt_rezhumx").next().hide();//隐藏编辑器    top.$("#rezhuinfo")
        .jsoneditor({
            schema: {
                "type": "array",
                "options": {
                    "disable_collapse": true
                },
                "format": "table",//table  grid tabs
                "title": "重复入住明细",
                //"uniqueItems": true,
                "items": {
                    "type": "object",
                    "title": "明细",
                    "properties": {
                        "房号": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "姓名": {
                            "type": "string",
                            "options": {
                                "input_width": "150px"
                            },
                        },
                        "联系电话": {
                            "type": "string",
                            "options": {
                                "input_width": "60px"
                            },
                        },

                        "备注": {
                            "type": "string",
                            "options": {
                                "input_width": "150px"
                            },
                        },

                    }
                },
            },
            theme: 'html'
        })
        .on('ready', function () {
            //// Get the value
            //var value = $(this).jsoneditor('value');

            //value.name = "John Smith";

            //// Set the value
            //$(this).jsoneditor('value', value);
        });    //重复入住明细    //当日支出明细初始化
    top.$("#txt_dayMoneyOutInfo").next().hide();
    top.$("#zcmx")
        .jsoneditor({
            schema: {
                "type": "array",
                "options": {
                    "disable_collapse": true
                },
                "format": "table",//table  grid tabs
                "title": "当日支出明细",
                //"uniqueItems": true,
                "items": {
                    "type": "object",
                    "title": "明细",
                    "properties": {
                        "房号": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "姓名": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "支出原因": {
                            "type": "string",
                            "options": {
                                "input_width": "120px"
                            },
                        },
                        "金额(元)": {
                            "type": "integer",
                            "options": {
                                "input_width": "60px"
                            },
                        }
                    }
                },
            },
            theme: 'html'
        })
        .on('ready', function () {
            //// Get the value
            //var value = $(this).jsoneditor('value');

            //value.name = "John Smith";

            //// Set the value
            //$(this).jsoneditor('value', value);
        });
    //当日支出明细初始化



    top.$("#txt_overdueInfo").next().hide();//隐藏编辑器
    top.$("#yuqiinfo")
        .jsoneditor({
            theme: 'html',
            schema: {//编辑器所需要的JSON Schema . 目前支持 版本 3 和版本 4
                "type": "array",
                "options": {
                    "disable_collapse": true
                },
                "format": "table",//table  grid tabs
                "title": "逾期交房明细",
                //"uniqueItems": true,
                "items": {
                    "type": "object",
                    "title": "明细",
                    "properties": {
                        "房号": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "姓名": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "电话": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "逾期天数": {
                            "type": "string",
                            "options": {
                                "input_width": "60px"
                            },
                        },
                        "店长处理意见": {
                            "type": "string",
                            "format": "textarea",
                            "options": {
                                "input_height": "50px"
                            },
                        }
                    }
                },
            }
        })
        .on('ready', function () {
            // Get the value
            //var value = top.$(this).jsoneditor('value');
            //value.name = "John Smith";
            // Set the value
            //top.$(this).jsoneditor('value', value);
        });



    top.$("#txt_hkmx").next().hide();//隐藏编辑器
    top.$("#hkinfo")
        .jsoneditor({
            theme: 'html',
            schema: {//编辑器所需要的JSON Schema . 目前支持 版本 3 和版本 4
                "type": "array",
                "options": {
                    "disable_collapse": true
                },
                "format": "table",//table  grid tabs
                "title": "汇款明细",
                //"uniqueItems": true,
                "items": {
                    "type": "object",
                    "title": "明细",
                    "properties": {
                        "汇款银行": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "用途": {
                            "type": "string",
                            "options": {
                                "input_width": "55px"
                            },
                        },
                        "姓名": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "电话": {
                            "type": "string",
                            "options": {
                                "input_width": "80px"
                            },
                        },
                        "汇款金额": {
                            "type": "integer",
                            "options": {
                                "input_width": "60px"
                            },
                        },
                        "备注": {
                            "type": "string",
                            "format": "textarea",
                            "options": {
                                "input_height": "50px"
                            },
                        }
                    }
                },
            }
        })
        .on('ready', function () {
            // Get the value
            //var value = top.$(this).jsoneditor('value');
            //value.name = "John Smith";
            // Set the value
            //top.$(this).jsoneditor('value', value);
        });




}




//把表格转为json字符串
function systemToJson() {
    var v = top.$("#yuqiinfo").jsoneditor('value');
    //alert(JSON.stringify(v));
    console.log("json:" + JSON.stringify(v));
    top.$("#txt_overdueInfo").textbox('setValue', JSON.stringify(v));
    //收入明细
    var srv = top.$("#srmx").jsoneditor('value');
    top.$("#txt_dayMoneyInInfo").textbox('setValue', JSON.stringify(srv));
    //支出明细
    var zcv = top.$("#zcmx").jsoneditor('value');
    top.$("#txt_dayMoneyOutInfo").textbox('setValue', JSON.stringify(zcv));

    //截止当日空房明细
    var kfmx = top.$("#kfmx").jsoneditor('value');
    top.$("#txt_freeRoomInfo").textbox('setValue', JSON.stringify(kfmx));
    //截止当日空房明细

    //重复入住明细
    var rezhu = top.$("#rezhuinfo").jsoneditor('value');
    top.$("#txt_rezhumx").textbox('setValue', JSON.stringify(rezhu));
    //重复入住明细

    //汇款明细
    var hkmx = top.$("#hkinfo").jsoneditor('value');
    top.$("#txt_hkmx").textbox('setValue', JSON.stringify(hkmx));
    //汇款明细


}
//把表格转为json字符串



var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 1000, height: 600, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                systeminit();//模块初始化添加和编辑公用 为了好看 逻辑清晰。模块化编程
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    systemToJson();//把表格转为json
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }

                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    systeminit();

                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                    top.$("#uiform").form('load', row);
                    top.$("#yuqiinfo").jsoneditor('value', JSON.parse(row.overdueInfo));//给逾期交房明细赋值
                    top.$("#kfmx").jsoneditor('value', JSON.parse(row.freeRoomInfo));//空房明细赋值
                    top.$("#zcmx").jsoneditor('value', JSON.parse(row.dayMoneyOutInfo));//支出明细赋值
                    top.$("#srmx").jsoneditor('value', JSON.parse(row.dayMoneyInInfo));//收入明细赋值
                    top.$("#rezhuinfo").jsoneditor('value', JSON.parse(row.rezhumx));//重复入住明细
                    top.$("#hkinfo").jsoneditor('value', JSON.parse(row.hkmx));





                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        systemToJson();//把表格转为json字符串



                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    } else {
                        msg.warning('请填写必填项！');
                    }

                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})



