using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJDayRptBll
    {
        public static MJDayRptBll Instance
        {
            get { return SingletonProvider<MJDayRptBll>.Instance; }
        }

        public int Add(MJDayRptModel model)
        {
            return MJDayRptDal.Instance.Insert(model);
        }

        public int Update(MJDayRptModel model)
        {
            return MJDayRptDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJDayRptDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJDayRptDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
