using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJWbtc")]
	[Description("外部停车")]
	public class MJWbtcModel
	{
				/// <summary>
		/// 关键ID
		/// </summary>
		[Description("关键ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 车型
		/// </summary>
		[Description("车型")]
		public string carType { get; set; }		
		/// <summary>
		/// 车牌号或车架号
		/// </summary>
		[Description("车牌号或车架号")]
		public string carNo { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 服务开始时间
		/// </summary>
		[Description("服务开始时间")]
		public DateTime order_starttime { get; set; }		
		/// <summary>
		/// 服务结束时间
		/// </summary>
		[Description("服务结束时间")]
		public DateTime order_endtime { get; set; }		
		/// <summary>
		/// 蓝牙卡押金
		/// </summary>
		[Description("蓝牙卡押金")]
		public decimal lanya_money { get; set; }		
		/// <summary>
		/// 门禁卡押金
		/// </summary>
		[Description("门禁卡押金")]
		public decimal menjin_money { get; set; }		
		/// <summary>
		/// 部门ID
		/// </summary>
		[Description("部门ID")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
		/// <summary>
		/// 联系人
		/// </summary>
		[Description("联系人")]
		public string connman { get; set; }		
		/// <summary>
		/// 联系电话
		/// </summary>
		[Description("联系电话")]
		public string mobile { get; set; }



        [Description("停车费")]
        public decimal tcf { get; set; }

        [Description("备注")]
        public string note { get; set; }

        [Description("付款账号")]
        public string account { get; set; }

        [Description("付款账号ID")]
        public int accountid { get; set; }

        [Description("停车费用科目")]
        public int subjectid { get; set; }

        [Description("初始停车单号")]
        public string edNumber { get; set; }

        [Description("退费时单号")]
        public string tuiedNumber { get; set; }
        [Description("首次停车时间")]
        public DateTime firsttime { get; set; }

        [Description("最后一次续费单号")]
        public string lastedNumber { get; set; }

        [Description("最后一次续费时间")]
        public DateTime lastxftime { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}