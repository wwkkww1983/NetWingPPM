using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("Tptypelate")]
	[Description("模板主键")]
	public class TptypelateModel
	{
				/// <summary>
		/// 模板主键ID
		/// </summary>
		[Description("模板主键ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 模板名称
		/// </summary>
		[Description("模板名称")]
		public string flowname { get; set; }		
		/// <summary>
		/// 创建时间
		/// </summary>
		[Description("创建时间")]
		public DateTime addtime { get; set; }		
		/// <summary>
		/// 修改时间
		/// </summary>
		[Description("修改时间")]
		public DateTime updatetime { get; set; }		
		/// <summary>
		/// json模板
		/// </summary>
		[Description("json模板")]
		public string jsontypelate { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}