using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class TptypelateBll
    {
        public static TptypelateBll Instance
        {
            get { return SingletonProvider<TptypelateBll>.Instance; }
        }

        public int Add(TptypelateModel model)
        {
            return TptypelateDal.Instance.Insert(model);
        }

        public int Update(TptypelateModel model)
        {
            return TptypelateDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return TptypelateDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return TptypelateDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
