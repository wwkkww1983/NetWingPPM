﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="fenqifk.aspx.cs" Inherits="NetWing.BPM.Admin.JydOrder.fenqifk" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>分期付款</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../jydorderfenqifk/layui/css/layui.css" media="all">
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">金额输入框</label>
            <div class="layui-input-block">
                <input type="text" id="je" name="title" required lay-verify="required" placeholder="请输入金额" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" onclick="fenqifk()" lay-submit lay-filter="formDemo">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <script>
        function fenqifk() {
            layer.load();
            $.ajax({
                url: '/JydModleOrder/ashx/ajax_submit.ashx',
                type: 'post',
                dataType: 'JSON',
                data: {
                    action: 'fenqijefk',
                    orderid: '<%=Request["ai"]%>',
                    je: $('#je').val(),
                },
                success: function (q, e) {
                    console.log(e);
                    if (e.status == 1) {
                         layer.close(layer.load());
                    }
                }
            })
        }
    </script>
    <table class="layui-hide" id="test" lay-filter="test"></table>
    <div class="layui-table-page" id="table-page"></div>
    <script type="text/html" id="toolbarDemo">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="getCheckData">获取选中行数据</button>
        </div>
    </script>


    <script src="../jydorderfenqifk/layui/layui.js" charset="utf-8"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->

    <script>
        var form,
            table,
            laydate,
            laypage,
            tableIns;
        var pageindex = 1;//页数
        var pagesize = 9;//每页条数
        layui.use(['form', 'table', 'laydate', 'laypage'], function () {
            form = layui.form;
            table = layui.table;
            laydate = layui.laydate;
            laypage = layui.laypage;

            tableIns = table.render({
                elem: '#test'
                , limit: pagesize
                , size: 'sm'
                , toolbar: true
                , defaultToolbar: ['print', 'exports']
                , title: '分期付款'
                , text: {
                    none: '暂无相关数据'
                }
                , cols: [[
                    { field: 'KeyId', width: 80, title: 'ID', sort: true }
                    , { field: 'comname', width: 150, title: '付款时间' }
                    , { field: 'orderid', width: 130, title: '接件单号', sort: true }
                    , { field: 'yspmc', width: 130, title: '印刷品名称' }
                    , { field: 'sl', width: 80, title: '数量' }
                    , { field: 'dj', width: 80, title: '单价', sort: true }
                    , { field: 'je', width: 130, title: '本次付款金额', sort: true }
                    , { field: 'jexx', width: 130, title: '本次付款小计' }
                    , { field: 'yufuk', width: 135, title: '已付款', sort: true }
                ]]
            });
            //初始化数据
            onSeach();
        });
        function onSeach() {
            layer.load();
            $.ajax({
                url: '/JydModleOrder/ashx/ajax_submit.ashx',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'fenqifk',
                    pageindex: pageindex,
                    pagesize: pagesize,
                    orderid: '<%=Request["ai"]%>',
                },
                success: function (d) {
                    tableIns.reload({
                        data: d.data
                        , limit: pagesize
                    })
                    laypage.render({
                        elem: 'table-page'
                        , count: d.datanum
                        , curr: pageindex
                        , limit: pagesize
                        , limits: [10, 20, 40, 60, 80, 100, 200, 500, 1000]
                        , groups: 2
                        , first: '首页'
                        , last: '尾页'
                        , layout: ['prev', 'page', 'next', 'refresh']
                        , jump: function (obj, first) {
                            if (!first) {
                                pageindex = obj.curr;
                                pagesize = obj.limit;
                                onSeach();
                            }
                        }
                    });
                    layer.close(layer.load());
                },
                error: function (e) {
                    layer.close(layer.load());
                }
            })
        }
    </script>
</body>
</html>
