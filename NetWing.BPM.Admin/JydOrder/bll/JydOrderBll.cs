using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydOrderBll
    {
        public static JydOrderBll Instance
        {
            get { return SingletonProvider<JydOrderBll>.Instance; }
        }

        public int Add(JydOrderModel model)
        {
            return JydOrderDal.Instance.Insert(model);
        }

        public int Update(JydOrderModel model)
        {
            return JydOrderDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydOrderDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydOrderDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
