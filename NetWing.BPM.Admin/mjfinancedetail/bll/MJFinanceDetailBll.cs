using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJFinanceDetailBll
    {
        public static MJFinanceDetailBll Instance
        {
            get { return SingletonProvider<MJFinanceDetailBll>.Instance; }
        }

        public int Add(MJFinanceDetailModel model)
        {
            return MJFinanceDetailDal.Instance.Insert(model);
        }

        public int Update(MJFinanceDetailModel model)
        {
            return MJFinanceDetailDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJFinanceDetailDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJFinanceDetailDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
