using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("MJFinanceDetail")]
    [Description("财务子表")]
    public class MJFinanceDetailModel
    {
        /// <summary>
        /// 编号
        /// </summary>
        [Description("编号")]
        public int KeyId { get; set; }
        /// <summary>
        /// 主表ID
        /// </summary>
        [Description("主表ID")]
        public int financeId { get; set; }
        /// <summary>
        /// 收支类型
        /// </summary>
        [Description("收支类型")]
        public string ftype { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [Description("编号")]
        public string edNumber { get; set; }
        /// <summary>
        /// 科目
        /// </summary>
        [Description("科目")]
        public string subject { get; set; }
        /// <summary>
        /// 科目ID
        /// </summary>
        [Description("科目ID")]
        public int subjectid { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        [Description("金额")]
        public decimal sumMoney { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        [Description("部门")]
        public string dep { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        [Description("部门ID")]
        public int depid { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string note { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        [Description("添加时间")]
        public DateTime add_time { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Description("更新时间")]
        public DateTime up_time { get; set; }
        /// <summary>
        /// 数据所有者ID
        /// </summary>
        [Description("数据所有者ID")]
        public int ownner { get; set; }
        /// <summary>
        /// 付款方式：年付 月付 季付
        /// </summary>
        [Description("付款方式")]
        public string paytype { get; set; }

        [Description("下次付款日期")]
        public DateTime nextpaydate { get; set; }

        [Description("服务或产品数量")]
        public int num { get; set; }

        [Description("单价")]
        public decimal sprice { get; set; }
        [Description("服务开始时间")]
        public DateTime stime{get;set;}


        [Description("服务到期时间")]
        public DateTime etime { get; set; }
        [Description("用户ID")]
        public int userid { get; set; }
        [Description("用户姓名")]
        public string username { get; set; }
        [Description("经手人ID")]
        public int contactid { get; set; }
        [Description("经手人姓名")]
        public string contact { get; set; }
        [Description("手机号")]
        public string mobile { get; set; }

        [Description("房间id")]
        public int roomid { get; set; }
        [Description("房间号")]
        public string roomno { get; set; }
        //原则上一个服务对应一个明细订单
        [Description("订单/服务明细ID")]
        public int orderdetailid { get; set; }

        [Description("车牌或车架号")]
        public string carno { get; set; }

        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}