﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="center.aspx.cs" Inherits="NetWing.BPM.Admin.report.center" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<%@ Import Namespace="NetWing.BPM.Core" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="tt" class="easyui-tabs" style="width: 100%; height: auto">
        <div title="银行账户余额分析" style="padding: 20px; display: none;">
            <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
            <div id="main" style="width: 600px; height: 600px;"></div>
            <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
                string sqla = "select sum(accountBalance) as a from MJBankAccount where";
                //数据权限筛选s
                string sqlwherea = " 1=1 ";
                if (SysVisitor.Instance.cookiesIsAdmin == "False")
                { //判断是否是超管如果是超管理，所有显示
                    sqlwherea = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
                }
                //数据权限筛选e
                sqla = sqla + sqlwherea;
                DataRow dra = SqlEasy.ExecuteDataRow(sqla);

                //得到财务表实付金额
                string sqlb = "select sum(payment) as b from MJFinanceMain where ";
                sqlb = sqlb + sqlwherea;
                DataRow drb = SqlEasy.ExecuteDataRow(sqlb);

            %>



            系统财务自动比对：账户总余额<%=dra["a"].ToString() %> 财务主表总余额<%=drb["b"].ToString() %> 
            <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
                string sql = "select * from MJBankAccount where";
                //数据权限筛选s
                string sqlwhere = " 1=1 ";
                if (SysVisitor.Instance.cookiesIsAdmin == "False")
                { //判断是否是超管如果是超管理，所有显示
                    sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
                }
                //数据权限筛选e
                sql = sql + sqlwhere;
                DataTable dt = SqlEasy.ExecuteDataTable(sql);

            %>
            <script type="text/javascript">
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('main'));
                option = {
                    title: {
                        text: '银行账户余额',
                        subtext: '实时统计',
                        x: 'center'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            dataZoom: {
                                yAxisIndex: 'none'
                            },
                            dataView: { readOnly: false },
                            magicType: { type: ['line', 'bar'] },
                            restore: {},
                            saveAsImage: {}
                        }
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data: [<%foreach (DataRow dr in dt.Rows)
                {%>'<%=dr["accountName"].ToString()%>余额：<%=dr["accountBalance"].ToString()%>元',<%}%>'其他']
                    },
                    series: [
                        {
                            name: '单位：元',
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            data: [
                            <%foreach (DataRow dr in dt.Rows)
                {%>
                                { value: <%=dr["accountBalance"].ToString()%>, name: '<%=dr["accountName"].ToString()%>余额：<%=dr["accountBalance"].ToString()%>元' },
                            <%}%>
                                { value: 0, name: '其他' }
                            ],
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };



                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
            // Enable data zoom when user click bar.

            </script>
        </div>
        <div title="收支分析" data-options="closable:true" style="overflow: auto; padding: 20px; display: none;">
            <div id="inout" style="width: 1200px; height: 600px;"></div>
            <script>
                // 基于准备好的dom，初始化echarts实例
                <%
                //为了报表好看这里吧支出弄成正数
                sql = "select  COALESCE(sum(case when summoney<0 then summoney end),0) as outmoney,COALESCE(sum(case when summoney>0 then summoney end),0) as inmoney,CONVERT(varchar(100), add_time, 23) as d from MJFinanceDetail  where add_time>=dateadd(month,-1,getdate()) and depid="+SysVisitor.Instance.UserDepId+" group by CONVERT(varchar(100), add_time, 23) order by d desc";
                DataTable inOutDt = SqlEasy.ExecuteDataTable(sql);

                %>

                var inoutChart = echarts.init(document.getElementById('inout'));
                option = {
                    title: {
                        text: '最近一周收支图',
                        subtext: ''
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['最高气温', '最低气温']
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            dataZoom: {
                                yAxisIndex: 'none'
                            },
                            dataView: { readOnly: false },
                            magicType: { type: ['line', 'bar'] },
                            restore: {},
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,

                        data: [<%foreach (DataRow dr in inOutDt.Rows)
                {%>'<%=dr["d"].ToString()%>',<%}%>'N日']
                    },
                    yAxis: {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}元'
                        }
                    },
                    series: [
                        {
                            name: '支出',
                            type: 'line',
                            data: [<%foreach (DataRow dr in inOutDt.Rows)
                {%><%=dr["outmoney"].ToString().Replace("-","")%>,<%}%>0],
                            markPoint: {
                                data: [
                                    { type: 'max', name: '最大值' },
                                    { type: 'min', name: '最小值' }
                                ]
                            },
                            markLine: {
                                data: [
                                    { type: 'average', name: '平均值' }
                                ]
                            }
                        },
                        {
                            name: '收入',
                            type: 'line',
                            data: [<%foreach (DataRow dr in inOutDt.Rows)
                {%><%=dr["inmoney"].ToString()%>,<%}%>0],
                            markPoint: {
                                data: [
                                    { name: '周最低', value: -2, xAxis: 1, yAxis: -1.5 }
                                ]
                            },
                            markLine: {
                                data: [
                                    { type: 'average', name: '平均值' },
                                    [{
                                        symbol: 'none',
                                        x: '90%',
                                        yAxis: 'max'
                                    }, {
                                        symbol: 'circle',
                                        label: {
                                            normal: {
                                                position: 'start',
                                                formatter: '最大值'
                                            }
                                        },
                                        type: 'max',
                                        name: '最高点'
                                    }]
                                ]
                            }
                        }
                    ]
                };
                // 使用刚指定的配置项和数据显示图表。
                inoutChart.setOption(option);
            </script>
        </div>
        <!--隐藏不要的
        <div title="Tab3" data-options="iconCls:'icon-reload',closable:true" style="padding: 20px; display: none;">
            tab3   
        </div>
        -->
    </div>


</asp:Content>


