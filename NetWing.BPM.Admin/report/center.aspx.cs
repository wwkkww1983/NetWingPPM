﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Model;
using NetWing.BPM.Core.Dal;
using NetWing.Common;
using NetWing.BPM.Core;

namespace NetWing.BPM.Admin.report
{
    public partial class center : NetWing.BPM.Core.BasePage.BpmBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //更新银行余额 因为这里账户id都是唯一的 这里不考虑数据权限
            SqlDataReader dr = SqlEasy.ExecuteDataReader("select * from MJBankAccount");
            while (dr.Read())
            {
                string sumsqlb = "select isnull(sum(shifub),0) shifub  from MJFinanceMain where accountidb=" + dr["Keyid"].ToString() + "";
                string sumsqlc = "select isnull(sum(shifuc),0) shifuc  from MJFinanceMain where accountidc=" + dr["Keyid"].ToString() + "";
                string sumsqld = "select isnull(sum(shifud),0) shifud  from MJFinanceMain where accountidd=" + dr["Keyid"].ToString() + "";
                decimal moneyb = (decimal)SqlEasy.ExecuteScalar(sumsqlb);
                decimal moneyc = (decimal)SqlEasy.ExecuteScalar(sumsqlc);
                decimal moneyd = (decimal)SqlEasy.ExecuteScalar(sumsqld);
                SqlEasy.ExecuteNonQuery("update MJBankAccount set accountBalance="+ (moneyb + moneyc+ moneyd)+ "  where keyid="+ dr["keyid"].ToString() + "");

                LogModel log = new LogModel();
                log.BusinessName = "统计银行余额-从财务分析";
                log.OperationIp = PublicMethod.GetClientIP();
                log.OperationTime = DateTime.Now;
                log.PrimaryKey = "";
                log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                log.TableName = "";
                log.note = "账户名:"+dr["accountName"].ToString()+"depid:"+ dr["depid"].ToString()+"账户ID："+ dr["keyid"].ToString()+"更新余额："+(moneyb + moneyc + moneyd);
                log.OperationType = (int)OperationType.Other;
                LogDal.Instance.Insert(log);
            }


        }
    }
}