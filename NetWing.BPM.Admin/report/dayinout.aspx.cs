﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;

namespace NetWing.BPM.Admin.report
{
    public partial class dayinout : System.Web.UI.Page
    {
        public static DataTable dt = null;
        public string depname = "";
        public string dayfrom = "";
        public string dayto = "";
        public string type = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //MJFinanceMain
                string sql = "select * from MJFinanceMain where ";

                //数据权限筛选s
                string sqlwhere = " 1=1 ";

                if (SysVisitor.Instance.cookiesIsAdmin == "False")
                { //判断是否是超管如果是超管理，所有显示
                    sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
                }
                //数据权限筛选e
                sql = sql + sqlwhere;
                if (string.IsNullOrEmpty(dayfrom) && string.IsNullOrEmpty(dayto))
                {
                    dayfrom = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
                    dayto = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
                }
                sql = sql + " and (add_time BETWEEN '" + dayfrom + "' and '" + dayto + "')";
                dt = SqlEasy.ExecuteDataTable(sql);
                DataRow depdr = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + SysVisitor.Instance.cookiesUserDepId + "");
                if (depdr != null)
                {
                    depname = depdr["DepartmentName"].ToString();
                }


            }



        }
        public static DataTable getdt(string sql)
        {

            return SqlEasy.ExecuteDataTable(sql);

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            dayfrom = Request["dayfrom"];
            dayto = Request["dayto"];
            type = Request["type"];

            //MJFinanceMain
            string sql = "select *,(select COUNT(*)  from MJFinanceDetail where financeId=MJFinanceMain.KeyId and summoney<0) t from MJFinanceMain where";
            //得到一个t t大于1 说明是支出
            //数据权限筛选s
            string sqlwhere = " 1=1 ";
            if (SysVisitor.Instance.cookiesIsAdmin == "False")
            { //判断是否是超管如果是超管理，所有显示
                sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            //数据权限筛选e
            sql = sql + sqlwhere;
            if (!string.IsNullOrEmpty(dayfrom) && !string.IsNullOrEmpty(dayto))
            {
                sql = sql + " and (add_time BETWEEN '" + dayfrom + "' and '" + dayto + "')";
            }
            dt = SqlEasy.ExecuteDataTable(sql);
            switch (type)
            {
                case "支出":
                    dt = GetNewDataTable(dt, "t>0", "keyid asc");
                    break;

                //case "收入":
                //    dt = GetNewDataTable(dt, "t=0", "keyid asc");
                //    break;
                default:
                    break;
            }

           




        }


        /// 执行DataTable中的查询返回新的DataTable
        /// </summary>
        /// <param name="dt">源数据DataTable</param>
        /// <param name="condition">查询条件</param>
        /// <returns></returns>
        private DataTable GetNewDataTable(DataTable dt, string condition, string sortstr)
        {
            DataTable newdt = new DataTable();
            newdt = dt.Clone();
            DataRow[] dr = dt.Select(condition, sortstr);
            for (int i = 0; i < dr.Length; i++)
            {
                newdt.ImportRow((DataRow)dr[i]);
            }
            return newdt;//返回的查询结果
        }

    }
}