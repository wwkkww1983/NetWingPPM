﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="daytui.aspx.cs" Inherits="NetWing.BPM.Admin.report.daytui" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.BPM.Core" %>
<%@ Import Namespace="NetWing.BPM.Admin.common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
            <%=WebExtensions.CombresLink(SysVisitor.Instance.ThemeName) %>
    <%=WebExtensions.CombresLink("siteCss") %>
    <%=WebExtensions.CombresLink("alertifyCss") %>

    <%=WebExtensions.CombresLink("jquery") %>
    <%=WebExtensions.CombresLink("siteJs") %>
    <script type="text/javascript">
        if (top.location == self.location) {
            top.location = '/';
        }
        var urlRoot = ""; // 设置后台程序所在的目录 如： /admin
        var currend_Date = '<%=DateTime.Now.ToString("yyyy-MM-dd") %>';
        var PAGESIZE = <%=SysVisitor.Instance.GridRows%>;


        $(function () {
            $('#toolbar,.toolbar').css({
                height: '28px',
                //background:#efefef;
                padding: '1px 2px',
                'padding-bottom': '0px'
                //,'border-bottom':'1px solid #ccc'
            });
        });


    </script>
</head>
    <style>
.biaoge td{border:#000 1px solid; margin-top:-1px;display: block; border-bottom: none; border-left: none; border-right: none;}
#form1 table tr td{line-height:24px;}
#form1 table{font-family:"微软雅黑";font-size:14px; }
</style>
<body>
     <form id="form1" runat="server">
        <div style="text-align:center;"><h1>Me+国际青年公寓<%=depname %>退押金、定金表</h1>

            从：<input  id="dayfrom" name="dayfrom" value="<%=dayfrom %>"  type= "text" class= "easyui-datetimebox" required ="required"/>到：<input  id="dayto" name="dayto"  value="<%=dayto %>" type= "text" class= "easyui-datetimebox" required ="required"/>  
                <asp:Button ID="Button1" runat="server"  Text="查询" OnClick="Button1_Click" />
                <br /><br />
            <table width="90%" border="1" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
  <tr>
    <td>序号</td>
    <td>日期</td>
    <td>房号</td>
    <td>姓名</td>
    <td>退房间押金</td>
    <td>退门禁卡押金</td>
    <td>退蓝牙卡押金</td>
    <td>退电拼车押金</td>
    <td>退水桶押金</td>
    <td>退定金</td>
  </tr>
<%  int k = 1;
    foreach (DataRow dr  in dt.Rows)
    {%>

  <% DataRow udr = mjcommon.getDataRow("select * from MJFinanceMain where keyid="+dr["financeid"].ToString()+""); %>
  <tr>
    <td><%=k %></td>
    <td><%=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy年MM月dd日") %></td>
    <td><%=dr["roomno"].ToString()%></td>
    <td><%=udr["unit"].ToString() %></td>
    <%if (dr["subjectid"].ToString() == "21")//房间押金
        {%>
      <td><%=decimal.Round(decimal.Parse( dr["summoney"].ToString()),2) %></td>
        <%} %>
      <%else
      { //没有还是要输出一个空%>
      <td>&nbsp;</td>
      <%} %>
 
    <%if (dr["subjectid"].ToString() == "22")//门禁卡押金
        {%>
      <td><%=decimal.Round(decimal.Parse( dr["summoney"].ToString()),2) %></td>
        <%} %>
      <%else
      { //没有还是要输出一个空%>
      <td>&nbsp;</td>
      <%} %>
    

    <%if (dr["subjectid"].ToString() == "35")//退蓝牙卡押金
        {%>
      <td><%=decimal.Round(decimal.Parse( dr["summoney"].ToString()),2) %></td>
        <%} %>
      <%else
      { //没有还是要输出一个空%>
      <td>&nbsp;</td>
      <%} %>
    <%if (dr["subjectid"].ToString() == "29")//退电瓶车押金
        {%>
      <td><%=decimal.Round(decimal.Parse( dr["summoney"].ToString()),2) %></td>
        <%} %>
      <%else
      { //没有还是要输出一个空%>
      <td>&nbsp;</td>
      <%} %>
    <%if (dr["subjectid"].ToString() == "32")//退水桶押金
        {%>
      <td><%=decimal.Round(decimal.Parse( dr["summoney"].ToString()),2) %></td>
        <%} %>
      <%else
      { //没有还是要输出一个空%>
      <td>&nbsp;</td>
      <%} %>
    <%if (dr["subjectid"].ToString() == "19")//退预约定金
        {%>
      <td><%=decimal.Round(decimal.Parse( dr["summoney"].ToString()),2) %></td>
        <%} %>
      <%else
      { //没有还是要输出一个空%>
      <td>&nbsp;</td>
      <%} %>
  </tr>
<%
        k = k + 1;
    } %>

  <tr>
    <td colspan="4">分项合计</td>
    <td><!--房间押金--><%=dt.Compute("Sum(summoney)","subjectid=21")%></td>
    <td><!--门禁卡押金--><%=dt.Compute("Sum(summoney)","subjectid=22") %></td>
    <td><!--蓝牙卡押金--><%=dt.Compute("Sum(summoney)","subjectid=35") %></td>
    <td><!--电拼车押金--><%=dt.Compute("Sum(summoney)","subjectid=29")%></td>
    <td><!--水桶押金--><%=dt.Compute("Sum(summoney)","subjectid=32")%></td>
    <td><!--预约定金--><%=dt.Compute("Sum(summoney)","subjectid=19")%></td>
  </tr>
  <tr>
    <td colspan="4">总合计</td>
    <td colspan="6"><%=dt.Compute("Sum(summoney)","subjectid in(19,32,29,35,22,21)")%></td>
  </tr>
</table>
        </div>
    </form>
</body>
</html>
