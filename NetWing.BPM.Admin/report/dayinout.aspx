﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dayinout.aspx.cs" Inherits="NetWing.BPM.Admin.report.dayinout" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.BPM.Core" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
        <%=WebExtensions.CombresLink(SysVisitor.Instance.ThemeName) %>
    <%=WebExtensions.CombresLink("siteCss") %>
    <%=WebExtensions.CombresLink("alertifyCss") %>

    <%=WebExtensions.CombresLink("jquery") %>
    <%=WebExtensions.CombresLink("siteJs") %>
    <script type="text/javascript">
        if (top.location == self.location) {
            top.location = '/';
        }
        var urlRoot = ""; // 设置后台程序所在的目录 如： /admin
        var currend_Date = '<%=DateTime.Now.ToString("yyyy-MM-dd") %>';
        var PAGESIZE = <%=SysVisitor.Instance.GridRows%>;


        $(function () {
            $('#toolbar,.toolbar').css({
                height: '28px',
                //background:#efefef;
                padding: '1px 2px',
                'padding-bottom': '0px'
                //,'border-bottom':'1px solid #ccc'
            });
        });


    </script>
</head>
<style>
.biaoge td{border:#000 1px solid; margin-top:-1px;display: block; border-bottom: none; border-left: none; border-right: none;}
#form1 table tr td{line-height:24px;}
#form1 table{font-family:"微软雅黑";font-size:14px; }
</style>
<body>
    <form id="form1" runat="server">
        <div style="text-align:center;"><h1>Me+国际青年公寓<%=depname %>日常收入明细表</h1>

            从：<input  id="dayfrom" name="dayfrom" value="<%=dayfrom %>"  type= "text" class= "easyui-datetimebox" required ="required"/>到：<input  id="dayto" name="dayto"  value="<%=dayto %>" type= "text" class= "easyui-datetimebox" required ="required"/>  <select id="cc" class="easyui-combobox" name="type" style="width:100px;">  
        
        <%if (!string.IsNullOrEmpty(type))
                


        {%>
    <option value="<%=type %>"><%=type %></option> 
        <%} %>
    <option value="收支">收支</option>  
    <option value="收入">收入</option>  
    <option value="支出">支出</option>  

</select>
            <asp:Button ID="Button1" runat="server"  Text="查询" OnClick="Button1_Click" />
            <br /><br />
            <table width="90%" border="1" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
  <tr>
    <td>房号</td>
    <td>姓名</td>
    <td>收费项目</td>
    <td>金额</td>
    <td>付款方式</td>
    <td>备注</td>
  </tr>
  <!--循环开始-->
    <%
    decimal moneyall = 0;
    decimal money = 0;
    %>
  <%foreach (DataRow dr in dt.Rows) {%>
  <%  string sql = "select * from MJFinanceDetail where 1=1";
      sql = sql + " and  financeId=" + dr["keyid"].ToString() + "";
      if (!string.IsNullOrEmpty(type))
      {
          switch (type)
          {
              case "收入":
                  sql = sql + " and summoney>=0 ";
                  break;
              case "支出":
                  sql = sql + " and summoney<0 ";
                  break;

              default:
                  break;
          }
      }
      DataTable ddt = getdt(sql);
      int drows = ddt.Rows.Count;//明细表有几行

      %>    
  <tr>
    <td><%=dr["roomno"].ToString() %></td>
    <td><%=dr["unit"].ToString() %></td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="biaoge">
        <%foreach (DataRow ddr in ddt.Rows){%>
          <tr>
            <td><%=ddr["subject"].ToString() %></td>
          </tr>
        <%} %>
        </table>
    </td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="biaoge">
        <%foreach (DataRow ddr in ddt.Rows){%>
          <tr>
            <td><%money = decimal.Round(decimal.Parse(ddr["summoney"].ToString()), 2); %><%=money %></td>
          </tr>
        <%moneyall = moneyall + money; %>
        <%} %>
        </table></td>
    <td><%=dr["account"].ToString() %></td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="biaoge">
        <%foreach (DataRow ddr in ddt.Rows){%>
          <tr>
            <td><%=ddr["note"].ToString() %></td>
          </tr>
        <%} %>
        </table></td>
  </tr>




<% } %>
<!--合计栏-->
  <tr>
    <td colspan="6">合计：<%=moneyall%></td>
  </tr>
<!--合计栏-->
<!--循环结束-->
</table>
        </div>
    </form>
</body>
</html>
