﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<script src="/scripts/echarts.min.js"></script>
<script src="/scripts/jquery-1.10.2.min.js"></script>
<body>
    <form id="form1" runat="server">
        <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
        <div id="main" style="width: 800px; height: 600px; "></div>
        <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
            string sql = "select sum(jrsg) jrsg,sgd from y_rz group by sgd";
            DataTable dt = SqlEasy.ExecuteDataTable(sql);

            %>


        <script type="text/javascript">
            // 基于准备好的dom，初始化echarts实例
            var myChart = echarts.init(document.getElementById('main'));





            option = {
                title: {
                    text: '各乡镇完成收购任务情况',
                    subtext: '数据来源于点长日志',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: [<%foreach (DataRow dr  in dt.Rows){%>'<%=dr["sgd"].ToString()%>',<%}%>'其他']
                },
                series: [
                    {
                        name: '完成任务单位：公斤',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: [
                            <%foreach (DataRow dr  in dt.Rows){%>
                            { value: <%=dr["jrsg"].ToString()%>, name: '<%=dr["sgd"].ToString()%>' },
                            <%}%>
                            { value: 0, name: '其他' }
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };



            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
            // Enable data zoom when user click bar.
      
        </script>
      

 
    </form>
</body>
</html>
