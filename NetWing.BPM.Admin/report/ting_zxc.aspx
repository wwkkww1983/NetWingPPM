﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ting_zxc.aspx.cs" Inherits="NetWing.BPM.Admin.report.ting_zxc" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.BPM.Core" %>
<%@ Import Namespace="NetWing.BPM.Admin.common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        #form1 {
            font-family: "微软雅黑";
        }

            #form1 table {
                font-family: "微软雅黑";
                font-size: 14px;
            }
    </style>
    <script>
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <a id="btn" href="#" class="easyui-print" onclick="printWin();" data-options="iconCls:'icon-search'">打印</a>
        <div id="content" style="text-align: center;">
            <div style="margin: 0 auto; height: 50px; line-height: 50px; text-align: center; background: #fff; font-size: 30px;">
                Ｍe+国际青年公寓（<%=depname %>）自行车停车表
            </div>
            <table width="90%" border="1" cellspacing="0" cellpadding="0" style="text-align: center; margin: 0 auto;">
                <tr>
                    <td>房号</td>
                    <td>业主</td>
                    <td>车牌或车架号</td>
                    <td>起止时间</td>
                    <td>自行车数</td>
                    <td>电话号码</td>
                    <td>备注</td>
                </tr>
                <%
                    int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {%>


                <tr>
                    <%DataRow udr = mjcommon.getDataRow("select * from MJUser where keyid=" + dr["usersid"].ToString() + "");%>
                    <td><%=dr["roomno"].ToString() %></td>
                    <td><%=dr["users"].ToString() %></td>
                    <td><%=dr["carno"].ToString() %></td>
                    <td><%=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy-MM-dd") %>到<%=DateTime.Parse(dr["exp_time"].ToString()).ToString("yyyy-MM-dd") %></td>
                    <td><%=dr["num"].ToString() %></td>
                    <td><%=udr["tel"].ToString() %></td>
                    <td><%=dr["note"].ToString() %></td>
                </tr>
                <%
                        i = i + 1;
                    } %>

                <%
                    int j = 0;
                    foreach (DataRow wbdr in wbdt.Rows)
                    {%>


                <tr>

                    <td>外部</td>
                    <td><%=wbdr["connman"].ToString() %></td>
                    <td><%=wbdr["carno"].ToString() %></td>
                    <td><%=DateTime.Parse(wbdr["order_starttime"].ToString()).ToString("yyyy-MM-dd") %>到<%=DateTime.Parse(wbdr["order_endtime"].ToString()).ToString("yyyy-MM-dd") %></td>
                    <td>1</td>
                    <td><%=wbdr["mobile"].ToString() %></td>
                    <td><%=wbdr["note"].ToString() %></td>
                </tr>
                <%
                        j = j + 1;
                    } %>



                <tr>
                    <td colspan="5">合计</td>
                    <td><%=i+j %>辆</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
