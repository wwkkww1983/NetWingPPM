﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;

namespace NetWing.BPM.Admin.report
{
    public partial class ting_zxc : System.Web.UI.Page
    {
        public string depname = "";
        public DataTable dt = null;
        public DataTable wbdt = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            DataRow depdr = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + SysVisitor.Instance.cookiesUserDepId + "");
            if (depdr != null)
            {
                depname = depdr["DepartmentName"].ToString();
            }

            string sql = "select * from MJOrderDetail where serviceid=13 ";
            //数据权限筛选s
            string sqlwhere = " and 1=1  ";
            if (SysVisitor.Instance.cookiesIsAdmin == "False")
            { //判断是否是超管如果是超管理，所有显示
                sqlwhere = " and (depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            //数据权限筛选e
            sql = sql + sqlwhere;
            dt = SqlEasy.ExecuteDataTable(sql);


            string wbsql = "select * from MJWbtc where depid=" + SysVisitor.Instance.cookiesUserDepId + " and subjectid=13 or (tuiedNumber<>'' and tuiedNumber is null)";
            //数据权限筛选s
            string wbsqlwhere = " and 1=1  ";
            if (SysVisitor.Instance.cookiesIsAdmin == "False")
            { //判断是否是超管如果是超管理，所有显示
                wbsqlwhere = " and (depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            //数据权限筛选e
            wbsql = wbsql + wbsqlwhere;
            wbdt = SqlEasy.ExecuteDataTable(wbsql);

        }
    }
}