﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;

namespace NetWing.BPM.Admin.report
{
    public partial class daytui : System.Web.UI.Page
    {
        public static DataTable dt = null;
        public string depname = "";
        public string dayfrom = "";
        public string dayto = "";
        public string type = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (string.IsNullOrEmpty(dayfrom) && string.IsNullOrEmpty(dayto))
                {
                    dayfrom = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
                    dayto = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
                }
                DataRow depdr = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + SysVisitor.Instance.cookiesUserDepId + "");
                if (depdr != null)
                {
                    depname = depdr["DepartmentName"].ToString();
                }

                string sql = "select * from MJFinanceDetail where ";
                //数据权限筛选s
                string sqlwhere = " 1=1 ";
                if (SysVisitor.Instance.cookiesIsAdmin == "False")
                { //判断是否是超管如果是超管理，所有显示
                    sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
                }
                //数据权限筛选e
                sql = sql + sqlwhere;
                sql = sql + " and summoney<0  and subjectid in (19,20,21,22,23,24,25,26,27,28,29,30,31,32,35)";
                dt = SqlEasy.ExecuteDataTable(sql);

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            dayfrom = Request["dayfrom"];
            dayto = Request["dayto"];

            string sql = "select * from MJFinanceDetail where ";

            //数据权限筛选s
            string sqlwhere = " 1=1 ";

            if (SysVisitor.Instance.cookiesIsAdmin == "False")
            { //判断是否是超管如果是超管理，所有显示
                sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            //数据权限筛选e
            sql = sql + sqlwhere;
            sql = sql + " and summoney<0  and subjectid in (19,20,21,22,23,24,25,26,27,28,29,30,31,32,35)";
            if (!string.IsNullOrEmpty(dayfrom) && !string.IsNullOrEmpty(dayto))
            {
                sql = sql + " and (add_time BETWEEN '" + dayfrom + "' and '" + dayto + "')";
            }
            dt = SqlEasy.ExecuteDataTable(sql);

        }
    }
}