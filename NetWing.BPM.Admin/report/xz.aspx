﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<script src="/scripts/echarts.min.js"></script>
<script src="/scripts/jquery-1.10.2.min.js"></script>
<body>
    <form id="form1" runat="server">
        <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
        <div id="main" style="width: 800px; height: 600px; "></div>
        <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
            string sql = "SELECT Convert ( VARCHAR(10), add_time, 120) AS t,sum(yje) AS r  FROM y_tz GROUP BY Convert ( VARCHAR(10), add_time, 120)";
            DataTable dt = SqlEasy.ExecuteDataTable(sql);

            %>


        <script type="text/javascript">
            // 基于准备好的dom，初始化echarts实例
            var myChart = echarts.init(document.getElementById('main'));


            option = {
                title: {
                    text: '乡镇烤烟收购统计表  绘制时间<%=DateTime.Now.ToString()%>'
                },
                color: ['#3398DB'],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: [<%foreach (DataRow dr  in dt.Rows){%>'<%=dr["t"].ToString()%>',<%}%>'null'],
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        barWidth: '60%',
                        data: [<%foreach (DataRow dr  in dt.Rows){%><%=dr["r"].ToString()%>,<%}%>0]
                    }
                ]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
            // Enable data zoom when user click bar.
      
        </script>
      

 
    </form>
</body>
</html>
