﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<script src="/scripts/echarts.min.js"></script>
<script src="/scripts/jquery-1.10.2.min.js"></script>
<body>
    <form id="form1" runat="server">
        <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
        <div id="main" style="width: 700px; height: 500px; float:left;margin:15px 15px 0 0;"></div>
        <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
            string sql = "select top 30 day(add_time) d,jrsg,zjhzl from y_rz";
            DataTable dt = SqlEasy.ExecuteDataTable(sql);

            %>


        <script type="text/javascript">
            // 基于准备好的dom，初始化echarts实例
            var myChart = echarts.init(document.getElementById('main'));

            // 指定图表的配置项和数据
            option = {
                title: {
                    text: '最近一个月收购情况',
                    subtext: '纯属虚构'
                },
                tooltip: {
                    trigger: 'axis'
                },
                toolbox: {
                    show: true,
                    feature: {
                        saveAsImage: {}
                    }
                },

                legend: {
                    data: ['最高收购量', '最低收购量']
                },

                xAxis: {//横坐标
                    type: 'category',
                    boundaryGap: false,
                    data: [<%foreach (DataRow dr in dt.Rows){%>'<%=dr["d"].ToString()%>日',<%}%>'N日']
            

        
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} 公斤'
                    }
                },
                series: [{
                    name: '收购量',
                    type: 'line',
                    data: [<%foreach (DataRow dr in dt.Rows){%>'<%=dr["jrsg"].ToString()%>',<%}%>'N'],
                    markPoint: {
                        data: [{
                            name: '周最低',
                            value: 5,
                            type: 'max',
                            symbolSize: 2,
                            label: {
                                normal: {
                                    position: 'top',
                                    formatter: '最高收购量'
                                }
                            }

                        }]
                    }
                }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        </script>
         <div id="sgwcqk" style="width: 700px; height: 500px;margin:15px 15px 0 0;  float:left;"></div>
        <script type="text/javascript">
            // 基于准备好的dom，初始化echarts实例
            var myChart = echarts.init(document.getElementById('sgwcqk'));

            option = {
                title: {
                    text: '收购任务完成情况',
                    subtext: '按百分比'
                },
                tooltip: {
                    trigger: 'axis'
                },
                toolbox: {
                    show: true,
                    feature: {
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: true,
                    data: [<%foreach (DataRow dr in dt.Rows){%>'<%=dr["d"].ToString()%>日',<%}%>'目标日']
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} %'
                    },
                },
                series: [{
                    name: '完成百分比',
                    type: 'line',
                    smooth: true,
                    showSymbol: false,
                    symbol: false,
                    lineStyle: {
                        normal: {
                            color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [{
                                offset: 0.7,
                                color: '#993CED' // 0% 处的颜色
                            }, {
                                offset: 1,
                                color: '#56D9FC' // 100% 处的颜色
                            }], false),
                            width: 5,
                        },
                    },
                    markPoint: {
                        data: [{
                            name: '最大值',
                            type: 'max',
                            valueIndex: 0
                        }],
                    },
                    data: [<%foreach (DataRow dr in dt.Rows){%>'<%=dr["zjhzl"].ToString()%>',<%}%>'100'],
                }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        </script>
        <div id="zzt" style="width: 700px; height: 500px;margin:15px 15px 0 0;float:left;"></div>
        <script>
            // 基于准备好的dom，初始化echarts实例
            var myChartzzt = echarts.init(document.getElementById('zzt'));
            var uploadedDataURL = "/report/cx.json";




            function showProvince() {
                var name = 'cx';

                 myChartzzt.showLoading();

                $.get(uploadedDataURL, function (geoJson) {

                     myChartzzt.hideLoading();

                    echarts.registerMap(name, geoJson);

                    myChartzzt.setOption(option = {

                        title: {
                            text: "楚雄彝族自治州烟草种植分布图",
                            left: 'center'
                        },

                        tooltip: {
                            trigger: 'item'
                        },

                        visualMap: {
                            min: 0,
                            max: 100,
                            left: 'left',
                            top: 'bottom',
                            text: ['大', '小'], // 文本，默认为数值文本
                            calculable: true,
                            color: ['#036513', 'yellow', 'orangered']
                            //style:'dark',
                        },
                        toolbox: {
                            show: true,
                            orient: 'vertical',
                            left: 'right',
                            top: 'center',
                            feature: {
                                dataView: { readOnly: false },
                                restore: {},
                                saveAsImage: {}
                            }
                        },
                        series: [{
                            type: 'map',
                            mapType: name,
                            label: {
                                normal: {
                                    show: true
                                },
                                emphasis: {
                                    textStyle: {
                                        color: '#fff'
                                    }
                                }
                            },
                            itemStyle: {

                                normal: {
                                    borderColor: '#059a1e',
                                    areaColor: '#fff',
                                },
                                emphasis: {
                                    areaColor: '#059a1e',
                                    borderWidth: 0
                                }
                            },
                            animation: false,

                            data: [
                                { name: '永仁县', value: 100 },
                                { name: '牟定县', value: 90 },
                                { name: '楚雄市', value: 20 },
                                { name: '南华县', value: 30 },
                                { name: '元谋县', value: 10 },
                                { name: '双柏县', value: 41 },
                                { name: '姚安县', value: 70 },
                                { name: '大姚县', value: 60 },
                                { name: '禄丰县', value: 35 },
                                { name: '武定县', value: 35 }
                      
                            ]

                        }],

                    });
                });
            }

            var currentIdx = 0;

            showProvince();
            // 使用刚指定的配置项和数据显示图表。
            //myChartzzt.setOption(option);
        </script>



        <div id="zztk" style="width: 700px; height: 500px; margin:15px 15px 0 0; float:left;"></div>
        <script>
            // 基于准备好的dom，初始化echarts实例
            var myChartzztk = echarts.init(document.getElementById('zztk'));
            var uploadedDataURL = "/report/cx.json";

            $.get(uploadedDataURL, function (geoJson) {

                myChartzztk.hideLoading();

                echarts.registerMap('楚雄州', geoJson);

                option = {
                    title: {
                        text: "楚雄彝族自治州收购实时完成情况",
                        left: 'center'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: {}
                        }
                    },
                    "tooltip": {
                        "trigger": "item"
                    },
                    "legend": {
                        "orient": "horizontal",
                        "y": "bottom",
                        "x": "center",
                        "data": ["1万", "2万", "5万", "10万", "100万"],
                        "itemWidth": 30,
                        "itemHeight": 30,
                        "textStyle": {
                            "color": "#ff0000"
                        }
                    },
                    "geo": {
                        "map": "楚雄州",
                        "layoutCenter": ["55%", "50%"],
                        "layoutSize": "80%",
                        "label": {
                            "emphasis": {
                                "show": true
                            }
                        },
                        "roam": false,
                        "mapLocation": {
                            "width": "110%",
                            "height": "97%"
                        },
                        "geoCoord": {
                            "牟定县": [101.50, 25.30],
                            "大姚县": [101.31, 25.72],
                            "永仁县": [101.67, 26.05],
                            "禄丰县": [102.07, 25.14],
                            "武定县": [102.41, 25.53],
                            "双柏县": [101.64, 24.69],
                            "楚雄市": [101.54, 25.04],
                            "南华县": [101.28, 25.19],
                            "姚安县": [101.24, 25.50],
                            "元谋县": [101.87, 25.70],

                        },
                        "itemStyle": {
                            "normal": {
                                "areaColor": "#3aa1bc",
                                "borderColor": "#111"
                            },
                            "emphasis": {
                                "areaColor": "#2a333d"
                            }
                        }
                    },
                    "series": [{
                        "name": "1万",
                        "type": "effectScatter",
                        "coordinateSystem": "geo",
                        "data": [{
                            "name": "牟定县",
                            "value": [101.50, 25.30, 100]
                        }, {
                            "name": "大姚县",
                            "value": [101.31, 25.72, 100]
                        }],
                        "rippleEffect": {
                            "period": 4,
                            "scale": 2.5,
                            "brushType": "fill"
                        },
                        "label": {
                            "normal": {
                                "formatter": "{b}",
                                "position": "right",
                                "show": true
                            },
                            "emphasis": {
                                "show": true
                            }
                        },
                        "itemStyle": {
                            "normal": {
                                "color": "#059a1e"
                            }
                        }
                    }, {
                        "name": "2万",
                        "type": "effectScatter",
                        "coordinateSystem": "geo",
                        "data": [{
                            "name": "双柏县",
                            "value": [101.64, 24.69, 150]
                        }, {
                            "name": "姚安县",
                            "value": [101.24, 25.50, 150]
                        }, {
                            "name": "元谋县",
                            "value": [101.87, 25.70, 150]
                        }],
                        "rippleEffect": {
                            "period": 4,
                            "scale": 2.5,
                            "brushType": "fill"
                        },
                        "label": {
                            "normal": {
                                "formatter": "{b}",
                                "position": "right",
                                "show": true
                            },
                            "emphasis": {
                                "show": true
                            }
                        },
                        "itemStyle": {
                            "normal": {
                                "color": "#C7AB0E"
                            }
                        }
                    }, {
                        "name": "5万",
                        "type": "effectScatter",
                        "coordinateSystem": "geo",
                        "data": [{
                            "name": "禄丰县",
                            "value": [102.07, 25.14, 250]
                        }, {
                            "name": "楚雄市",
                            "value": [101.54, 25.04, 250]
                        }],
                        "rippleEffect": {
                            "period": 4,
                            "scale": 2.5,
                            "brushType": "fill"
                        },
                        "label": {
                            "normal": {
                                "formatter": "{b}",
                                "position": "right",
                                "show": true
                            },
                            "emphasis": {
                                "show": true
                            }
                        },
                        "itemStyle": {
                            "normal": {
                                "color": "#CD3E0A"
                            }
                        }
                    }, {
                        "name": "10万",
                        "type": "effectScatter",
                        "coordinateSystem": "geo",
                        "data": [{
                            "name": "武定县",
                            "value": [102.41, 25.53, 400]
                        }],
                        "rippleEffect": {
                            "period": 4,
                            "scale": 2.5,
                            "brushType": "fill"
                        },
                        "label": {
                            "normal": {
                                "formatter": "{b}",
                                "position": "right",
                                "show": true
                            },
                            "emphasis": {
                                "show": true
                            }
                        },
                        "itemStyle": {
                            "normal": {
                                "color": "#9B0A4C"
                            }
                        }
                    }, {
                        "name": "100万",
                        "type": "effectScatter",
                        "coordinateSystem": "geo",
                        "data": [{
                            "name": "永仁县",
                            "value": [101.67, 26.05, 400]
                        }, {
                            "name": "南华县",
                            "value": [101.28, 25.19, 400]
                        }],
                        "rippleEffect": {
                            "period": 4,
                            "scale": 2.5,
                            "brushType": "fill"
                        },
                        "label": {
                            "normal": {
                                "formatter": "{b}",
                                "position": "right",
                                "show": true
                            },
                            "emphasis": {
                                "show": true
                            }
                        },
                        "itemStyle": {
                            "normal": {
                                "color": "#78168B"
                            }
                        }
                    }]
                };
                myChartzztk.setOption(option);
            });
            // 使用刚指定的配置项和数据显示图表。
           // myChartzztk.setOption(option);
        </script>


 
    </form>
</body>
</html>
