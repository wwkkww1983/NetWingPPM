﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="daypt.aspx.cs" Inherits="NetWing.BPM.Admin.report.daypt" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<%@ Import Namespace="NetWing.BPM.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="tt" class="easyui-tabs" style="width: 100%; height: 760px;">
        <div title="财务日报表" style="padding: 20px; display: none;" data-options="border:true">
            <ul>
                <li style="float: left;">
                    <div class="easyui-panel" title="当日收入:<%=decimal.Round(dayin,2) %>"
                        style="width: 500px; height: 500px; padding: 5px; background: #fafafa;"
                        data-options="iconCls:'icon-save',closable:false,   
                collapsible:true,minimizable:false,maximizable:false">
                        <%  int k = 1;
                            foreach (DataRow indr in indt.Rows)
                            {%>
                        <p><%=k %>.<%=DateTime.Parse(indr["add_time"].ToString()).ToString("yyyy-MM-dd") %>,<%=indr["unit"].ToString() %>,<%=indr["roomno"].ToString() %>,<%=indr["note"].ToString() %>,金额：<%=decimal.Round(decimal.Parse(indr["total"].ToString()),2) %></p>

                        <%  k = k + 1;
                            } %>
                    </div>
                </li>
                <li style="float: left; margin: 0 0 0 10px;">
                    <div class="easyui-panel" title="当日支出:<%=decimal.Round(dayout,2) %>"
                        style="width: 500px; height: 500px; padding: 5px; background: #fafafa;"
                        data-options="iconCls:'icon-save',closable:false,   
                collapsible:true,minimizable:false,maximizable:false">
                    </div>
                </li>
                <li style="float: left; margin: 0 0 0 10px;">
                    <div class="easyui-panel" title="当日账户余额:见下表"
                        style="width: 500px; height: 500px; padding: 5px; background: #fafafa;"
                        data-options="iconCls:'icon-save',closable:false,   
                collapsible:true,minimizable:false,maximizable:false">
                        <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
                        <div id="main" style="width: 400px; height: 400px;"></div>
                        <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
                            string sqla = "select sum(accountBalance) as a from MJBankAccount where";
                            //数据权限筛选s
                            string sqlwherea = " 1=1 ";
                            if (SysVisitor.Instance.cookiesIsAdmin == "False")
                            { //判断是否是超管如果是超管理，所有显示
                                sqlwherea = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
                            }
                            //数据权限筛选e
                            sqla = sqla + sqlwherea;
                            DataRow dra = SqlEasy.ExecuteDataRow(sqla);

                            //得到财务表实付金额
                            string sqlb = "select sum(payment) as b from MJFinanceMain where ";
                            sqlb = sqlb + sqlwherea;
                            DataRow drb = SqlEasy.ExecuteDataRow(sqlb);

                        %>



            系统财务自动比对：账户总余额<%=dra["a"].ToString() %> 财务主表总余额<%=drb["b"].ToString() %>
                        <%//SqlDataReader dr = SqlEasy.ExecuteDataReader("select top 30 day(add_time) d,jrsg from y_rz");
                            string sql = "select * from MJBankAccount where";
                            //数据权限筛选s
                            string sqlwhere = " 1=1 ";
                            if (SysVisitor.Instance.cookiesIsAdmin == "False")
                            { //判断是否是超管如果是超管理，所有显示
                                sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
                            }
                            //数据权限筛选e
                            sql = sql + sqlwhere;
                            DataTable dt = SqlEasy.ExecuteDataTable(sql);

                        %>
                        <script type="text/javascript">
                            // 基于准备好的dom，初始化echarts实例
                            var myChart = echarts.init(document.getElementById('main'));
                            option = {
                                title: {
                                    text: '',//标题隐藏    银行账户余额
                                    subtext: '',//实时统计
                                    x: 'center'
                                },
                                toolbox: {
                                    show: true,
                                    feature: {
                                        dataZoom: {
                                            yAxisIndex: 'none'
                                        },
                                        dataView: { readOnly: false },
                                        magicType: { type: ['line', 'bar'] },
                                        restore: {},
                                        saveAsImage: {}
                                    }
                                },
                                tooltip: {
                                    trigger: 'item',
                                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                                },
                                legend: {
                                    orient: 'vertical',
                                    left: 'left',
                                    data: [<%foreach (DataRow dr in dt.Rows)
                            {%>'<%=dr["accountName"].ToString()%>余额：<%=dr["accountBalance"].ToString()%>元',<%}%>'其他']
                            },
                            series: [
                                {
                                    name: '单位：元',
                                    type: 'pie',
                                    radius: '55%',
                                    center: ['50%', '60%'],
                                    data: [
                            <%foreach (DataRow dr in dt.Rows)
                            {%>
                                { value: <%=dr["accountBalance"].ToString()%>, name: '<%=dr["accountName"].ToString()%>余额：<%=dr["accountBalance"].ToString()%>元' },
                            <%}%>
                                { value: 0, name: '其他' }
                            ],
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                                }
                            ]
                            };



                            // 使用刚指定的配置项和数据显示图表。
                            myChart.setOption(option);
            // Enable data zoom when user click bar.

                        </script>


                    </div>
                </li>
            </ul>
            <ul style="margin-top:10px;float:left;">
                <li style="float: left;">
                    <div class="easyui-panel" title="截止当日空房数:<%=kongcount %>"
                        style="width: 500px; height: 500px; padding: 5px; background: #fafafa;"
                        data-options="iconCls:'icon-save',closable:false,   
                collapsible:true,minimizable:false,maximizable:false">
                        <%  int i = 1;
                            foreach (DataRow kongdr   in kongdt.Rows){%>
                            <p><%=i %>.<%=kongdr["roomnumber"].ToString() %>,<%=kongdr["state"].ToString() %>,<%=kongdr["remarks"].ToString() %></p>

                        <%  i = i + 1;
                            } %>

                    </div>

                </li>
                <li style="float: left; margin: 0 0 0 10px;"></li>
                <li style="float: left; margin: 0 0 0 10px;"></li>
            </ul>




        </div>
    </div>
</asp:Content>
