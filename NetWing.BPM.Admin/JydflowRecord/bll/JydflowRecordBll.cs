using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydflowRecordBll
    {
        public static JydflowRecordBll Instance
        {
            get { return SingletonProvider<JydflowRecordBll>.Instance; }
        }

        public int Add(JydflowRecordModel model)
        {
            return JydflowRecordDal.Instance.Insert(model);
        }

        public int Update(JydflowRecordModel model)
        {
            return JydflowRecordDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydflowRecordDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydflowRecordDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
