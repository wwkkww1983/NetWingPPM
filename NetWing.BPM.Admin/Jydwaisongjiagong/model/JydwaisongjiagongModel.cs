using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydwaisongjiagong")]
	[Description("外送加工")]
	public class JydwaisongjiagongModel
	{
				/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 后加工厂
		/// </summary>
		[Description("后加工厂")]
		public string houjiagong { get; set; }		
		/// <summary>
		/// 下单人
		/// </summary>
		[Description("下单人")]
		public string auser { get; set; }		
		/// <summary>
		/// 下单时间
		/// </summary>
		[Description("下单时间")]
		public DateTime atime { get; set; }		
		/// <summary>
		/// 交货时间
		/// </summary>
		[Description("交货时间")]
		public DateTime utitme { get; set; }		
		/// <summary>
		/// 印刷品名称
		/// </summary>
		[Description("印刷品名称")]
		public string title { get; set; }		
		/// <summary>
		/// 订单ID
		/// </summary>
		[Description("订单ID")]
		public string orderid { get; set; }		
		/// <summary>
		/// 接件尺寸
		/// </summary>
		[Description("接件尺寸")]
		public string jiejiancc { get; set; }		
		/// <summary>
		/// 开纸尺寸
		/// </summary>
		[Description("开纸尺寸")]
		public string kaizhicc { get; set; }		
		/// <summary>
		/// 加工数量
		/// </summary>
		[Description("加工数量")]
		public int jiagongsl { get; set; }		
		/// <summary>
		/// 加工单价
		/// </summary>
		[Description("加工单价")]
		public decimal jiagongdj { get; set; }		
		/// <summary>
		/// 加工金额
		/// </summary>
		[Description("加工金额")]
		public decimal jiagongje { get; set; }		
		/// <summary>
		/// 加工项目
		/// </summary>
		[Description("加工项目")]
		public string porject { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string bareak { get; set; }		
		/// <summary>
		/// 自定义
		/// </summary>
		[Description("自定义")]
		public string id { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}