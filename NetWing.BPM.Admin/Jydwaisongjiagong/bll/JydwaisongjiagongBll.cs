using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydwaisongjiagongBll
    {
        public static JydwaisongjiagongBll Instance
        {
            get { return SingletonProvider<JydwaisongjiagongBll>.Instance; }
        }

        public int Add(JydwaisongjiagongModel model)
        {
            return JydwaisongjiagongDal.Instance.Insert(model);
        }

        public int Update(JydwaisongjiagongModel model)
        {
            return JydwaisongjiagongDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydwaisongjiagongDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydwaisongjiagongDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
