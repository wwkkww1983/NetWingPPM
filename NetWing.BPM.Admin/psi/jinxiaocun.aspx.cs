﻿using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace NetWing.BPM.Admin.psi
{
    public partial class jinxiaocun : System.Web.UI.Page
    {
        protected DataTable dataTable = null;
        public int pagesize = 50;
        public int page = 1;
        public int start = 1;
        public int nextpage = 1;
        public int firstpage = 1;
        public int end = 20;
        public int total = 0;
        public int allpage = 0;
        public string mdpage = "";//中间的页码
        public string searchSql = "";
        public string searchUrl = "";
        public string mcname = "";
        public int zongjia = 0;
        public int zj = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = " * ";
            if (!string.IsNullOrEmpty(Request["goodsclassname"]))//企业名称不为空
            {
                searchSql = searchSql + " and goodsclassname like '%" + Request["goodsclassname"] + "%'";
                searchUrl = searchUrl + "&goodsclassname=" + Request["goodsclassname"] + "";
            }
            if (!string.IsNullOrEmpty(Request["goodsname"]))//简拼不为空
            {
                searchSql = searchSql + " and goodsname like '%" + Request["goodsname"] + "%'";
                searchUrl = searchUrl + "&goodsname=" + Request["goodsname"] + "";
            }
            if (!string.IsNullOrEmpty(Request["xdrq"]))//下单日期
            {
                string xd = Request["xdrq"];                string[] xdarr = xd.Split('~');
                searchSql = searchSql + " and up_time between '" + xdarr[0] + "' and '" + xdarr[1] + "'";
                searchUrl = searchUrl + "&up_time=" + Request["xdrq"] + "";
            }


            if (!string.IsNullOrEmpty(Request["orderType"]))
            {
                searchSql = searchSql + " and orderType like '%" + Request["orderType"] + "%'";
                searchUrl = searchUrl + " and orderType like '%" + Request["orderType"] + "%'";
            }
            if (!string.IsNullOrEmpty(Request["unitname"]))
            {
                searchSql = searchSql + " and unitname like '%" + Request["unitname"] + "%'";
                searchUrl = searchUrl + " and unitname like '%" + Request["unitname"] + "%'";
            }
            if (!string.IsNullOrEmpty(Request["order_id"]))
            {
                searchSql = searchSql + " and order_id like '%" + Request["order_id"] + "%'";
                searchUrl = searchUrl + " and order_id like '%" + Request["order_id"] + "%'";
            }
            



            total = (int)SqlEasy.ExecuteScalar("select count(keyid) from Psi_BuyDetails where 1=1" + searchSql + "");

            allpage = DbUtils.GetPageCount(pagesize, total);

            if (!string.IsNullOrEmpty(Request["page"]))
            {
                page = int.Parse(Request["page"]);
                if (page < allpage)
                {
                    nextpage = page + 1;
                }
                else
                {
                    nextpage = page;
                }


                if (page != 1)
                {
                    firstpage = page - 1;
                }
                start = pagesize * (page - 1) + 1;
                end = pagesize * page;
            }

            //页数很少的时候
            if (allpage < 8)
            {
                for (int i = 1; i <= allpage; i++)
                {
                    if (i == page)
                    {
                        mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                    }
                    else
                    {
                        mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                    }


                }


            }
            else
            {
                //小于9页也就是8页的时候
                if (page <= 9)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }


                    }


                }
                //在末尾的情况
                if ((page + 9) >= allpage)
                {
                    for (int i = (allpage - 8); i <= allpage; i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }


                    }


                }
                //在末尾的情况
                //中间的情况s
                if (((page + 9) < allpage) && page > 9)
                {
                    for (int i = (page - 4); i <= (page + 4); i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }
                    }


                }
            }

            //显示中间的页码e
            dataTable = SqlEasy.ExecuteDataTable("Select * FROM (SELECT ROW_NUMBER()Over(order by KeyId desc) as rowId," + sql + " from  jxc where 1=1 " + searchSql + ") as mytable WHERE rowId between " + start + " and " + end + "");
         
        }

    }
}