using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("Psi_GoodsClass")]
	[Description("商品分类")]
	public class Psi_GoodsClassModel
	{
				/// <summary>
		/// 货品类别id 
		/// </summary>
		[Description("货品类别id ")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 内部编号 
		/// </summary>
		[Description("内部编号 ")]
		public string classNo { get; set; }		
		/// <summary>
		/// 分类名称
		/// </summary>
		[Description("分类名称")]
		public string className { get; set; }		
		/// <summary>
		/// 描述
		/// </summary>
		[Description("描述")]
		public string description { get; set; }		
		/// <summary>
		/// 父级ID
		/// </summary>
		[Description("父级ID")]
		public int parentid { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string note { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
		/// <summary>
		/// 部门所有者ID
		/// </summary>
		[Description("部门所有者ID")]
		public int depid { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}