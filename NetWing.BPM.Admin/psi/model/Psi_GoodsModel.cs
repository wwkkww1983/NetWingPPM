using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("Psi_Goods")]
	[Description("商品管理")]
	public class Psi_GoodsModel
	{
				/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 商品编码
		/// </summary>
		[Description("商品编码")]
		public string goodsNo { get; set; }		
		/// <summary>
		/// 商品名称
		/// </summary>
		[Description("商品名称")]
		public string goodsName { get; set; }		
		/// <summary>
		/// 商品简称
		/// </summary>
		[Description("商品简称")]
		public string goodsShortName { get; set; }		
		/// <summary>
		/// 商品分类ID
		/// </summary>
		[Description("商品分类ID")]
		public int goodsClassID { get; set; }		
		/// <summary>
		/// 商品分类名称
		/// </summary>
		[Description("商品分类名称")]
		public string goodsClassName { get; set; }		
		/// <summary>
		/// 规格
		/// </summary>
		[Description("规格")]
		public string specs { get; set; }		
		/// <summary>
		/// 单位
		/// </summary>
		[Description("单位")]
		public string unit { get; set; }		
		/// <summary>
		/// 库存量
		/// </summary>
		[Description("库存量")]
		public int stock { get; set; }		
		/// <summary>
		/// 进价
		/// </summary>
		[Description("进价")]
		public decimal buyPrice { get; set; }


        [Description("售价")]
        public decimal sellPrice { get; set; }
        [Description("状态")]
        public string status { get; set; }

		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
		/// <summary>
		/// 部门权限所有者
		/// </summary>
		[Description("部门权限所有者")]
		public int depid { get; set; }		
		///// <summary>
  //      /// 状态
  //      /// </summary>


  //      /// <returns></returns>
  //      [Description("审核状态")]
  //      public int examine_status { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}