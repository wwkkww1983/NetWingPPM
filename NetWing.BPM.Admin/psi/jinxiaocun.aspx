﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jinxiaocun.aspx.cs" Inherits="NetWing.BPM.Admin.psi.jinxiaocun" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form action="?" method="get">
        <div id="toolber">

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>搜索条件</legend>
            </fieldset>

            <div class="layui-form">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">类别</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goodsclassname" value="<%=Request["goodsclassname"] %>" id="goodsclassname" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                     <div class="layui-inline">
                        <label class="layui-form-label">进出类型</label>
                        <div class="layui-input-inline">
                            <input type="text" name="orderType" value="<%=Request["orderType"] %>" id="orderType" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goodsname" value="<%=Request["goodsname"] %>" id="goodsname" autocomplete="off" class="layui-input">
                        </div>
                    </div>






                    <div class="layui-inline">
                        <label class="layui-form-label">日期</label>
                        <div class="layui-input-inline">
                            <input type="text" value="<%=Request["xdrq"] %>" class="layui-input" name="xdrq" id="xdrq" readonly placeholder=" - ">
                        </div>
                    </div>
                     <div class="layui-inline">
                        <label class="layui-form-label">单位</label>
                        <div class="layui-input-inline">
                            <input type="text" name="unitname" value="<%=Request["unitname"] %>" id="unitname" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                     <div class="layui-inline">
                        <label class="layui-form-label">订单号</label>
                        <div class="layui-input-inline">
                            <input type="text" name="order_id" value="<%=Request["order_id"] %>" id="order_id" autocomplete="off" class="layui-input">
                        </div>
                    </div>




                    <div class="layui-inline">
                        <div class="layui-input-inline">
                            <input type="submit" class="layui-btn" value="搜索" />
                        </div>
                        
                    </div>



                </div>
            </div>


        </div>
    </form>

    <form id="form1" runat="server">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card"></div>
                    <div class="layui-card-body">
                        <table border="1" class="layui-table" id="grid" lay-filter="datatable">
                            <thead>
                                <tr>


                                    <td data-field="序号">序号</td>
                                    <td data-field="进出类型">进出类型</td>
                                    <td data-field="编号">编号</td>
                                    <td data-field="名称">名称</td>
                                    <td data-field="类别">类别</td>
                                    <td data-field="规格">规格</td>
                                    <td data-field="单位">单位</td>
                                    <td data-field="数量">数量</td>


                                    <td data-field="单价">单价</td>
                                    <td data-field="总价">总价</td>
                                    <td data-field="更新时间">更新时间</td>
                                    <td data-field="收款账户">收款账户</td>
                                    <td data-field="单位">单位</td>
                                    <td data-field="单位联系人">单位联系人</td>
                                    <td data-field="经手人">经手人</td>
                                    <td data-field="进出编号">进出编号</td>
                                    <td data-field="订单号">订单号</td>
                                    <td data-field="预付款">预付款</td>
                                    <td data-field="尾款">尾款</td>
                                </tr>
                            </thead>
                        
                            <%
                                int i = 1;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {%>
                            <tr>
                                <td data-field="序号"><%=dataRow["rowid"].ToString() %></td>
                            <td data-field="<%=dataRow["orderType"].ToString() %>"><%=dataRow["orderType"].ToString() %></td>
                            <td data-field="<%=dataRow["goodsNo"].ToString() %>"><%=dataRow["goodsNo"].ToString() %></td>
                            <td data-field="<%=dataRow["goodsName"].ToString() %>"><%=dataRow["goodsName"].ToString() %></td>
                            <td data-field="<%=dataRow["goodsClassName"].ToString() %>"><%=dataRow["goodsClassName"].ToString() %></td>












                            <td data-field="<%=dataRow["specs"].ToString() %>"><%=dataRow["specs"].ToString() %></td>
                            <td data-field="<%=dataRow["unit"].ToString() %>"><%=dataRow["unit"].ToString() %></td>
                            <td data-field="<%=dataRow["buyNum"].ToString() %>"><%=dataRow["buyNum"].ToString() %></td>
                            <td data-field="<%=dataRow["buyprice"].ToString() %>"><%=dataRow["buyprice"].ToString() %></td>
                            <td data-field="<%=dataRow["buyAllmoney"].ToString() %>"><%=dataRow["buyAllmoney"].ToString() %></td>
                            <td data-field="<%=dataRow["add_time"].ToString() %>"><%=dataRow["add_time"].ToString() %></td>
                            <td data-field="<%=dataRow["paybank"].ToString() %>"><%=dataRow["paybank"].ToString() %></td>
                            <td data-field="<%=dataRow["unitname"].ToString() %>"><%=dataRow["unitname"].ToString() %></td>
                            <td data-field="<%=dataRow["unit_man"].ToString() %>"><%=dataRow["unit_man"].ToString() %></td>
                            <td data-field="<%=dataRow["Contacts"].ToString() %>"><%=dataRow["Contacts"].ToString() %></td>
                            <td data-field="<%=dataRow["edNumber"].ToString() %>"><%=dataRow["edNumber"].ToString() %></td>
                            <td data-field="<%=dataRow["order_id"].ToString() %>"><%=dataRow["order_id"].ToString() %></td>
                            <td data-field="<%=dataRow["yfk"].ToString() %>"><%=dataRow["yfk"].ToString() %></td>
                            <td data-field="<%=dataRow["wk"].ToString() %>"><%=dataRow["wk"].ToString() %></td>
                                </tr>
                            <%} %>

                        </table>



                    </div>
                </div>
            </div>
        </div>
        <center><div class="layui-box layui-laypage layui-laypage-default"><span>共<%=total %>记录</span><span>共<%=allpage %>页</span><span>每页<%=pagesize %>条记录</span><a href="?page=1<%=searchUrl %>">第一页</a><a href="?page=<%=firstpage %><%=searchUrl %>">«上一页</a><%=mdpage %><a href="?page=<%=nextpage %><%=searchUrl %>">下一页»</a><a href="?page=<%=allpage %><%=searchUrl %>">最后一页</a></div></center>

        <%--<center><div class="layui-box layui-laypage layui-laypage-default"><span>共记录</span><span>共页</span><span>每页条记录</span><a href="?page=1">第一页</a><a href="?page=">«上一页</a><a href="?page=">下一页»</a><a href="?page=">最后一页</a></div></center>--%>
    </form>
    
            <script src="../scripts/layui/layui.js"></script>
        <!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
        <script>
            layui.use(['form', 'layedit', 'laydate'], function () {
                var form = layui.form
                    , layer = layui.layer
                    , layedit = layui.layedit
                    , laydate = layui.laydate;
                //日期时间范围
                laydate.render({
                    elem: '#xdrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
                laydate.render({
                    elem: '#jhrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
            });

        </script>
</body>
</html>