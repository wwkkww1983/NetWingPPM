using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_OrderBll
    {
        public static Psi_OrderBll Instance
        {
            get { return SingletonProvider<Psi_OrderBll>.Instance; }
        }

        public int Add(Psi_OrderModel model)
        {
            return Psi_OrderDal.Instance.Insert(model);
        }

        public int Update(Psi_OrderModel model)
        {
            return Psi_OrderDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_OrderDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_OrderDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
