using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_GoodsBll
    {
        public static Psi_GoodsBll Instance
        {
            get { return SingletonProvider<Psi_GoodsBll>.Instance; }
        }

        public int Add(Psi_GoodsModel model)
        {
            return Psi_GoodsDal.Instance.Insert(model);
        }

        public int Update(Psi_GoodsModel model)
        {
            return Psi_GoodsDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_GoodsDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_GoodsDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
