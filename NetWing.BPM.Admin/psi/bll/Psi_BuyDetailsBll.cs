using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_BuyDetailsBll
    {
        public static Psi_BuyDetailsBll Instance
        {
            get { return SingletonProvider<Psi_BuyDetailsBll>.Instance; }
        }

        public int Add(Psi_BuyDetailsModel model)
        {
            return Psi_BuyDetailsDal.Instance.Insert(model);
        }

        public int Update(Psi_BuyDetailsModel model)
        {
            return Psi_BuyDetailsDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_BuyDetailsDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_BuyDetailsDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
