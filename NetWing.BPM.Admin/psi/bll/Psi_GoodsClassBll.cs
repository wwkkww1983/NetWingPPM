using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_GoodsClassBll
    {
        public static Psi_GoodsClassBll Instance
        {
            get { return SingletonProvider<Psi_GoodsClassBll>.Instance; }
        }

        public int Add(Psi_GoodsClassModel model)
        {
            return Psi_GoodsClassDal.Instance.Insert(model);
        }

        public int Update(Psi_GoodsClassModel model)
        {
            return Psi_GoodsClassDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_GoodsClassDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_GoodsClassDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
