﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="yujkc.aspx.cs" Inherits="NetWing.BPM.Admin.psi.yujkc" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>预警产品</title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <link href="../../layUI/src/css/layui.css" rel="stylesheet" />
</head>
<body>
   <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">预警产品</div>
                    <div class="layui-card-body">
                        <table class="layui-hide" id="test-table-checkbox"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../scripts/layui/layui.all.js"></script>
    <script>
        layui.use(['table'], function () {
            var table = layui.table;
            table.render({
                elem: '#test-table-checkbox'
                , cols: [[
                    { field: 'id', title: '序号', type: 'numbers' }
                    , { field: 'KeyId', title: 'ID', sort: true }
                    , { field: 'goodsNo', title: '商品编码' }
                    , { field: 'goodsName', title: '商品名称' }
                    , { field: 'goodsShortName', title: '商品简称' }
                    , { field: 'goodsClassName', title: '商品分类名称' }
                    , { field: 'specs', title: '规格' }
                    , { field: 'unit', title: '单位' }
                    , { field: 'buyPrice', title: '进价' }
                    , { field: 'up_time', title: '最后更新时间' }
                ]]
                , data: [<%=sDateJson%>]
            });
        });
    </script>


 
</body>
</html>
