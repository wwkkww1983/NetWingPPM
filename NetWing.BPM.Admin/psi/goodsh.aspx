﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="goodsh.aspx.cs" Inherits="NetWing.BPM.Admin.psi.goodsh" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form action="?" method="get">
        <div id="toolber">

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>搜索条件</legend>
            </fieldset>

            <div class="layui-form">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">类别</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goodsclassname" value="<%=Request["goodsclassname"] %>" id="goodsclassname" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goodsname" value="<%=Request["goodsname"] %>" id="goodsname" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">日期</label>
                        <div class="layui-input-inline">
                            <input type="text" value="<%=Request["xdrq"] %>" class="layui-input" name="xdrq" id="xdrq" readonly placeholder=" - ">
                        </div>
                    </div>
                    




                    <div class="layui-inline">
                        <div class="layui-input-inline">
                            <input type="submit" class="layui-btn" value="搜索" />
                        </div>
                        
                    </div>



                </div>
            </div>


        </div>
    </form>

    <form id="form1" runat="server">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card"></div>
                    <div class="layui-card-body">
                        <table border="1" class="layui-table" id="grid" lay-filter="datatable">
                            <thead>
                                <tr>


                                    <td data-field="序号">序号</td>
                                    <td data-field="类别">类别</td>
                                    <td data-field="编号">编号</td>
                                    <td data-field="名称">名称</td>
                                    <td data-field="单位">单位</td>
                                    <td data-field="规格">规格</td>
                                    <td data-field="库存">库存</td>
                                    <td data-field="进价">进价</td>


                                    <td data-field="售价">售价</td>
                                    <td data-field="状态">状态</td>
                                    <td data-field="更新时间">更新时间</td>
                                </tr>
                            </thead>
                        
                            <%
 int i = 1;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {%>
                            <tr>
                                <td data-field="序号"><%=dataRow["rowid"].ToString() %></td>   
                                <td data-field="<%=dataRow["goodsclassname"].ToString() %>"><%=dataRow["goodsclassname"].ToString() %></td>
                                <td data-field="<%=dataRow["goodsNo"].ToString() %>"><%=dataRow["goodsNo"].ToString() %></td>
                                 <td data-field="<%=dataRow["goodsName"].ToString() %>"><%=dataRow["goodsName"].ToString() %></td>
                                <td data-field="<%=dataRow["unit"].ToString() %>"><%=dataRow["unit"].ToString() %></td>
                                <td data-field="<%=dataRow["specs"].ToString() %>"><%=dataRow["specs"].ToString() %></td>
                                <td data-field="<%=dataRow["stock"].ToString() %>"><%=dataRow["stock"].ToString() %></td>
                                <td data-field="<%=dataRow["buyprice"].ToString() %>"><%=dataRow["buyprice"].ToString() %></td>
                                <td data-field="<%=dataRow["sellprice"].ToString() %>"><%=dataRow["sellprice"].ToString() %></td>
                                 <td data-field="<%=dataRow["status"].ToString() %>"><%=dataRow["status"].ToString() %></td>
                                <td data-field="<%=dataRow["up_time"].ToString() %>"><%=dataRow["up_time"].ToString() %></td>
                            </tr>


















                            <% 
                                    i = i + 1;
                                } %>
                            <tr><td colspan="11">总价:<%=zj %></td></tr>
                            <tr><td colspan="11">库存总价:<%=zongjia %></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <center><div class="layui-box layui-laypage layui-laypage-default"><span>共<%=total %>记录</span><span>共<%=allpage %>页</span><span>每页<%=pagesize %>条记录</span><a href="?page=1<%=searchUrl %>">第一页</a><a href="?page=<%=firstpage %><%=searchUrl %>">«上一页</a><%=mdpage %><a href="?page=<%=nextpage %><%=searchUrl %>">下一页»</a><a href="?page=<%=allpage %><%=searchUrl %>">最后一页</a></div></center>

        <%--<center><div class="layui-box layui-laypage layui-laypage-default"><span>共记录</span><span>共页</span><span>每页条记录</span><a href="?page=1">第一页</a><a href="?page=">«上一页</a><a href="?page=">下一页»</a><a href="?page=">最后一页</a></div></center>--%>
    </form>
    
            <script src="../scripts/layui/layui.js"></script>
        <!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
        <script>
            layui.use(['form', 'layedit', 'laydate'], function () {
                var form = layui.form
                    , layer = layui.layer
                    , layedit = layui.layedit
                    , laydate = layui.laydate;
                //日期时间范围
                laydate.render({
                    elem: '#xdrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
                laydate.render({
                    elem: '#jhrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
            });

        </script>
</body>
</html>