using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJShenpi")]
	[Description("审批管理")]
	public class MJShenpiModel
	{
				/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 员工ID
		/// </summary>
		[Description("员工ID")]
		public int userid { get; set; }		
		/// <summary>
		/// 员工姓名
		/// </summary>
		[Description("员工姓名")]
		public string username { get; set; }		
		/// <summary>
		/// 审批类型
		/// </summary>
		[Description("审批类型")]
		public string shenpileixing { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 审批事由
		/// </summary>
		[Description("审批事由")]
		public string shiyou { get; set; }		
		/// <summary>
		/// 审批人ID
		/// </summary>
		[Description("审批人ID")]
		public int shenpiid { get; set; }		
		/// <summary>
		/// 审批人
		/// </summary>
		[Description("审批人")]
		public string shenpiname { get; set; }		
		/// <summary>
		/// 审批时间
		/// </summary>
		[Description("审批时间")]
		public DateTime shenpi_time { get; set; }		
		/// <summary>
		/// 审批意见
		/// </summary>
		[Description("审批意见")]
		public string shenpiyijian { get; set; }		
		/// <summary>
		/// 审批状态 同意或不同意
		/// </summary>
		[Description("审批状态 同意或不同意")]
		public string status { get; set; }		
		/// <summary>
		/// 部门权限
		/// </summary>
		[Description("部门权限")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}