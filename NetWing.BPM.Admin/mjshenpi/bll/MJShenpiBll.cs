using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJShenpiBll
    {
        public static MJShenpiBll Instance
        {
            get { return SingletonProvider<MJShenpiBll>.Instance; }
        }

        public int Add(MJShenpiModel model)
        {
            return MJShenpiDal.Instance.Insert(model);
        }

        public int Update(MJShenpiModel model)
        {
            return MJShenpiDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJShenpiDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJShenpiDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
