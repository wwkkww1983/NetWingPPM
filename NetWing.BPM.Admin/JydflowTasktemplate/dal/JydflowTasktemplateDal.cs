using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class JydflowTasktemplateDal : BaseRepository<JydflowTasktemplateModel>
    {
        public static JydflowTasktemplateDal Instance
        {
            get { return SingletonProvider<JydflowTasktemplateDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(JydflowTasktemplateModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}