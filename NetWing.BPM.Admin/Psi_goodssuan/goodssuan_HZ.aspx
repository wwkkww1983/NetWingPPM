﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="goodssuan_HZ.aspx.cs" Inherits="NetWing.BPM.Admin.Psi_goodsjilu.goodssuan_HZ" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!-- 引入 echarts.js -->
    <script src="../../Psi_goodssuan/js/echarts.min.js"></script>
</head>
<body>
    <%--计算进的总数--%>
    <%string je1 = "select SUM(buyprice*stock) from psi_goodssuan where add_time>='" + DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00.00' and add_time<='" + DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59.687' and status=0"; %>
    <%int je = Convert.ToInt32(SqlEasy.ExecuteScalar(je1) == DBNull.Value ? "0" : SqlEasy.ExecuteScalar(je1)); %>
    <%--计算出的价格--%>
    <%string je2 = "select SUM(buyprice*stock) from psi_goodssuan where add_time>='" + DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00.00' and add_time<='" + DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59.687' and status=1"; %>
    <%int jee = Convert.ToInt32(SqlEasy.ExecuteScalar(je2) == DBNull.Value ? "0" : SqlEasy.ExecuteScalar(je2)); %>
    <%--外销进货--%>
    <%DataTable dataTable = SqlEasy.ExecuteDataTable("select * from psi_goodssuan where add_time>='" + DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00.00' and add_time<='" + DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59.687'"); %>
    <%--没小时出库--%>
    <%string[] arrey = new string[24];%>
    <%string[] myarrey = new string[24];%>
    <%string jisuan = "SELECT SUM(stock) stock,CONVERT(varchar(2),add_time,108) add_time FROM psi_goodssuan where status=1 and CONVERT(varchar(10),add_time,120)=CONVERT(varchar(10),GETDATE(),120) group by CONVERT(varchar(2),add_time,108)";%>
    <%DataTable dataa = SqlEasy.ExecuteDataTable(jisuan);%>
    <%

        for (int i = 1; i <= 24; i++)
        {
            myarrey[i - 1] = i.ToString() + "时";
            if (dataa.Rows.Count > 0)
            {
                foreach (DataRow row in dataa.Rows)
                {
                    if (i == int.Parse(row["add_time"].ToString()))
                    {
                        arrey[i - 1] = row["stock"].ToString();
                        break;
                    }
                    else
                        arrey[i - 1] = "0";
                }
            }
            else
                arrey[i - 1] = "0";
        } %>
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div id="main" style="width: 100vw; height: 100vh;"></div>
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        // 指定图表的配置项和数据
        var option = {
            title: [{
                text: '今日进销存预览',
                x: 'center',
                subtext: '<%= DateTime.Now.ToLocalTime().ToString()%>',
                textStyle: {
                    fontSize: 20
                }
            },
                {
                    text: '内外库存比',
                    x: '19.8%',
                    y: '45%',
                    textAlign: 'center',
                    textBaseline: 'middle',
                    textStyle: {
                        fontSize: 20
                    }
                },
                
                {
                    text: '内外比',
                    x: '60.6%',
                    y: '45%',
                    textAlign: 'center',
                    textBaseline: 'middle',
                    textStyle: {
                        fontSize: 20
                    }
                },
                {
                    text: '进:<%=je%>.00元',
                    x: '20%',
                    y: '8%',
                    textStyle: {
                        fontSize: 60
                    }
                },
                {
                    text: '出:<%=jee%>.00元',
                    x: '60%',
                    y: '8%',
                    textStyle: {
                        fontSize: 60
                    }
                }

            ],
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            legend: [{
                data: ['外销', '进货'],
                x: '25%',
                y: '60%'
            }
            ],
            grid: [{
                left: '0%',
                right: '55%',
                top: '65%',
                bottom: '5%',
                containLabel: true
            },
            {
                gridindex: 1,
                left: '48%',
                right: '3%',
                top: '65%',
                bottom: '5%',
                containLabel: true
            }
            ],
            xAxis: [{
                type: 'value',
                axisLabel: {
                    formatter: '{value} '
                },
                boundaryGap: [0, 0.01]
            },
                {
                    gridIndex: 1,
                    type: 'category',
                    boundaryGap: false,
                    data: <%=JSONhelper.ToJson(myarrey)%>,
            }
            ],
            yAxis: [{
                type: 'category',
                data: [
                    <%if (dataTable.Rows.Count > 0)
        {%>
                    <%foreach (DataRow data in dataTable.Rows)
        {%>
                    '<%=data["goodsName"].ToString()%>',
                    <%}%>
                    <%}
        else
        {%>
                    '0'
                    <%}%>
                ],
                axisLabel: {
                    interval: 0
                }
            },
                {
                    gridIndex: 1,
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} '
                    }
                }
            ],
            series: [{
                name: '外销',
                type: 'bar',
                label: {
                    normal: {
                        show: true,
                        position: 'right'
                    }
                },
                data: [
        <%if (dataTable.Rows.Count > 0)
        {%>
                    <%foreach (DataRow data in dataTable.Rows)
        {%>
                    <%if (int.Parse(data["status"].ToString()) == 1)
        {%>
                    <%=data["stock"].ToString()%>,
                    <%}%>
                    <%}%>
                    <%}
        else
        {%>
                    0,
                    <%}%>
                ]
            },
                {
                    name: '进货',
                    type: 'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'right'
                        }
                    },
                    data: [
        <%if (dataTable.Rows.Count > 0)
        {%>
                 <%foreach (DataRow data in dataTable.Rows)
        {%>
                    <%if (int.Parse(data["status"].ToString()) == 0)
        {%>
                    <%=data["stock"].ToString()%>,
                    <%}%>
                    <%}%>
                        <%}
        else
        {%>
                        0,
                        <%}%>
                    ]
                },
                {
                    name: '',
                    type: 'pie',
                    center: ['20%', '45%'],
                    radius: ['15%', '20%'],
                    label: {
                        normal: {

                            formatter: '{b} :{c}({d}%)'
                        }
                    },
                    data: [
                    <%if (dataTable.Rows.Count > 0)
        {%>
                 <%foreach (DataRow data in dataTable.Rows)
        {%>
                        {
                            value: <%=data["stock"].ToString()%>,
                            <%if (int.Parse(data["status"].ToString()) == 1)
        {%>
                            name: '进货<%=data["goodsName"].ToString()%>'
                            <%}
        else
        {%>
                            name: '外销<%=data["goodsName"].ToString()%>'
                            <%}%>
                        },
                <%}%>
                        <%}
        else
        {%>
                    
                        <%}%>
                     
                    ]


                },
                
                {
                    name: '内外比',
                    type: 'pie',
                    center: ['60.6%', '45%'],
                    radius: ['15%', '20%'],
                    label: {
                        normal: {
                            formatter: '{b} :{c}元({d}%)'
                        }
                    },
                    data: [

                        {
                            value: <%=je%>,
                            name: '入库'
                        },

                        {
                            value: <%=jee%>,
                            name: '外销'
                        }
                    ]
                },
                {
                    xAxisIndex: 1,
                    yAxisIndex: 1,
                    name: '分时统计',
                    type: 'line',
                    lineStyle: {
                        normal: {
                            color: '#4ea397'
                        }
                    },
                    data: <%=JSONhelper.ToJson(arrey)%>,
                    markPoint: {
                        data: [{
                            type: 'max',
                            name: '最大值',
                            symbolSize: 80
                        },
                        {
                            type: 'min',
                            name: '最小值',
                            symbolSize: 50
                        }
                        ],
                        itemStyle: {
                            normal: {
                                color: '#d0648a'
                            }
                        }
                    },

                    markLine: {
                        data: [{
                            type: 'average',
                            name: '平均值'
                        }]
                    }
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
</body>
</html>
