using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class TpjsontemplateBll
    {
        public static TpjsontemplateBll Instance
        {
            get { return SingletonProvider<TpjsontemplateBll>.Instance; }
        }

        public int Add(TpjsontemplateModel model)
        {
            return TpjsontemplateDal.Instance.Insert(model);
        }

        public int Update(TpjsontemplateModel model)
        {
            return TpjsontemplateDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return TpjsontemplateDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return TpjsontemplateDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
