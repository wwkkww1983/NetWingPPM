using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_inventoryBll
    {
        public static Psi_inventoryBll Instance
        {
            get { return SingletonProvider<Psi_inventoryBll>.Instance; }
        }

        public int Add(Psi_inventoryModel model)
        {
            return Psi_inventoryDal.Instance.Insert(model);
        }

        public int Update(Psi_inventoryModel model)
        {
            return Psi_inventoryDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_inventoryDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_inventoryDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
