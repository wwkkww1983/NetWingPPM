using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJDeviceBll
    {
        public static MJDeviceBll Instance
        {
            get { return SingletonProvider<MJDeviceBll>.Instance; }
        }

        public int Add(MJDeviceModel model)
        {
            return MJDeviceDal.Instance.Insert(model);
        }

        public int Update(MJDeviceModel model)
        {
            return MJDeviceDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJDeviceDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJDeviceDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
