﻿$(document).ready(function () {

    $('#list').datagrid({
        //url: 'datagrid_data.json',
        singleSelect: true,//只能选择一行
        toolbar: [{
            iconCls: 'icon-save',
            text: '保存',
            handler: function () {
                var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
                if (isValid) {

                } else {
                    $.messager.alert('提醒', '表格没有填写完整');
                    return false;//返回false 程序不执行
                }

                //asp.net 自动生成了一个form1 所以这里验证表单时有个#form1
                $('#list').datagrid('acceptChanges');
                compute();//计算求和

            }
        }, {
            iconCls: 'icon-add',
            text: '新增',
            handler: function () {
                $('#list').datagrid('appendRow', { KeyId: 0, productname: '', material: '', specifications: '', number: '', materialopening: '', technology: '', note: '', billingclerk: '', tell: '', wechareopen: '' });

            }
        }],
        rowStyler: function (index, row) {
            //格式化行 这里也只用来做参考
            //if (row.ftype == '支出') {
            //    return 'background-color:#6293BB;color:#fff;';    // rowStyle是一个已经定义了的ClassName(类名)
            //}
        },
        rownumbers: true, //行号
        columns: [[
            { title: '备料单Spareparts', field: 'KeyId', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
            {
                title: '备料名称', field: 'productname', sortable: true, width: '95px', hidden: false, editor: {
                    type: 'combogrid', options: {
                        panelWidth: 200, validType: '', url: '/sys/ashx/dichandler.ashx?categoryId=107', columns: [[
                            { field: 'KeyId', title: 'ID', width: 30, hidden: false },
                            { field: 'Title', title: '品名', width: 99, hidden: false },
                            { field: 'Code', title: '分类', width: 100, },
                        ]], textField: 'productname', idField: 'KeyId', missingMessage: '备料名称不能为空！'


                    }
                }
            },
            { title: '材料', field: 'material', sortable: true, width: '95px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '规格', field: 'specifications', sortable: true, width: '95px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '数量', field: 'number', sortable: true, width: '95px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '开料规格', field: 'materialopening', sortable: true, width: '95px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '工艺要求', field: 'technology', sortable: true, width: '95px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },

            {
                title: '备料员', field: 'billingclerk', sortable: true, width: '95px', hidden: false, editor: {
                    type: 'combogrid', options: {
                        required: true, panelWidth: 200, validType: '', url: '/sys/ashx/userhandler.ashx?action=mydep', columns: [[
                            { field: 'TrueName', title: '姓名', width: 60 },
                            { field: 'KeyId', title: 'KeyId', width: 100 },
                        ]], textField: 'billingclerk', idField: 'TrueName', missingMessage: '备料员不能为空！'



                    }
                }
            },
            { title: '备料员手机号', field: 'tell', sortable: true, width: '95px', hidden: true, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '备料员openid', field: 'wechareopen', sortable: true, width: '95px', hidden: true, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '备注', field: 'note', sortable: true, width: '280px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '历史', field: 'jhdate', sortable: true, width: '280px', hidden: true, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '材料成本', field: 'cailiao', sortable: true, width: '95px', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },

        ]],








        data: [//先整10个空表格出来
            { KeyId: 0, productname: '', material: '', specifications: '', number: '', materialopening: '', technology: '', note: '', billingclerk: '', tell: '', wechareopen: '' },
                    ],
        onClickCell: function (index, field, value) { //双击可以修改
        },
        onDblClickRow: function (index, row) {//单击row
            $('#list').datagrid('beginEdit', index);
            //以下代码主要是用于行内编辑文本框做一些判断留着做一些参考
            var ed = $('#list').datagrid('getEditor', { index: index, field: 'productname' });//找到行editor
            $(ed.target).combogrid({
                keyHandler: {
                    up: function () { },
                    down: function () { },
                    enter: function () { },
                    query: function (q) {
                        //动态搜索
                        console.log("搜索关键词是：" + q);
                        $(this).combogrid("grid").datagrid("reload", { 'q': q });
                        $(this).combogrid("setValue", q);
                    }
                },
                onChange: function (newValue, oldValue) {
                    artChanged = true;//记录是否有改变（当手动输入时发生)
                    console.log("值已发生改变:" + newValue);
                },
                onHidePanel: function () {
                    var t = $(this).combogrid('getValue');
                    //console.log("面板即将隐藏t是：" + t + ",artChange值：" + artChanged + ",selectRow:" + JSON.stringify(selectRow));
                    if (artChanged) {
                        if (selectRow == null || t != selectRow.Title) {//没有选择或者选项不相等时清除内容
                            //alert('请选择，不要直接输入');
                            $(this).combogrid('setValue', '');
                            $.messager.alert("对不起，值无效！", "请选择，不要直接输入!");
                        } else {
                            console.log("值没有发生改变");
                            //do something...
                        }
                    }
                    artChanged = false;
                    selectRow = null;
                },
                onShowPanel: function () {
                },
                onSelect: function (rowIndex, rowData) {//子节点选择时的动作
                    selectRow = rowData;
                    //console.log("触发选择动作把row赋值给selectRow");
                    $(ed.target).combogrid('setValue', rowData.Title);//设置商品名称

                }
            });



        }

    });

    loadMainInfo();//加载主表数据源字段信息

});
//提交表单方法s
$("#submit").linkbutton({
    onClick: function () {//这里绑定的是easyui linkbutton事件
        submit();
    }
});

function submit() {
    $('#form1').form('submit', {
        url: '/sparepartszhu/ashx/sparepartszh_add.ashx',
        onSubmit: function (param) {
            var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
            if (isValid == false) {
                $.messager.alert('警告', '表单还有信息没有填完整！');
                return false;
                //$.messager.progress('close');	// 如果表单是无效的则隐藏进度条
            } else {//表单验证通过

                //var v = $('#shifu').numberbox('getValue');
                //if (v == "0.00") {
                //    //$.messager.alert('注意', '实付金额不能为0元！');
                //    //return false;

                //}

                $('#list').datagrid('acceptChanges');//datagrid接受改变
                //以下逻辑提供参考
                //if ($('#payment').numberbox('getValue') == '') {
                //alert("请先保存了再提交");
                //    $.messager.alert('警告', '请先保存再提交!');
                //    return false;
                //}









                $('#list').datagrid({ singleSelect: false });//设置允许多选
                //循环选择行 只有选择行了之后才能返回选中行
                var rows = $('#list').datagrid('getRows');//获取当前的数据行
                for (var i = 0; i < rows.length; i++) {//循环所有列
                    //不允许为负数 if (rows[i]['subjectName'] != '' && rows[i]['sumMoney'] != 0) {
                    if (rows[i]['subject'] != '') {
                        $('#list').datagrid('selectRow', i);
                        //alert(rows[i]['subject']);
                    }
                    else {
                        //alert("有项目等于0");
                    }
                }
                var rows = $('#list').datagrid('getSelections');//返回选中的行
                if (rows == '') {//判断是否返回空数组 说明没有填写数据
                    //alert("空数组");
                    $.messager.alert('注意', '请填写或保存详细数据!');
                    return false;//返回失败
                } else {
                    //alert(JSON.stringify(rows));//有数据返回数据
                }
                param.main = $("#form1").serializeJSON();  //jquery方法把表单系列化成json后提交
                //以上用法参考https://github.com/macek/jquery-serialize-object
                param.detail = JSON.stringify(rows);//param  是EasyUI格式详细表序列化成json 
                //alert(param.main);
                //return false;
                return true;
            }
        },
        success: function (d) {
            var d = eval('(' + d + ')');  // change the JSON string to javascript object    
            if (d.status == 1) {//返回1标识成功！返回0失败
                //$.messager.alert('恭喜', d.msg);
                $.messager.confirm('恭喜', d.msg, function (r) {
                    if (r) {//是否确认都关闭进货单录入窗口
                        top.$('#tabs').tabs('close', '入库单');//进货单录入窗口
                    } else {
                        top.$('#tabs').tabs('close', '入库单');//进货单录入窗口

                    }
                });

                //location = location;//刷新本地网址
            } else {
                $.messager.alert('对不起!', d.msg);
            }

            //
            //alert("成功动作");
            //$.messager.progress('close');	// 如果提交成功则隐藏进度条
        }
    });
}

//提交表单方法e

/*
*  datagrid 获取正在编辑状态的行，使用如下：
*  $('#id').datagrid('getEditingRowIndexs'); //获取当前datagrid中在编辑状态的行编号列表
*/
$.extend($.fn.datagrid.methods, {
    getEditingRowIndexs: function (jq) {
        var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
        var indexs = [];
        rows.each(function (i, row) {
            var index = row.sectionRowIndex;
            if (indexs.indexOf(index) == -1) {
                indexs.push(index);
            }
        });
        return indexs;
    }
});


//经手人
$("#ContactsId").combogrid({
    panelWidth: 350,
    required: true,
    //value:'fullname',   
    idField: 'KeyId',
    textField: 'TrueName',
    url: '/sys/ashx/userhandler.ashx?action=mydep',
    columns: [[
        { field: 'TrueName', title: '姓名', width: 60 },
        { field: 'KeyId', title: 'KeyId', width: 100 }
    ]],
    onSelect: function (rowIndex, rowData) {
        //$("#ContactsId").textbox("setValue",rowData.KeyId);//给经手人设置ID
        $("#Contacts").textbox("setValue", rowData.TrueName);//经手人
        //alert(rowData.TrueName);
    }
});







//往来单位
//初始化后加工工厂
var gys = '{ "groupOp":"AND", "rules": [{ "field":"KeyId", "op":"ge", "data":"0"}], "groups": [] }';
$("#unit").combogrid({
    delay: 500, //自动完成功能实现搜索
    mode: 'remote',//开启后系统会自动传一个参数q到后台 
    panelWidth: 350,
    required: true,
    queryParams: {
        filter: gys
    },
    //value:'fullname',   
    editable: true,
    idField: 'comname',
    //textField: 'comname',
    url: '/JydUser/ashx/JydUserHandler.ashx',
    columns: [[
        { field: 'KeyId', title: 'KeyId', width: 50 },
        { field: 'comname', title: '客户', width: 120 },
        { field: 'tell', title: '电话', width: 120 },

    ]],
    limitToList: true,//只能从下拉中选择值
    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
    onHidePanel: function () {
        var t = $(this).combogrid('getValue');//获取combogrid的值
        var g = $(this).combogrid('grid');	// 获取数据表格对象
        var r = g.datagrid('getSelected');	// 获取选择的行
        //console.log("选择的行是：" + r + "选择的值是:" + t);
        if (r == null || t != r.comname) {//没有选择或者选项不相等时清除内容
            $.messager.alert('警告', '请选择，不要直接输入!');
            $(this).combogrid('setValue', '');
        } else {
            //do something...
        }
    },
    onSelect: function (rowIndex, rowData) {
        //console.log(JSON.stringify(rowData));
        //console.log("设置的值是：" + rowData.KeyId);
        //$("#unitid").combogrid("setValue",rowData.KeyId);//给经手人设置ID
        $("#unitid").val(rowData.KeyId);//客户
        $("#unit_man").textbox("setValue", rowData.realname);//设置联系人       //top.$("#connman").textbox('setValue', rowData.realname);//经手人
        //top.$("#tel").textbox('setValue', rowData.tell)
        //alert(rowData.TrueName);
    }
});



//接件单订单号
var gys = '{ "groupOp":"AND", "rules": [{ "field":"KeyId", "op":"ge", "data":"0"}], "groups": [] }';
$("#orderid").combogrid({
    delay: 500, //自动完成功能实现搜索
    mode: 'remote',//开启后系统会自动传一个参数q到后台 
    panelWidth: 566,
    required: true,
    queryParams: {
        filter: gys
    },
    //value:'fullname',   
    editable: true,
    idField: 'OrderID',
    textField: 'OrderID',
    url: '/JydOrder/ashx/JydOrderHandler.ashx?action=sousuo',
    columns: [[
        { field: 'OrderID', title: '订单号', width: 130 },
        { field: 'comname', title: '客户', width: 130 },
        { field: 'deliveryDate', title: '交货日期', width: 130 }

    ]],
    limitToList: true,//只能从下拉中选择值
    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
    onSelect: function (rowIndex, rowData) {
        //console.log(JSON.stringify(rowData));
        //console.log("设置的值是：" + rowData.deliveryDate);
        $("#jhdate").val(rowData.deliveryDate);
        //$("#unitid").val(rowData.KeyId);//客户
        $("#unit").textbox("setValue", rowData.comname);       //top.$("#connman").textbox('setValue', rowData.realname);//经手人
        //$("#jhdate").textbox('setValue', rowData.deliveryDate);
        jieji(rowData.OrderID);
        //alert(rowData.TrueName);
    },
    onHidePanel: function () {
        var t = $(this).combogrid('getValue');//获取combogrid的值
        var g = $(this).combogrid('grid');	// 获取数据表格对象
        var r = g.datagrid('getSelected');	// 获取选择的行
        //console.log("选择的行是：" + r + "选择的值是:" + t);
        if (r == null || t != r.OrderID) {//没有选择或者选项不相等时清除内容
            $.messager.alert('警告', '请选择，不要直接输入!');
            $(this).combogrid('setValue', '');
        } else {
            //do something...
        }
    }
});
//接件名称
function jieji(value) {



    //console.log(value);
//var gys = '{ "groupOp":"AND", "rules": [{ "field":"orderid", "op":"ep", "data":"2019/7/5-3"}], "groups": [] }';
$("#ordername").combogrid({
    delay: 500, //自动完成功能实现搜索
    multiple: true, //设置允许多选
    mode: 'remote',//开启后系统会自动传一个参数q到后台 
    panelWidth: 350,
    required: true,
    queryParams: {
        orderid: value
    },
    //value:'fullname',   
    editable: true,
    idField: 'yspmc',
    textField: 'yspmc',
    url: '/JydOrder/ashx/JydOrderHandler.ashx?action=bl',
    columns: [[
        { field: 'yspmc', title: '印刷品', width: 130 },
        { field: 'orderid', title: '单号', width: 130 },
        { field: 'sl', title: '数量', width: 130 },

    ]],
    //limitToList: true,//只能从下拉中选择值
    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
    onHidePanel: function () {
        var t = $(this).combogrid('getValues');//获取combogrid的值                
        var g = $(this).combogrid('grid');	// 获取数据表格对象
        var r = g.datagrid('getSelected');	// 获取选择的行
        console.log("选择的行是：" + r + "选择的值是:" + t);
        $('#ordernameCopy').val(t);
        if (r == null) {//没有选择或者选项不相等时清除内容
            $.messager.alert('警告', '请选择，不要直接输入!');
            //$(this).combogrid('setValue', '');
        } else {
            //do something...
        }
    },
    onSelect: function (rowIndex, rowData) {
        console.log("设置的值是：" + rowData.yspmc);
        console.log(rowData.sl);
        var sla = rowData.sl;
        console.log(rowData.yspmc + "a");
        var ccc = $("#ordernuber").val();
        if (ccc) $("#ordernuber").val(ccc + ',' + sla);
        else $("#ordernuber").val(sla);
    }
    });
}
//银行
//$(".accountid").combobox({
//    valueField: 'KeyId',//用中文名作为名称
//    required: true,
//    textField: 'accountName',
//    url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
//    onSelect: function (rec) {
//        //查找附近的方法
//        //$(this).prev('.account').val(rec.accountName);

//        $("#paybank").textbox("setValue", rec.accountName);//设置隐藏ID
//    }
//});

function compute() {//计算函数
    var rows = $('#list').datagrid('getRows')//获取当前的数据行
    var numtotal = 0//
    allpricetotal = 0;//
    for (var i = 0; i < rows.length; i++) {
        numtotal += parseInt(rows[i]['buyNum']);
        //alert(rows[i]['buyAllMoney']);
        allpricetotal += parseFloat(rows[i]['buyAllMoney']);
    }
    //alert(allpricetotal);
    $("#total").numberbox('setValue', allpricetotal);
    $("#sumnum").numberbox('setValue', numtotal);
    //alert(numtotal);
    //新增一行显示统计信息
    //$('#dg').datagrid('appendRow', { itemid: '<b>统计：</b>', listprice: ptotal, unitcost: utotal });
}




//暂时废弃加载客户信息
function loadMainInfo() {
    

}
