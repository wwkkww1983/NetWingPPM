﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using NetWing.Model;//系统所有的model 都在这个命名空间里，这个还是比较有好处的。
using NetWing.BPM.Core;
using NetWing.Common;
using Omu.ValueInjecter;
using NetWing.Common.Data;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Model;
using NetWing.BPM.Core.Dal;

namespace NetWing.BPM.Admin.mjorder
{
    public partial class initsystem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {




        }

        protected void okInit_Click(object sender, EventArgs e)
        {
            if (this.initPwd.Text=="Sinba520")
            {
                SqlEasy.ExecuteNonQuery("delete from MJFinanceMain");
                SqlEasy.ExecuteNonQuery("delete from MJFinanceDetail");
                SqlEasy.ExecuteNonQuery("delete from MJOrder");
                SqlEasy.ExecuteNonQuery("delete from MJOrderDetail");
                SqlEasy.ExecuteNonQuery("update MJBankAccount set accountBalance=0");//清空银行余额
                                                                                     //清空房间使用者信息
                string clearroomsql = "update MJRooms set userids=null,usernames=null,usermobiles=null,state='空闲',add_time=null,exp_time=null,orderstart_time=null,orderend_time=null,zuqi=null,jzrs=null,OrderId=null,rzl=null,paytype=null,nextpaydate=null";
                SqlEasy.ExecuteNonQuery(clearroomsql);
                SqlEasy.ExecuteNonQuery("delete from Psi_Order");
                SqlEasy.ExecuteNonQuery("delete from Psi_BuyDetails");
                SqlEasy.ExecuteNonQuery("delete from nw_customform_user_MakeRoom");//初始化预约表
                SqlEasy.ExecuteNonQuery("update Psi_Goods set stock=0");//初始化库存为0

                this.Label1.Text = "系统初始化成功！";
                //Response.Write("系统初始化成功！");
                //写入退出日志
                LogModel log = new LogModel();
                log.BusinessName = "初始化系统";
                log.OperationIp = PublicMethod.GetClientIP();
                log.OperationTime = DateTime.Now;
                log.PrimaryKey = "";
                log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                log.TableName = "";
                log.note = "初始化系统成功";
                log.OperationType = (int)OperationType.Other;
                LogDal.Instance.Insert(log);



            }
            else
            {
                this.Label1.Text = "系统初始化失败，密码不对。请联系开发人员：大王13700605160！";
                LogModel log = new LogModel();
                log.BusinessName = "初始化系统";
                log.OperationIp = PublicMethod.GetClientIP();
                log.OperationTime = DateTime.Now;
                log.PrimaryKey = "";
                log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                log.TableName = "";
                log.note = "初始化系统失败";
                log.OperationType = (int)OperationType.Other;
                LogDal.Instance.Insert(log);
                //Response.Write("系统初始化失败，密码不对。请联系开发人员：大王13700605160！");
            }
        }


    }
}