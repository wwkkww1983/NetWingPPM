﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="initsystem.aspx.cs" Inherits="NetWing.BPM.Admin.mjorder.initsystem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            初始化密码：<asp:TextBox ID="initPwd" runat="server"></asp:TextBox>
            <asp:Button ID="okInit" runat="server" Text="确定初始化" OnClick="okInit_Click" />
            <br />
            <asp:Label ID="Label1" runat="server" Text="等待操作"></asp:Label>
            <br />
            <br />
            初始化系统将有以下操作：
            <br />
            #.清空财务主表、明细表;
            <br />
            #.设置整个系统银行余额为0;
            <br />
            #.设置清空进销存订单主表;
            <br />
            #.设置清空进销存订单明细表;
            <br />
            #.更新所有商品库存为0；

        </div>
    </form>
</body>
</html>
