﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;

namespace NetWing.BPM.Admin.common
{
    public class mjcommon
    {

        /// 获取时间戳
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp(System.DateTime time)
        {
            long ts = ConvertDateTimeToInt(time);
            return ts.ToString();
        }
        /// <summary>  
        /// 将c# DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time">时间</param>  
        /// <returns>long</returns>  
        public static long ConvertDateTimeToInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
            return t;
        }
        /// 
        /// 返回任务模板排序数字
        /// 
        public static int templatesort(int str)
        {
            int template = 0;
            int dr =  (Int32)SqlEasy.ExecuteScalar("select COUNT(*) from jydflowTasktemplate where PrintedmatterID='" + str + " '");
            //SqlDataReader dr = SqlEasy.ExecuteDataReader("");
            template = dr+1;
            return template;
        }
        /// <summary>
        /// 返回订单财务订单号 F-20170718-002 按每天00x递增
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string getedNumber(string type)
        {
            string edNumber = "";
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT   edNumber  FROM  MJFinanceMain where CONVERT(varchar(100),add_time, 23)='" + DateTime.Now.ToString("yyyy-MM-dd") + "' and edNumber like '" + type + "%' order by keyid desc");
            if (dr.Read())
            {
                string ovl = dr["edNumber"].ToString();
                string[] ovlArr = ovl.Split('-');
                ovl = ovlArr[2].ToString();
                edNumber = type + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + (Convert.ToInt16(ovl) + 1).ToString("D3");
            }
            else
            {
                edNumber = type + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + "001";
            }
            dr.Close();
            return edNumber;
        }

        public static string getedNumbere(string type)
        {
            string edNumber = "";
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT   order_sn  FROM  sparepartszhu where CONVERT(varchar(100),add_time, 23)='" + DateTime.Now.ToString("yyyy-MM-dd") + "' and order_sn like '" + type + "%' order by keyid desc");
            if (dr.Read())
            {
                string ovl = dr["order_sn"].ToString();
                string[] ovlArr = ovl.Split('-');
                ovl = ovlArr[2].ToString();
                edNumber = type + "-" + DateTime.Now.ToString("yyyy/MM/dd") + "-" + (Convert.ToInt16(ovl) + 1).ToString("D3");


            }
            else
            {
                edNumber = type + "-" + DateTime.Now.ToString("yyyy/MM/dd") + "-" + "001";
            }
            dr.Close();
            return edNumber;
        }


        public static string getedNumberJyd()
        {
            string edNumber = "";
            string ccc = "SELECT  top 1  orderid  FROM  jydOrder where CONVERT(varchar(100),adddate, 23)='" + DateTime.Now.ToString("yyyy-MM-dd") + "'  order by keyid desc";
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT  top 1  orderid  FROM  jydOrder where CONVERT(varchar(100),adddate, 23)='" + DateTime.Now.ToString("yyyy-MM-dd") + "'  order by keyid desc");

            if (dr.Read())
            {
                string ovl = dr["orderid"].ToString();
                string[] ovlArr = ovl.Split('-');
                ovl = ovlArr[3].ToString();
                //时间拆分必须统一,不然索引中的数组超出
                edNumber = DateTime.Now.ToString("yyyy-MM-dd") + "-" + (Convert.ToInt16(ovl) + 1).ToString("D");
            }

            //if (dr.Read())
            //{
            //    string ovl = dr["orderid"].ToString();
            //    string[] ovlArr = ovl.Split('-');
            //    ovl = ovlArr[3].ToString();
            //    edNumber = DateTime.Now.ToString("yyyy-MM-dd") + "-" + (Convert.ToInt16(ovl) + 1).ToString("D");
            //}
            else
            {
                //时间拆分必须统一,不然索引中的数组超出
                edNumber = DateTime.Now.ToString("yyyy-MM-dd") + "-" + "1";
            }
            dr.Close();
            return edNumber;
        }

        



        public static DataRow getDataRow(string sql)
        {
            return SqlEasy.ExecuteDataRow(sql);
        }

    }

}