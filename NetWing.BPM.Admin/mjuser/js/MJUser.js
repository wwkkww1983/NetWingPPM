var actionURL = '/MJUser/ashx/MJUserHandler.ashx';
var formurl = '/MJUser/html/MJUser.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "数据列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: 'ID', field: 'KeyId', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '姓名', field: 'fullname', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'CHS', missingMessage: '只能输入汉字' } } },
                //{ title: '用户名', field: 'username', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'loginName', missingMessage: '登录名称只允许汉字、英文字母、数字及下划线' } } },
                //{ title: '密码', field: 'password', sortable: true, width: '', hidden: false, editor: { type: 'passwordbox', options: { required: true, validType: 'safepass', missingMessage: '密码由字母和数字组成，至少6位' } } },
                { title: '证件类型', field: 'cardtype', sortable: true, width: '', editor: { type: 'combobox', options: { required: true, validType: 'CHS', url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=70', textField: 'Title', valueField: 'Code', missingMessage: '只能输入汉字' } } },
                {
                    title: '证件号码', field: 'cardid', formatter: function (value, row, index) {
                        //return row.cardid.replace(/\d{7}(\d{4})/, '*******$1');//匹配连续11位数字，并替换其中的前7位为*号
                        return row.cardid.replace(/^(.{14})(?:\d+)(.{0})$/, "$1****$2");//我这里保留的是身份证前14位和后0位
                    }, sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'idcard', missingMessage: '请输入正确的身份证号' } }
                },
                {
                    title: '性别', field: 'sex', formatter: function (value, row, index) {
                        //return new Date(value).Format("yyyy-MM-dd");
                        var res = CertificateNoParse(row.cardid);
                        return res.sex;
                    }, sortable: true, width: '', editor: { type: 'combobox', options: { required: true, validType: 'length[1,1000]', url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=69', textField: 'Title', valueField: 'Code', missingMessage: '性别必须选择' } }
                },
                {
                    title: '年龄', field: 'age', formatter: function (value, row, index) {
                        //return new Date(value).Format("yyyy-MM-dd");
                        var res = CertificateNoParse(row.cardid);
                        return res.age;
                    }, sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'age', missingMessage: '年龄必须是0到120之间的整数' } }
                },
                {
                    title: '生日', field: 'birthday', formatter: function (value, row, index) {
                        //return new Date(value).Format("yyyy-MM-dd");
                        var res = CertificateNoParse(row.cardid);
                        return res.birthday;
                    }, sortable: true, width: '', hidden: false, editor: { type: 'datebox', options: { required: true, validType: 'date', missingMessage: '请输入合法的日期' } }
                },
                { title: '联系电话', field: 'tel', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'mobile', missingMessage: '请输入正确手机号码' } } },
                { title: '紧急联系方式', field: 'jjlxr', sortable: true, width: '', },
                { title: '职业', field: 'careres', sortable: true, width: '', editor: { type: 'combobox', options: { required: true, validType: 'length[1,1000]', url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=73', textField: 'Title', valueField: 'Code', missingMessage: '职业不能为空' } } },
                { title: '职务', field: 'zw', sortable: true, width: '', },
                { title: '学历', field: 'xl', sortable: true, width: '', },
                { title: '邮箱', field: 'emaile', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'email', missingMessage: '请输入有效的电子邮件账号(例：227208@qq.com)' } } },
                { title: 'QQ', field: 'qq', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'QQ', missingMessage: '请输入正确QQ号码' } } },
                { title: '地址', field: 'address', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '微信ID', field: 'openid', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '用户头像', field: 'avater', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '余额', field: 'amount', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: true, validType: 'money', missingMessage: '请输入正确的金额' } } },
                { title: '备注', field: 'remarks', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                //{ title: '系统时间', field: 'add_time', sortable: true, editor: 'datebox', width: '', hidden: true },//主键自动判断隐藏

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //alert($('body').data('data70'));
                //alert($('body').data('data69'));
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            //onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            //onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加用户基本信息', width: 1000, height: 600, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$('#txt_cardtype').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=70', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '只能输入汉字', disabled: false });
                top.$('#txt_sex').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=69', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '性别必须选择', disabled: false });
                top.$('#txt_careres').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=73', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '职业不能为空', disabled: false });
                top.$('#txt_zw').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=81', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '职务不能为空', disabled: false });
                top.$('#txt_hy').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=82', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '行业不能为空', disabled: false });
                top.$('#txt_xl').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=83', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '学历不能为空', disabled: false });
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }

                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑用户基本信息', width: 1000, height: 600, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    top.$('#txt_KeyId').textbox('setValue', row.KeyId);
                    top.$('#txt_fullname').textbox('setValue', row.fullname);
                    top.$('#txt_username').textbox('setValue', row.username);
                    top.$('#txt_password').passwordbox('setValue', row.password);
                    top.$('#txt_cardtype').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=70', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '只能输入汉字', disabled: false, onLoadSuccess() { top.$('#txt_cardtype').combobox('setValue', row.cardtype); } });
                    top.$('#txt_cardid').textbox('setValue', row.cardid);
                    top.$('#txt_sex').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=69', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '性别必须选择', disabled: false, onLoadSuccess() { top.$('#txt_sex').combobox('setValue', row.sex); } });
                    top.$('#txt_age').textbox('setValue', row.age);
                    top.$('#txt_birthday').datebox('setValue', row.birthday);
                    top.$('#txt_tel').textbox('setValue', row.tel);
                    top.$('#txt_careres').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=73', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '职业不能为空', disabled: false, onLoadSuccess() { top.$('#txt_careres').combobox('setValue', row.careres); } });
                    top.$('#txt_emaile').textbox('setValue', row.emaile);
                    top.$('#txt_qq').textbox('setValue', row.qq);
                    top.$('#txt_address').textbox('setValue', row.address);
                    top.$('#txt_openid').textbox('setValue', row.openid);
                    top.$('#txt_avater').textbox('setValue', row.avater);
                    top.$('#txt_amount').numberspinner('setValue', row.amount);
                    top.$('#txt_remarks').textbox('setValue', row.remarks);
                    top.$('#txt_add_time').textbox('setValue', row.add_time);
                    top.$('#txt_zw').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=81', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '职务不能为空', disabled: false, onLoadSuccess() { top.$('#txt_zw').combobox('setValue', row.zw); } });
                    top.$('#txt_hy').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=82', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '行业不能为空', disabled: false, onLoadSuccess() { top.$('#txt_hy').combobox('setValue', row.hy); } });
                    top.$('#txt_xl').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=83', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '学历不能为空', disabled: false, onLoadSuccess() { top.$('#txt_xl').combobox('setValue', row.xl); } });

                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    } else {
                        msg.warning('请填写必填项！');
                    }
                    
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//调用： 

//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss"); 

//身份证计算年龄 性别 生日
function CertificateNoParse(certificateNo) {
    var pat = /^\d{6}(((19|20)\d{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])\d{3}([0-9]|x|X))|(\d{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])\d{3}))$/;
    if (!pat.test(certificateNo))
        return null;

    var parseInner = function (certificateNo, idxSexStart, birthYearSpan) {
        var res = {};
        var idxSex = 1 - certificateNo.substr(idxSexStart, 1) % 2;
        res.sex = idxSex == '1' ? '女' : '男';

        var year = (birthYearSpan == 2 ? '19' : '') +
            certificateNo.substr(6, birthYearSpan);
        var month = certificateNo.substr(6 + birthYearSpan, 2);
        var day = certificateNo.substr(8 + birthYearSpan, 2);
        res.birthday = year + '-' + month + '-' + day;

        var d = new Date(); //当然，在正式项目中，这里应该获取服务器的当前时间  
        var monthFloor = ((d.getMonth() + 1) < parseInt(month, 10) || (d.getMonth() + 1) == parseInt(month, 10) && d.getDate() < parseInt(day, 10)) ? 1 : 0;
        res.age = d.getFullYear() - parseInt(year, 10) - monthFloor;
        return res;
    }

    return parseInner(certificateNo, certificateNo.length == 15 ? 14 : 16, certificateNo.length == 15 ? 2 : 4);
}
//调用var res = CertificateNoParse(certificateNo); 
//男女res.sex
//年龄res.age
//生日res.res.birthday