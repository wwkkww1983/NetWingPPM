using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJUserDal : BaseRepository<MJUserModel>
    {
        public static MJUserDal Instance
        {
            get { return SingletonProvider<MJUserDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJUserModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}