using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJOverBll
    {
        public static MJOverBll Instance
        {
            get { return SingletonProvider<MJOverBll>.Instance; }
        }

        public int Add(MJOverModel model)
        {
            return MJOverDal.Instance.Insert(model);
        }

        public int Update(MJOverModel model)
        {
            return MJOverDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJOverDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJOverDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
