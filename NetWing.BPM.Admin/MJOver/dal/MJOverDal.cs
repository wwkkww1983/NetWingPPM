using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJOverDal : BaseRepository<MJOverModel>
    {
        public static MJOverDal Instance
        {
            get { return SingletonProvider<MJOverDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJOverModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}