using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("nw_customform_user_MakeRoom")]
	[Description("预约表")]
	public class Nw_customform_user_MakeRoomModel
	{
				/// <summary>
		/// 原始编号
		/// </summary>
		[Description("原始编号")]
		public int id { get; set; }		
		/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 表单ID
		/// </summary>
		[Description("表单ID")]
		public int form_id { get; set; }		
		/// <summary>
		/// 排序ID
		/// </summary>
		[Description("排序ID")]
		public int sort_id { get; set; }		
		/// <summary>
		/// 状态
		/// </summary>
		[Description("状态")]
		public int status { get; set; }		
		/// <summary>
		/// 是否是消息
		/// </summary>
		[Description("是否是消息")]
		public int is_msg { get; set; }		
		/// <summary>
		/// 是否置顶
		/// </summary>
		[Description("是否置顶")]
		public int is_top { get; set; }		
		/// <summary>
		/// 是否可读
		/// </summary>
		[Description("是否可读")]
		public int is_red { get; set; }		
		/// <summary>
		/// 是否热门
		/// </summary>
		[Description("是否热门")]
		public int is_hot { get; set; }		
		/// <summary>
		/// 是否系统
		/// </summary>
		[Description("是否系统")]
		public int is_sys { get; set; }		
		/// <summary>
		/// 是否是幻灯片
		/// </summary>
		[Description("是否是幻灯片")]
		public int is_slide { get; set; }		
		/// <summary>
		/// 点击量
		/// </summary>
		[Description("点击量")]
		public int click { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 门店
		/// </summary>
		[Description("门店")]
		public string maa_md { get; set; }		
		/// <summary>
		/// 房型
		/// </summary>
		[Description("房型")]
		public string maa_fx { get; set; }
        [Description("房号")]
        public string maa_fh { get; set; }
		/// <summary>
		/// 预约金
		/// </summary>
		[Description("预约金")]
		public decimal maa_price { get; set; }		
		/// <summary>
		/// 姓名
		/// </summary>
		[Description("姓名")]
		public string maa_name { get; set; }		
		/// <summary>
		/// 性别
		/// </summary>
		[Description("性别")]
		public string maa_sex { get; set; }		
		/// <summary>
		/// 身份证号码
		/// </summary>
		[Description("身份证号码")]
		public string maa_idc { get; set; }		
		/// <summary>
		/// 手机号码
		/// </summary>
		[Description("手机号码")]
		public string maa_tel { get; set; }		
		/// <summary>
		/// 入住人数
		/// </summary>
		[Description("入住人数")]
		public string maa_num { get; set; }		
		/// <summary>
		/// 预约看房时间
		/// </summary>
		[Description("预约看房时间")]
		public DateTime maa_yykfsj { get; set; }		
		/// <summary>
		/// 预计入住时间
		/// </summary>
		[Description("预计入住时间")]
		public DateTime maa_yyrzsj { get; set; }		
		/// <summary>
		/// 预计退房时间
		/// </summary>
		[Description("预计退房时间")]
		public DateTime maa_yytfsj { get; set; }		
		/// <summary>
		/// 支付状态
		/// </summary>
		[Description("支付状态")]
		public string maa_paystate { get; set; }		
		/// <summary>
		/// 微信支付订单号
		/// </summary>
		[Description("微信支付订单号")]
		public string maa_wxpaynum { get; set; }		
		/// <summary>
		/// 商户订单号
		/// </summary>
		[Description("商户订单号")]
		public string maa_shpaynum { get; set; }		
		/// <summary>
		/// 预约者微信ID
		/// </summary>
		[Description("预约者微信ID")]
		public string maa_openid { get; set; }		
		/// <summary>
		/// 系统时间
		/// </summary>
		[Description("系统时间")]
		public DateTime maa_addtime { get; set; }		
		/// <summary>
		/// 入住状态
		/// </summary>
		[Description("入住状态")]
		public string maa_rzstate { get; set; }		
		/// <summary>
		/// 房间名称
		/// </summary>
		[Description("房间名称")]
		public string maa_fjname { get; set; }		
		/// <summary>
		/// 房间ID
		/// </summary>
		[Description("房间ID")]
		public string maa_fjid { get; set; }		
		/// <summary>
		/// 支付时间
		/// </summary>
		[Description("支付时间")]
		public DateTime maa_patime { get; set; }
        /// <summary>
        /// 系统单号
        /// </summary>
        [Description("系统单号")]
        public string edNumber { get; set; }
        [Description("退定金单号")]
        public string tuiedNumber { get; set; }
        [Description("部门数据所有者")]
        public int depid { get; set; }
        [Description("数据所有者")]
        public int ownner { get; set; }
        [Description("备注")]
        public string note { get; set; }
        /// <summary>
        /// 银行账户ID
        /// </summary>
        public int accountid {
            get;
            set;
        }
        /// <summary>
        /// 银行账户名称
        /// </summary>
        public string account {
            get;set;
        }


        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}