using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Nw_customform_user_MakeRoomBll
    {
        public static Nw_customform_user_MakeRoomBll Instance
        {
            get { return SingletonProvider<Nw_customform_user_MakeRoomBll>.Instance; }
        }

        public int Add(Nw_customform_user_MakeRoomModel model)
        {
            return Nw_customform_user_MakeRoomDal.Instance.Insert(model);
        }

        public int Update(Nw_customform_user_MakeRoomModel model)
        {
            return Nw_customform_user_MakeRoomDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Nw_customform_user_MakeRoomDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Nw_customform_user_MakeRoomDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
