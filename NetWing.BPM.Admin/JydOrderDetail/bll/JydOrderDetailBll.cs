using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydOrderDetailBll
    {
        public static JydOrderDetailBll Instance
        {
            get { return SingletonProvider<JydOrderDetailBll>.Instance; }
        }

        public int Add(JydOrderDetailModel model)
        {
            return JydOrderDetailDal.Instance.Insert(model);
        }

        public int Update(JydOrderDetailModel model)
        {
            return JydOrderDetailDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydOrderDetailDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydOrderDetailDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
        
    }
}
