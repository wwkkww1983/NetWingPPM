using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class JydOrderDetailDal : BaseRepository<JydOrderDetailModel>
    {
        public static JydOrderDetailDal Instance
        {
            get { return SingletonProvider<JydOrderDetailDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(JydOrderDetailModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}