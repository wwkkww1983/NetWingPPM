using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJaccountingSubjectsDal : BaseRepository<MJaccountingSubjectsModel>
    {
        public static MJaccountingSubjectsDal Instance
        {
            get { return SingletonProvider<MJaccountingSubjectsDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJaccountingSubjectsModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}