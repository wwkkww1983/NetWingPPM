using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJaccountingSubjectsBll
    {
        public static MJaccountingSubjectsBll Instance
        {
            get { return SingletonProvider<MJaccountingSubjectsBll>.Instance; }
        }

        public int Add(MJaccountingSubjectsModel model)
        {
            return MJaccountingSubjectsDal.Instance.Insert(model);
        }

        public int Update(MJaccountingSubjectsModel model)
        {
            return MJaccountingSubjectsDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJaccountingSubjectsDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJaccountingSubjectsDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
