﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.SessionState;
using NetWing.Model;
using NetWing.Bll;
using Omu.ValueInjecter;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Excel;
using NetWing.Common.IO;
using NetWing.Common.IO.DirFile;
using NetWing.Common.Data.Filter;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Model;
using NetWing.BPM.Core.Dal;

namespace NetWing.BPM.Admin.mjrooms.ashx
{
    /// <summary>
    /// addman 的摘要说明
    /// </summary>
    public class addman : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            int k;
            var json = HttpContext.Current.Request["json"];
            var rpm = new RequestParamModel<MJRoomsModel>(context) { CurrentContext = context, Action = context.Request["action"] };
            if (!string.IsNullOrEmpty(json))
            {
                rpm = JSONhelper.ConvertToObject<RequestParamModel<MJRoomsModel>>(json);
                rpm.CurrentContext = context;
            }

            switch (rpm.Action)
            {
                case "edit":

                    SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
                    try
                    {
                        MJOrderModel d = new MJOrderModel();
                        d.InjectFrom(rpm.Entity);
                        d.KeyId = rpm.KeyId;
                        //判断居住人数
                        string[] jzrsarr = d.usermobiles.Split(',');
                        d.jzrs = jzrsarr.Length;//得到正确居住人数

                        string[] uporderfiles = { "usernames", "useridcards", "usermobiles", "jzrs" };
                        DbUtils.tranUpdateOnlyUpdateFields(d, tran, uporderfiles);//只更新指定的行
                        MJRoomsModel m = new MJRoomsModel();
                        m.KeyId = int.Parse(context.Request["roomid"]);
                        m.jzrs = d.jzrs;
                        m.usernames = d.usernames;
                        m.usermobiles = d.usermobiles;
                        m.useridcards = d.useridcards;
                        DbUtils.tranUpdateOnlyUpdateFields(m, tran, uporderfiles);
                        tran.Commit();//提交事务
                        LogModel log = new LogModel();
                        log.BusinessName = "增减入住人办理成功";
                        log.OperationIp = PublicMethod.GetClientIP();
                        log.OperationTime = DateTime.Now;
                        log.PrimaryKey = "";
                        log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                        log.TableName = "MJOrder";
                        log.note = "增减入住人办理成功";
                        log.OperationType = (int)OperationType.Other;
                        LogDal.Instance.Insert(log);

                        context.Response.Write("{\"status\":1,\"msg\":\"退房登记成功！\"}");




                    }
                    catch (Exception e)
                    {

                        LogModel log = new LogModel();
                        log.BusinessName = "增减入住人办理失败";
                        log.OperationIp = PublicMethod.GetClientIP();
                        log.OperationTime = DateTime.Now;
                        log.PrimaryKey = "";
                        log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                        log.TableName = "MJOrder";
                        log.note = e.Message;
                        log.OperationType = (int)OperationType.Other;
                        LogDal.Instance.Insert(log);

                        tran.Rollback();//回滚事务
                        context.Response.Write("{\"status\":1,\"msg\":\"" + e.Message + "\"}");
                    }








                    break;

                default:
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}