﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.SessionState;
using NetWing.Model;
using NetWing.Bll;
using Omu.ValueInjecter;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Excel;
using NetWing.Common.IO;
using NetWing.Common.IO.DirFile;
using NetWing.Common.Data.Filter;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json.Linq;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Model;
using NetWing.BPM.Core.Dal;

namespace NetWing.BPM.Admin.mjrooms.ashx
{
    /// <summary>
    /// istui 的摘要说明
    /// </summary>
    public class istui : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            if (context.Request["KeyId"].ToString() == "" && context.Request["roomid"].ToString() == "")
            {
                context.Response.Write("{\"status\":0,\"msg\":\"错误：系统没有获取到ID值\"}");
            }
            else
            {

                SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
                try
                {
                    MJOrderModel o = new MJOrderModel();
                    o.KeyId = int.Parse(context.Request["KeyId"].ToString());
                    o.istui = context.Request["istui"].ToString();
                    o.istuinote = context.Request["istuinote"].ToString();
                    string[] upfields = { "istui", "istuinote"};
                    DbUtils.tranUpdateOnlyUpdateFields(o, tran, upfields);
                    DataRow dr = SqlEasy.ExecuteDataRow("select * from MJRooms where Keyid="+ context.Request["roomid"].ToString() + "");
                    MJRoomsModel r = new MJRoomsModel();
                    r.KeyId = int.Parse(dr["keyid"].ToString());
                    r.istui = context.Request["istui"].ToString();
                    r.istuinote = context.Request["istuinote"].ToString();
                    DbUtils.tranUpdateOnlyUpdateFields(r, tran, upfields);
                    tran.Commit();//提交事务

                    if (context.Request["istui"].ToString()=="")
                    {
                        context.Response.Write("{\"status\":1,\"msg\":\"退房登记已取消！\"}");
                    }
                    else
                    {
                        context.Response.Write("{\"status\":1,\"msg\":\"退房登记成功！\"}");
                    }

                    
                }
                catch (Exception e)
                {
                    context.Response.Write("{\"status\":1,\"msg\":\""+e.Message+"\"}");
                }


            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}