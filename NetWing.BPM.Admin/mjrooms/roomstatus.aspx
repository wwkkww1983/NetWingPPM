﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="roomstatus.aspx.cs" Inherits="NetWing.BPM.Admin.mjrooms.roomstatus" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/css.css" rel="stylesheet" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="../scripts/layer/layer.js"></script>
    <!--引入lhgdialog插件-->
    <script src="/scripts/lhgdialog/lhgdialog.min.js?skin=idialog"></script>
    <script src="../scripts/jquery.tooltip.js"></script>
    <link href="../scripts/jquery.tooltip.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $(".item").tooltip();
        });

    </script>

    <script>
        function OrderPrint(mtitle, murl) {
            //alert(murl);
            var dialog = $.dialog({
                title: mtitle,
                content: 'url:' + murl,
                min: false,
                max: false,
                lock: true,
                width: 1000,
                height: 650,
            });
        }
    </script>
    <style>
        .col-xs-13 {
            width: 6%;
            float: left;
            height: 70px;position: relative;
        }

        .panel-image {
            margin: -10px 0 0 0;
        }

        .text_ellipsis {
            text-align: center;
            font-family: "微软雅黑";
            font-weight: normal;
            margin: 0px 0 0 0;
            font-size: 14px;
        }

        .fa {
            margin: 0 auto;
            text-align: center;
            color: #000;
        }

            .fa img {
                margin: 0 auto;
                text-align: center;
                display: block;
            }

        #box {
            background: rgba(8,97,175,0.5) !important;
            background: #000;
        }

        .top_text {
            font-size: 16px;
            margin: 0 0 0px 15px;
            width: 100%;
            border-bottom: #0861af 2px solid;
            padding-bottom: 10px;
        }

        #DropDownList1, #DropDownList2 {
            font-size: 16px;
            font-family: "微软雅黑";
        }

        #laballroom {
            background: #FF6600;
            color: #fff;
            padding: 0 5px 0 5px;
            margin: 0 5px 0 5px;
            border-radius: 4px;
        }

        #labruzhu {
            background: #f00;
            color: #fff;
            padding: 0 5px 0 5px;
            margin: 0 5px 0 5px;
            border-radius: 4px;
        }

        #labkong {
            background: #99CC33;
            color: #fff;
            padding: 0 5px 0 5px;
            margin: 0 5px 0 5px;
            border-radius: 4px;
        }

        #labkzl {
            background: #66CCCC;
            color: #fff;
            padding: 0 5px 0 5px;
            margin: 0 5px 0 5px;
            border-radius: 4px;
        }

        #labpjzj {
            background: #6699CC;
            color: #fff;
            padding: 0 5px 0 5px;
            margin: 0 5px 0 5px;
            border-radius: 4px;
        }

.dingtui{width:100%;font-size:12px !important;}
.dingtui h1, .dingtui h2{font-weight:normal;}
.dingtui h1{position: absolute;top:-8px; left:0;font-size:12px;width:20px; height:20px; background:#333366;display:block;color:#fff; text-align:center;}
.dingtui h2{position: absolute;top:-10px; right:0;font-size:12px;width:20px; height:20px; background:#660033;display:block; color:#fff; text-align:center;}

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <!--顶部那条-->
        <div class="top_text">
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="=000">费用(全部)</asp:ListItem>
                <asp:ListItem Value="&lt;0">已逾期</asp:ListItem>
                <asp:ListItem Value="=0">今天到期</asp:ListItem>
                <asp:ListItem Value="&lt;3">3天内续费</asp:ListItem>
                <asp:ListItem Value="&lt;7">7天内续费</asp:ListItem>
                <asp:ListItem Value="&lt;10">10天内续费</asp:ListItem>
                <asp:ListItem Value="&lt;30">30天内续费</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="000">合同(全部)</asp:ListItem>
                <asp:ListItem Value="&lt;30">30天内合同到期</asp:ListItem>
                <asp:ListItem Value="&lt;15">15天内合同到期</asp:ListItem>
                <asp:ListItem Value="&lt;7">7天内合同到期</asp:ListItem>
                <asp:ListItem Value="&lt;=0">合同已到期</asp:ListItem>
            </asp:DropDownList>
            总<asp:Label ID="laballroom" runat="server" Text="Label"></asp:Label>间房，入住<asp:Label ID="labruzhu" runat="server" Text="Label"></asp:Label>间房，空置<asp:Label ID="labkong" runat="server" Text="Label"></asp:Label>间房 空置率：<asp:Label ID="labkzl" runat="server" Text="Label"></asp:Label>%,平均租金<asp:Label ID="labpjzj" runat="server" Text="Label"></asp:Label>,总入住率<asp:Label ID="zrzl" runat="server" Text=""></asp:Label>%
        </div>


<div style="text-align:center;margin:10px 0 0 0;"><font color="#e6b8b7">浅紫色:已预约退房</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color="#ff0000">浅红色:费用5天内到期</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color="#92cddc">浅蓝色:费用5-7天内到期</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color="#9bbb59">浅绿色:费用7-10天到期</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color="#fabf8f">浅黄色:10-365天到期</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color="#000000">黄色:空房</font></div>
        

        <div class=" p10" style="margin-bottom: 30px">
            <div class="row clearspace">
                <div class="col-xs-11">
                    <!--循环开始-->
                    <%foreach (DataRow dr in dt.Rows)
                        { %>
                    <%
                        //#F1C40F 30天内过期
                        //#ff5a5f
                        string color = "#f4f249";//默认颜色
                        //if (dr["state"].ToString() == "租赁中")
                        //{
                        //    color = "#e37932";//租赁中
                        //}
                        if (!string.IsNullOrEmpty(dr["add_time"].ToString()))//如果添加时间不为空
                        {
                            DateTime dtaa = DateTime.Parse(dr["add_time"].ToString());
                            DateTime dtee = DateTime.Parse(dr["exp_time"].ToString());
                            DateTime nextpayd = DateTime.Parse(dr["nextpaydate"].ToString());//下次交租时间
                            DateTime dtnn = DateTime.Now;
                            if ((nextpayd - dtnn).Days <= 5)//5天内该交房租的用红色
                            {
                                color = "#ff0000";//已逾期
                            }

                            if ((nextpayd - dtnn).Days > 5 && (nextpayd - dtnn).Days <=7)
                            {
                                color = "#92cddc";//5-7天该交房租的 蓝色
                            }


                            if ((nextpayd - dtnn).Days > 7 && (nextpayd - dtnn).Days <= 10)
                            {
                                color = "#9bbb59";//7-10浅绿色
                            }

                            if ((nextpayd - dtnn).Days >= 11 && (nextpayd - dtnn).Days <= 365)
                            {
                                color = "#fabf8f";//10天以上30天以下该交租的灰色
                            }

                            if (dr["istui"].ToString()=="是")
                            {
                                color = "#e6b8b7";//已经预约退房
                            }


                            //if ((nextpayd - dtnn).Days >= 31)
                            //{
                            //    color = "#429406";//31天以上该交房租的绿色
                            //}

                            //if ((dtee - dtnn).Days >= 15 && (dtee - dtnn).Days <= 30)
                            //{
                            //    color = "#FF6633";//15-30tian
                            //}

                            //if ((dtee - dtnn).Days >= 1 && (dtee - dtnn).Days <= 15)
                            //{
                            //    color = "#CC0099";//15-30tian
                            //}

                        }
                    %>
                    <div class="room">
                        <div class="col-xs-13 border item clearspace" style="margin: 6px; cursor: pointer; background-color: <%=color%>">
                            <div class="text-center listing-img panel-image" style="">
                                <!--<i class="fa">
                                    <img src="img/icon01.png"></i>-->
                                <p style="margin: 12px 0 0 26px; color: #F1C40F; font-size: 14px;"><i class="fa fa-weixin "><%=dr["jzrs"].ToString() %></i>人</p>

                                <h4 class="text_ellipsis" style="color: #000000"><%=dr["roomnumber"].ToString()%></h4>

                                <p style="margin: 0px 0 0 0; font-size: 14px; text-align: center;">入住率：<%=dr["rzl"].ToString() %>%</p>

                            </div>
                            <%
                                

                                if (!string.IsNullOrEmpty(dr["add_time"].ToString()))
                                {%>
                            <div class="tooltip_description" orderid="<%=dr["OrderId"].ToString() %>" roomid="<%=dr["keyid"].ToString() %>" userids="<%=dr["userids"].ToString() %>" style="display: none" title="<%=dr["vest"].ToString() %><%=dr["roomnumber"].ToString()%> <%=dr["price"].ToString() %>/月">
                                <%
                                    DateTime dta = DateTime.Parse(dr["add_time"].ToString());
                                    DateTime dte = DateTime.Parse(dr["exp_time"].ToString());
                                    DateTime dtn = DateTime.Now;
                                    DateTime osta = DateTime.Parse(dr["orderstart_time"].ToString());
                                    DateTime oend = DateTime.Parse(dr["orderend_time"].ToString());
                                    DateTime nextpaydate = DateTime.Parse(dr["nextpaydate"].ToString());//下次交租时间


                                %>

                                <p><span>租期：<span><%=DateTime.Parse(dr["orderstart_time"].ToString()).ToString("yyyy-MM-dd") %></span></span> ~ <span><%=DateTime.Parse(dr["orderend_time"].ToString()).ToString("yyyy-MM-dd") %></span><span class="text-primary pull-right">共<%=(oend-osta).Days %>天</span></p>
                                <p><span>姓名：<span><%=dr["usernames"].ToString() %></span></span></p>
                                <p>电话：<span class="ml10"><%=dr["usermobiles"].ToString() %></span></p>
                                <p>证件号：<span class="ml10"><%=dr["useridcards"].ToString() %></span></p>
                                <p>上次交租时间：<span><%=DateTime.Parse(dr["add_time"].ToString()).ToString() %></span></p>
                                <p>下次交租时间：<span><%=DateTime.Parse(dr["nextpaydate"].ToString()).ToString("yyyy-MM-dd") %></span><span class="ml10"> 离下次交租还有<b><%=(nextpaydate-dtn).Days %></b>天</span></p>

                                <p><span>备注：<span><%if (dr["istui"].ToString()=="是") {%>   <%=dr["istuinote"].ToString() %>  <% }%> <%if (dr["isding"].ToString()=="是") {%>   <%=dr["isdingnote"].ToString() %>  <% }%></span></span></p>
                            </div>
                            <%} %>
<div class="dingtui"><%if (dr["isding"].ToString() == "是")
                                   {%><h1>定</h1><%} %><%if (dr["istui"].ToString() == "是")
                                   {%><h2>退</h2><%} %></div>
                        </div>
                    </div>



                    <% } %>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
