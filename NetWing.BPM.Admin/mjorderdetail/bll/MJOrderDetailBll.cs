using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJOrderDetailBll
    {
        public static MJOrderDetailBll Instance
        {
            get { return SingletonProvider<MJOrderDetailBll>.Instance; }
        }

        public int Add(MJOrderDetailModel model)
        {
            return MJOrderDetailDal.Instance.Insert(model);
        }

        public int Update(MJOrderDetailModel model)
        {
            return MJOrderDetailDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJOrderDetailDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJOrderDetailDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
