﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="MJOrderMain.aspx.cs" Inherits="NetWing.BPM.Admin.MJOrderMain.MJOrderMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



    <!-- 工具栏按钮 -->
    <!-- datagrid 列表 -->
    <div id="tt" class="easyui-tabs" style="width: ; height: ;">
        <div title="入住办理" style="padding: 20px; display: none;">
            特别提示：退房宽带费不退，房租、垃圾费、停车费等按月计费项目不足一个月按一个月计算，就餐卡及电费卡剩余费用及押金项目全额退还，房间设施按入住清单检查验收，丢失损坏按价赔。<br />
            <br />
            <!--请修改项目顺序和修改样式-->
            <!--系统标识：<input id="KeyId" class="easyui-textbox" name="KeyId" style="width: 100px;">-->
            <!--
            客户姓名：<input id="users" name="users" value="" style="width: 100px; display: none;">
            <input id="usersid" name="usersid" class="easyui-combobox" value="" style="width: 100px; display: ;">-->
            <input id="roomno" value="" name="roomno" style="width: 100px; display: none;">
            房间：<input id="roomid" name="roomid" class="easyui-combobox" style="width: 100px; display: ;">
            <!--手机号：<input id="mobile" data-options="required:true,validType:'mobile'" class="easyui-textbox" name="mobile" style="width: 100px;">-->
            <!--系统改进说明 用户姓名和用户ID、手机号从新增录入获取-->
            <!--不需要隐藏
            入住时间：<input id="add_time" class="easyui-datetimebox" name="add_time" style="width: 100px;">
          更新时间：<input id="up_time" class="easyui-datetimebox" name="up_time" style="width: 100px;">
            单号：<input id="orderid" name="edNumber" class="easyui-textbox" value="<%=edNumber%>" data-options="readonly:true" style="width: 120px">
            <br /><br />-->
            租期： 
            <select name="zuqi" id="zuqi">
                <option value="1" selected="selected">1个月</option>
                <option value="2">2个月</option>
                <option value="3">3个月</option>
                <option value="4">4个月</option>
                <option value="5">5个月</option>
                <option value="6">6个月</option>
                <option value="7">7个月</option>
                <option value="8">8个月</option>
                <option value="9">9个月</option>
                <option value="10">10个月</option>
                <option value="11">11个月</option>
                <option value="12">12个月</option>
                <option value="24">二年</option>
                <option value="36">三年</option>
            </select>从<input id="orderstart_time" value="<%=DateTime.Now.ToString("yyyy-MM-dd") %>" class="easyui-datebox" data-options="required:true,validType:'md[\'2012-11-12\']'" name="orderstart_time" style="width: 100px;" />到<input id="orderend_time" value="<%=DateTime.Now.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") %>" data-options="required:true,validType:'md[\'2012-11-12\']'" class="easyui-datebox" name="orderend_time" style="width: 100px;">
            居住人数：<input id="jzrs" name="jzrs" value="1" class="easyui-numberspinner" style="width: 80px;" required="required" data-options="min:1,max:20,editable:false">
            付款方式:<select id="paytype" name="paytype" style="width: 100px;"> </select>
            下次付款时间: 
            <input id="nextpay" name="nextpay" type="text" style="width: 100px" class="easyui-datebox" required="required">
           
            <div id="nextpayno" style="display:none;"></div>
            <br /><br />
            <div id="jzrsdiv"><!--居住人-->
            </div>
            <br />
            温馨提示:第一个入住人为签合同人。入住手续办完后，信息录入客户信息。请操作员补全客户信息。紧急联系人及联系方式在用户信息里查看。
            <br />
            <table id="list"></table>
            <br />
            <br />
            合计金额：<input id="total" class="easyui-numberbox" data-options="min:0,precision:2" name="total" style="width: 100px;">
            经手人:<input id="contact" name="contact" type="" value="" style="width: 100px; display: none;">
            <input id="contactid" name="contactid" type="" value="" style="width: 100px; display: none;">
            <!--隐藏收款部门
            <input id="dep" name="dep"  type="text" value="" style="width:100px;display:none;"> 
            部门:<input id="depid" name="depid"  type="" value="" style="width:100px"> -->
            <br />
            <br />
            <div id="paytypediv">
                <li>
                    <input  class="account" name="account" value="" style="display: none;" />
                    收款方式:<input id="accountid" name="accountid" class="accountid" value="" />
                    实收金额:<input type="text" name="shifu" class="easyui-numberbox shifu" value="0" style="width: 100px;" data-options="min:0,precision:2"><a id="addpaytype" onclick="addpaytype();"  href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'">增加</a>
                </li>
                
            </div>
            <br />
            <br />
            <a id="submit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'">提交</a>
       <!--     <a id="btnprint" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-printer'">打印</a>-->

        </div>

    </div>


    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>

    <!-- 引入主表js文件 -->
    <script src="js/MJOrderMain.js"></script>
    <script>
        var DeviceMain;//主头

    </script>
</asp:Content>



