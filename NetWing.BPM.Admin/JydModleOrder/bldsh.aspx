﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bldsh.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.bldsh" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>

<!DOCTYPE html>
<html>
<head>
    <title>审核</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.
">

    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
</head>

<body ontouchstart>


    <header class='demos-header'>
    </header>
    <%if (data >= 1)
        {%>
    <%if (Dt.Rows.Count < 1)
        {%>
    <div class="weui-msg">
        <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
        <div class="weui-msg__text-area">
            <h2 class="weui-msg__title">暂无审核</h2>
        </div>
        <div class="weui-msg__opr-area">
            <p class="weui-btn-area">
                <a href="/JydModleOrder/JydWapindex.aspx" class="weui-btn weui-btn_primary">返回首页</a>
            </p>
        </div>
        <div class="weui-msg__extra-area">
            <div class="weui-footer">
                <p class="weui-footer__links">
                    <a href="javascript:void(0);" class="weui-footer__link"><%=ConfigHelper.GetValue("systemName") %></a>
                </p>
            </div>



        </div>
    </div>
    <%}
        else
        {%>


    <%foreach (DataRow dataRow in Dt.Rows)
        {%>
    <div class="weui-form-preview">
       
        <div class="weui-form-preview__bd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件单号</label>
                <em class="weui-form-preview__value"><%=dataRow["order_sn"].ToString() %></em>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">商品名称</label>
                <span class="weui-form-preview__value"><%=dataRow["ordername"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">商品数量</label>
                <span class="weui-form-preview__value"><%=dataRow["ordernuber"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">商品规格</label>
                <span class="weui-form-preview__value"><%=dataRow["gg"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">客户名称</label>
                <span class="weui-form-preview__value"><%=dataRow["comname"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">



                <label class="weui-form-preview__label">备注</label>
                <span class="weui-form-preview__value"><%=dataRow["unit_man"].ToString() %></span>
            </div>
        </div>
        <div class="weui-form-preview__ft">
            <a class="weui-form-preview__btn weui-form-preview__btn_default" href="javascript:" onclick="butongguo('<%=dataRow["keyid"].ToString() %>' , '<%=dataRow["order_sn"].ToString() %>')">不通过</a>
            <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="/JydWodrmb/print_order_sn.aspx?order_sn=<%=dataRow["order_sn"].ToString() %>">查看详细</a>
            <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="javascript:" onclick="tongguo('<%=dataRow["keyid"].ToString() %>' , '<%=dataRow["order_sn"].ToString() %>')">通过</a>
        </div>
    </div>
    <br />
    <%} %>
    <%} %>
    <%}
        else
        {%>
    <div>


        <div class="weui-msg">
            <div class="weui-msg__icon-area"><i class="weui-icon-warn weui-icon_msg-primary"></i></div>
            <div class="weui-msg__text-area">




                <h2 class="weui-msg__title">您没有权限</h2>
            </div>
            <div class="weui-msg__opr-area">
                <p class="weui-btn-area"><a href="/JydModleOrder/JydWapindex.aspx" class="weui-btn weui-btn_primary">首页</a></p>
            </div>
            <div class="weui-msg__extra-area">



                <div class="weui-footer">
                    <p class="weui-footer__links">
                        <a href="javascript:void(0);" class="weui-footer__link"><%=ConfigHelper.GetValue("systemName") %></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <%} %>
    <script src="dist/lib/jquery-2.1.4.js"></script>
    <script src="dist/lib/fastclick.js"></script>
    <script src="dist/js/jquery-weui.js"></script>
    <script>
        function butongguo(id, order) {
            console.log(order);
            $.showLoading("数据加载中");
            $.ajax({
                url: "/JydModleOrder/ashx/ajax_submit.ashx",
                type: "POST",
                dataType: "JSON",
                data: {
                    action: 'statusorder_sn',
                    order: '' + order + '',
                    keyid: id
                },




                success: function (e) {
                    console.log(e);
                    $.hideLoading();
                    if (e.status == 0) {
                        $.alert(e.msg);
                        window.location.href = window.location.href + "?id=" + 10000 * Math.random();
                    } else {
                        $.alert(e.msg);
                    }
                }

            });
        }

        function tongguo(id, order) {
            $.showLoading("数据加载中");
            $.ajax({
                url: "/JydModleOrder/ashx/ajax_submit.ashx",
                type: "POST",
                dataType: "JSON",
                data: {
                    action: 'statusorder_sn',
                    keyid: id,
                    order: '' + order + ''
                },
                success: function (e) {
                    $.hideLoading();
                    if (e.status == 0) {
                        $.alert(e.msg);
                        window.location.href = window.location.href + "?id=" + 10000 * Math.random();
                    } else {
                        $.alert(e.msg);




                    }
                }



            });
        }
    </script>
</body>
</html>
