﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JydProcessing.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.JydProcessing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <title>二维码加工</title>
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <style>
        .weui-form-preview__label {
            color: #000;
            width: 90px;
            margin-right: 0;
            padding-right: 1em;
            text-align: right;
            text-align-last: right;
        }

        .weui-form-preview__item {
            border-top: solid 1px #ececec;
        }

        .weui-form-preview__value {
            text-align: left;
            padding-left: 1em;
            border-left: solid 1px #ececec;
            min-height: 22px;
            line-height: 22px;
            padding: 3px;
        }

        .weui-pull-to-refresh {
            margin-top: -80px;
            transition: transform .4s;
        }

            .weui-pull-to-refresh.refreshing {
                transform: translate3d(0, 80px, 0);
            }

        .weui-pull-to-refresh__layer {
            padding: 20px 0 0 0;
            height: 60px;
            line-height: 60px;
        }

        .status {
            height: 20px;
            line-height: 20px;
            font-size: 12px;
        }

        .circle-wrap {
            line-height: 100%;
        }

        .circle {
            height: 40px;
            line-height: 40px;
            width: 40px;
            border-radius: 50%;
            font-size: 12px;
            display: inline-block;
            border: 1px solid #3cc51f;
        }
    </style>
</head>
<body ontouchstart>
    <div class="weui-pull-to-refresh__layer">
        <div class="circle-wrap">
            <div id="circle" class="circle"></div>
        </div>
        <div class="status">
            <div class="down">下拉刷新</div>
            <div class="up">释放刷新</div>
            <div class="refresh">正在刷新</div>
        </div>
    </div>


     <div class="weui-msg" style="padding-top: 0;">
        <div class="weui-cells__title" style="text-align: center;">--- 二维码无效？下拉试试 ---</div>
        <div class="weui-msg__icon-area" style="margin-bottom: 0;" id="CodeBox">
            <%--<img src="<%=qrcode %>">--%>
        </div>
        <div class="weui-msg__text-area">
            <p class="weui-msg__desc">订单号<%=model.OrderID %></p>
        </div>
        <div class="weui-msg__opr-area">
            <p class="weui-btn-area">
                <a href="JydWapindex.aspx" class="weui-btn weui-btn_primary">返回</a>
            </p>
        </div>
        <div class="weui-msg__extra-area" style="position: relative; margin: 20px auto;">
            <div class="weui-footer">
                <p class="weui-footer__links">
                    <a href="javascript:void(0);" class="weui-footer__link">聚源达印刷厂</a>
                </p>
                <p class="weui-footer__text"><%--<%=copyRight %>--%></p>
            </div>
        </div>
    </div>
    <script src="js/jquery-2.1.4.js"></script>
    <script src="js/fastclick.js"></script>
    <script>
        $(function () {
            FastClick.attach(document.body);
        });
    </script>
    <script src="dist/js/jquery-weui.min.js"></script>
    <script>
        var $circle = $("#circle")
        $(document.body).pullToRefresh({
            distance: 30,
            onRefresh: function () {
                $.showLoading("正在获取二维码...");
                setTimeout(function () {
                    $.ajax({
                        url: '/netwing/request.ashx',
                        type: 'POST',
                        data: {
                            action: 'GetQRCode',
                            KeyId: '<%=KeyId%>'
                        },
                        dataType: "json",
                        success: function (dd) {
                            $.hideLoading();
                            $(document.body).pullToRefreshDone();
                            if (dd.code == 0) {
                                $('#CodeBox').html('<img src="../' + dd.QRCodeUrl + '">');
                                $.toast(dd.msg, "success");
                            } else {
                                $.toast(dd.msg, "cancel");
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $.hideLoading();
                            $(document.body).pullToRefreshDone();
                        }
                    })
                }, 2000);
            },
            onPull: function (percent) {
                if (percent > 100) percent = 100
                $circle.css('background-image', 'linear-gradient(0deg, #3cc51f ' + percent + '%, #3cc51f ' + percent + '%, transparent 50%, transparent 100%)')
            }
        });
    </script>
</body>
</html>
