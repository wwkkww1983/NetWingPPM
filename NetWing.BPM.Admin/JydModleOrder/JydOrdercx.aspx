﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JydOrdercx.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.JydOrdercx" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <title>查询订单</title>
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <style>
        .weui-search-bar {
            z-index: 999;
        }

        .weui-panel {
            margin-top: auto;
        }

        .weui-content {
            background-color: #FFFFFF;
            width: 100%;
            height: 100%;
            margin-top: -45px;
            position: absolute;
            margin-bottom: 45px;
            padding: 0px;
            overflow: hidden;
        }
    </style>
</head>
<body>
    <div class="weui-search-bar" id="searchBar">
        <form class="weui-search-bar__form" id="search_from">
            <div class="weui-search-bar__box" style="background: #fff; border-radius: 5px;">
                <i class="weui-icon-search"></i>
                <input type="search" class="weui-search-bar__input" id="searchInput" placeholder="输入订单号" required="">
                <a href="javascript:" class="weui-icon-clear" id="searchClear"></a>
            </div>
            <label class="weui-search-bar__label" id="searchText">
                <i class="weui-icon-search"></i>
                <span>输入订单号</span>
            </label>
        </form>
        <a href="javascript:" class="weui-search-bar__cancel-btn" id="searchCancel">取消</a>
    </div>
    <div class="weui-content">
        <div class="list-main-mian infinite weui-pull-to-refresh" id="listwrap" style="height: 100%; margin-top: 1px; overflow: auto; z-index: 1">
            <!--下拉刷新-->
            <%--<div class="weui-pull-to-refresh__layer" style="padding: 5px;">
                <div class="weui-pull-to-refresh__arrow"></div>
                <div class="weui-pull-to-refresh__preloader"></div>
                <div class="down">下拉刷新</div>
                <div class="up">释放刷新</div>
                <div class="refresh">正在刷新</div>
            </div>--%>
            <div class="weui-panel weui-panel_access">
                
                 <div class="weui-panel__bd" id="project_list">
                    <!--内容展示区域-->
                </div>
               <%-- <div class="weui-msg__opr-area">
                    <p class="weui-btn-area">
                        <a href="JydWapindex.aspx" class="weui-btn weui-btn_primary">返回</a>
                    </p>
                </div>--%>
            </div>
            
          
        </div>

    </div>
    
    <script src="../scripts/jquery-1.10.2.min.js"></script>

    <script src="dist/js/jquery-weui.min.js"></script>
     
    <script src="dist/js/jquery-weui.js"></script>

    <script>
        var pageindex = 1;//页数
        var pagesize = 10;//每页条数
        var search = '';//搜索关键词
        var mystatus = 1;//工作单状态
        function Refresh() {
            pageindex = 1;
            $.ajax({
                url: '/JydModleOrder/ashx/ajax_submit.ashx',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'GetMyJobList',
                    pageindex: pageindex,
                    pagesize: pagesize,
                    search: search,
                    mystatus: mystatus
                },
                success: function (d) {
                    $("#project_list").html('');
                    if (d.code == 0) {
                        var str = "";
                        jQuery.each(d.data, function (i, o) {

                            str += '<div class="weui-form-preview" >';
                            str += '<div class="weui-form-preview__bd">';
                            str += '<p class="weui-media-box__desc"><div class="weui-form-preview__item"><label class="weui-form-preview__label">公司名称</label><span class="weui-form-preview__value">' + o.comname + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">联系人</label><span class="weui-form-preview__value">' + o.connman + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">订单号</label><span class="weui-form-preview__value">' + o.OrderID + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">联系人电话</label><span class="weui-form-preview__value">' + o.tel + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">订单总额</label><span class="weui-form-preview__value">' + o.jexx + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">货发</label><span class="weui-form-preview__value">' + o.huofa + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">运输</label><span class="weui-form-preview__value">' + o.ys + '</span></div>';
                            if (o.mystatus == 0) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">未走上流程</span></div>';

                            }
                            else if (o.mystatus == 1) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">正在生产中</span></div>';

                            }
                            else if (o.mystatus == 2) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">已送货签收</span></div>';

                            }
                            else if (o.mystatus == 3) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">全单收款</span></div>';

                            }
                            else {
                                 str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">(已作废)</span></div>';
                            }
                            str += '<div class="weui-form-preview__ft"><span class="weui-form-preview__btn weui-form-preview__btn_primary" onclick="caozuo(\'' + o.OrderID + '\')">催单</span></div>';
                            str += '</div>';
                            str += '</div>';
                        });
                        $("#project_list").append(str);
                        if (d.datanum < pagesize) {
                            var loadmore = '<div class="weui-loadmore weui-loadmore_line">' +
                                '<span class="weui-loadmore__tips">暂无更多数据</span>' +
                                '</div>';
                            $("#loadmore").html(loadmore);
                        } else {
                            var loadmore = '<div class="weui-loadmore weui-loadmore_line">' +
                                '<span class="weui-loadmore__tips" onclick="jzmore()">暂无更多数据</span>' +
                                '</div>';
                            $("#loadmore").html(loadmore);
                        }
                    }
                    
                    if (d.code == 1000) {
                        console.log("789");
                        var syu = "";
                        syu += '<i class="weui-icon-warn weui-icon_msg-primary" >暂无数据</i >';
                        console.log("369");
                    }
                },
                error: function (e) {
                }
            });
        }
        //监听搜索
        document.getElementById('search_from').onsubmit = function (e) {
            document.activeElement.blur();//软键盘收起
            search = $('#searchInput').val();
            Refresh();
            return false;
        }
        //搜索取消
        $('#searchCancel').click(function () {
            search = '';
        });

        function caozuo(id) {
    $.ajax({
        url: "/JydModleOrder/ashx/ajax_submit.ashx",
        type: "POST",
        datatype: "JSON",
        data: {
            action: 'caozuo',
            OrderID: id
        },
        success: function (e) {
            alert("催单成功");
        }
    });
}

    </script>


</body>
</html>
