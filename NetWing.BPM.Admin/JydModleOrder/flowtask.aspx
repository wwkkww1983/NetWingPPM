﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="flowtask.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.flowtask" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.3/style/weui.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-weui/1.2.1/css/jquery-weui.min.css">
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="https://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery-weui/1.2.1/js/jquery-weui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/scripts/gooflow1.3/codebase/GooFlow.css" />
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFunc.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/plugin/json2.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFlow.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFlow.color.js"></script>
    <script src="../scripts/layui/layui.all.js"></script>
    <title>任务待办</title>
</head>
<body>
    <script>
        $(function () {// 初始化内容
            loadD();

        });
        function loadD() {
            //每次加载都清空这几个值
            $("#VerificationOpinion").val("");//清空备注
            $("#FlowInstanceId").val("");//清空流程实例id

            //待办流程
            $.showLoading();
            $("#tab1content").empty();
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx?action=flow',
                type: 'POST', //GET
                async: true,  //或false,是否异步 同步则执行完才回执行后面的东西
                data: {
                    type: 'wait',
                    page: 1,
                    rows: 100
                },
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    $("#dcl").html("("+d.total+")");
                    $.each(d.rows, function (i, v) {
                        var c = '<a onclick="viewflow(' + v.KeyId + ');" class="weui-cell weui-cell_access" href="javascript:;">'
                            + '<div class="weui-cell__bd">'
                            + '<p>' + v.CustomName + '</p>'
                            + '</div>'
                            + '<div class="weui-cell__ft">'
                            + '</div>'
                            + '</a>';
                        $("#tab1content").append(c);

                    });


                    //console.log(d);
                    //console.log(textStatus);
                    //console.log(jqXHR);
                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            //已处理流程
            //已完成
            $("#tab3content").empty();
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx?action=flow',
                type: 'POST', //GET
                async: true,  //或false,是否异步 同步则执行完才回执行后面的东西
                data: {
                    type: 'finish',
                    page: 1,
                    rows: 100
                },
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    $("#ywc").html("("+d.total+")");
                    $.each(d.rows, function (i, v) {
                        var c = '<a  onclick="viewflow(' + v.KeyId + ');" class="weui-cell weui-cell_access" href="javascript:;">'
                            + '<div class="weui-cell__bd">'
                            + '<p>' + v.CustomName + '</p>'
                            + '</div>'
                            + '<div class="weui-cell__ft">'
                            + '</div>'
                            + '</a>';
                        $("#tab3content").append(c);

                    });


                    //console.log(d);
                    //console.log(textStatus);
                    //console.log(jqXHR);
                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            $.hideLoading();
        }


        //初始化设计流程器
        $.fn.flowdesign = function (options) {
            var $frmpreview = $(this);
            if (!$frmpreview.attr('id')) {
                return false;
            }
            $frmpreview.html("");

            var defaultcnf = {
                width: 500,
                height: 400,

                haveHead: false,
                haveTool: true,
                headLabel: true,
                toolBtns: ["start round mix", "end round", "node", "join", "fork"],
                haveGroup: true,
                useOperStack: true
            };
            if (options != undefined) {
                $.extend(defaultcnf, options);
            }

            var flowPanel = $.createGooFlow($(this), defaultcnf);
            flowPanel.setNodeRemarks({
                cursor: "选择指针",
                direct: "转换连线",
                dashed: "关联虚线",
                start: "开始结点",
                end: "结束结点",
                task: "任务结点",
                node: "任务结点",
                chat: "决策结点",
                state: "状态结点",
                plug: "附加插件",
                fork: "分支结点",
                join: "联合结点",
                complex: "复合结点",
                group: "组织划分框编辑开"
            });
            if (options != undefined
                && options.flowcontent != undefined
                && options.flowcontent != null) {  //加载内容

                //flowPanel.loadData(options.flowcontent);
                flowPanel.loadDataNew(options.flowcontent); //注意已经用了新方法loadDataNew

            }

            //导出数据扩展方法
            //所有节点必须有进出线段
            //必须有开始结束节点（且只能为一个）
            //分流合流节点必须成对出现
            //分流合流节点必须一一对应且中间必须有且只能有一个普通节点
            //分流节点与合流节点之前的审核节点必须有且只能有一条出去和进来节点
            flowPanel.exportDataEx = function () {
                var data = flowPanel.exportData();
                var fromlines = {},
                    tolines = {},
                    nodes = {},
                    fnodes = [],   //会签分流节点
                    hnodes = [],  //会签合流节点
                    startroundFlag = 0,   //开始节点标识
                    endroundFlag = 0;   //结束节点标识
                for (var i in data.lines) {
                    if (fromlines[data.lines[i].from] == undefined) {
                        fromlines[data.lines[i].from] = [];
                    }
                    fromlines[data.lines[i].from].push(data.lines[i].to);

                    if (tolines[data.lines[i].to] == undefined) {
                        tolines[data.lines[i].to] = [];
                    }
                    tolines[data.lines[i].to].push(data.lines[i].from);
                }
                for (var j in data.nodes) {
                    var _node = data.nodes[j];
                    var _flag = false;
                    switch (_node.type) {
                        case "start round mix":
                            startroundFlag++;
                            if (fromlines[_node.id] == undefined) {
                                layer.msg("开始节点无法流转到下一个节点");
                                return -1;
                            }
                            break;
                        case "end round":
                            endroundFlag++;
                            if (tolines[_node.id] == undefined) {
                                layer.msg("无法流转到结束节点");
                                return -1;
                            }
                            break;
                        case "node":
                            if (_node.setInfo == null) {
                                layer.msg("请设置节点【" + _node.name + "】操作人员");
                                return -1;
                            }
                            _flag = true;
                            break;
                        case "fork":
                            _flag = true;
                            fnodes.push(_node.id);
                            break;
                        case "join":
                            hnodes.push(_node.id);
                            _flag = true;
                            break;
                        default:
                            layer.msg("节点数据异常！");
                            return -1;
                            break;
                    }
                    nodes[_node.id] = _node;
                }
                if (startroundFlag == 0) {
                    layer.msg("必须有开始节点");
                    return -1;
                }

                if (endroundFlag == 0) {
                    layer.msg("必须有结束节点");
                    return -1;
                }

                if (fnodes.length != hnodes.length) {
                    layer.msg("分流节点必须等于合流节点");
                    return -1;
                }
                return data;
            }

            flowPanel.SetNodeEx = function (id, data) {
                flowPanel.setName(id, data.NodeName, "node", data);
            }
            flowPanel.SetLineEx = function (id, data) {
                flowPanel.setName(id, data.LineName, "line", data);
            }
            flowPanel.onItemDbClick = function (id, type) {
                var obj = flowPanel.getItemInfo(id, type);
                switch (type) {
                    case "node":
                        options.OpenNode(obj);
                        break;
                    case "line":
                        options.OpenLine(obj);
                        break;
                }
                return false;
            }
            if (options.isprocessing) //如果是显示进程状态
            {
                var tipHtml =
                    '<div style="position:absolute;left:10px;margin-top: 10px;padding:10px;border-radius:5px;background:rgba(0,0,0,0.05);z-index:1000;display:inline-block;">';
                tipHtml +=
                    '<div style="display: inline-block;"><i style="padding-right:5px;color:#5cb85c;" class="layui-icon">&#xe612;</i><span>已处理</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#5bc0de;" class="layui-icon">&#xe612;</i><span>正在处理</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#d9534f;" class="layui-icon">&#xe612;</i><span>不通过</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#f0ad4e;" class="layui-icon">&#xe612;</i><span>驳回</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#999;" class="layui-icon">&#xe612;</i><span>未处理</span></div></div>';

                $('.GooFlow_work .GooFlow_work_inner').css('background-image', 'none');
                $('td').css('color', '#fff');
                $frmpreview.css('background', '#fff');
                $('.ico').remove();
                $('.GooFlow_item').css('border', '0px');
                $frmpreview.append(tipHtml);
                $.each(options.nodeData,
                    function (i, item) {
                        $("#" + item.id).css("background", "#999");
                        if (item.type == "start round mix") {
                            $("#" + item.id).css("background", "#5cb85c");
                        } else {
                            if (item.id == options.activityId) {
                                $("#" + item.id).css("background", "#5bc0de"); //正在处理
                            }
                            if (item.setInfo != undefined && item.setInfo.Taged != undefined) {
                                if (item.setInfo.Taged == -1) {
                                    $("#" + item.id).css("background", "#d9534f"); //不通过
                                } else if (item.setInfo.Taged == 1) {
                                    $("#" + item.id).css("background", "#5cb85c"); //通过
                                } else {
                                    $("#" + item.id).css("background", "#f0ad4e"); //驳回
                                }
                            }
                        }
                        if (item.setInfo != undefined && item.setInfo.Taged != undefined) {
                            var tips = '<div style="text-align:left">';
                            var tagname = { "-1": "不通过", "1": "通过", "0": "驳回" };
                            tips += "<p>处理人：" + item.setInfo.UserName + "</p>";
                            tips += "<p>结果：" + tagname[item.setInfo.Taged] + "</p>";
                            tips += "<p>处理时间：" + item.setInfo.TagedTime + "</p>";
                            tips += "<p>备注：" + item.setInfo.Description + "</p></div>";

                            $('#' + item.id).click(function () {
                                layer.tips(tips, '#' + item.id);
                            });
                        } else {
                            $('#' + item.id).click(function () {
                                layer.tips('暂无处理信息', '#' + item.id);
                            });
                        }
                    });
            }
            if (options.preview == 1) {
                preview();
            }

            //预览
            function preview() {
                var _frmitems = {};
                for (var i in options.frmData) {
                    var _frmitem = options.frmData[i];
                    _frmitems[_frmitem.control_field] = _frmitem.control_label;
                }
                var DataBaseLinkData = {};


                var _NodeRejectType = { "0": "前一步", "1": "第一步", "2": "某一步", "3": "用户指定", "4": "不处理" };
                var _NodeIsOver = { "0": "不允许", "1": "允许" };
                var _NodeDesignate = {
                    "NodeDesignateType1": "所有成员",
                    "NodeDesignateType2": "指定成员",
                    "NodeDesignateType3": "发起者领导",
                    "NodeDesignateType4": "前一步骤领导",
                    "NodeDesignateType5": "发起者部门领导",
                    "NodeDesignateType6": "发起者公司领导"
                };
                var _NodeConfluenceType = { "0": "所有步骤通过", "1": "一个步骤通过即可", "2": "按百分比计算" };
                if (options.flowcontent == undefined) return;
                $.each(options.flowcontent.nodes,
                    function (i, item) {
                        if (item.setInfo != undefined) {
                            var _popoverhtml = "";
                            _popoverhtml +=
                                '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;基本信息</div>';
                            _popoverhtml += '<ul>';
                            _popoverhtml += '<li>节点标识:' + item.setInfo.NodeCode + '</li>';
                            _popoverhtml += '<li>驳回类型:' + _NodeRejectType[item.setInfo.NodeRejectType] + '</li>';
                            _popoverhtml += '<li>终止流程:' + _NodeIsOver[item.setInfo.NodeIsOver] + '</li>';
                            if (item.setInfo.Description != "") {
                                _popoverhtml += '<li>备注:' + item.setInfo.Description + '</li>';
                            }
                            if (item.setInfo.NodeConfluenceType != "") {
                                _popoverhtml += '<li>会签策略:' +
                                    _NodeConfluenceType[item.setInfo.NodeConfluenceType] +
                                    '</li>';
                                if (item.setInfo.NodeConfluenceType == 2) {
                                    _popoverhtml += '<li>会签比例:' + item.setInfo.NodeConfluenceRate + '</li>';
                                }
                            }
                            if (item.setInfo.NodeDataBase != "") {
                                _popoverhtml += '<li>绑定数据库:' + DataBaseLinkData[item.setInfo.NodeDataBase] + '</li>';
                            }
                            if (item.setInfo.NodeTable != "") {
                                _popoverhtml += '<li>绑定表名:' + item.setInfo.NodeTable + '</li>';
                            }
                            if (item.setInfo.NodePram != "") {
                                _popoverhtml += '<li>绑定字段:' + item.setInfo.NodePram + '</li>';
                            }
                            _popoverhtml += '</ul>';

                            _popoverhtml +=
                                '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;审核者</div>';
                            _popoverhtml += '<ul>';
                            _popoverhtml += '<li>类型:' + _NodeDesignate[item.setInfo.NodeDesignate] + '</li>';
                            if (item.setInfo.NodeDesignateData != undefined) {
                                var _rowstr = "";
                                for (var i in item.setInfo.NodeDesignateData.roles) {
                                    var _postitem = item.setInfo.NodeDesignateData.roles[i];
                                    var _one = top.clientroleData[_postitem];
                                    _rowstr += ' <span class="label label-success">' +
                                        (_one == undefined ? _postitem : _one.FullName) +
                                        '</span>';
                                    if (i == item.setInfo.NodeDesignateData.roles.length - 1) {
                                        _popoverhtml += '<li>角色:' + _rowstr + '</li>';
                                    }
                                }

                                _rowstr = "";
                                for (var i in item.setInfo.NodeDesignateData.users) {
                                    var _postitem = item.setInfo.NodeDesignateData.users[i];
                                    var _one = clientuserData[_postitem];
                                    _rowstr += ' <span class="label label-danger">' +
                                        (_one == undefined ? _postitem : _one.RealName) +
                                        '</span>';
                                    if (i == item.setInfo.NodeDesignateData.users.length - 1) {
                                        _popoverhtml += '<li>用户:' + _rowstr + '</li>';
                                    }
                                }
                            }
                            _popoverhtml += '</ul>';

                            var _row = "";
                            for (var i in item.setInfo.frmPermissionInfo) {
                                var _item = item.setInfo.frmPermissionInfo[i];
                                var _downtext = "";
                                if (_item.down) {
                                    _downtext = ' | 可下载';
                                } else if (_item.down != undefined) {
                                    _downtext = ' | 不可下载';
                                }
                                _row += '<li>' +
                                    _frmitems[_item.fieldid] +
                                    ': ' +
                                    (_item.look ? '可查看' : '不可查看') +
                                    _downtext +
                                    '</li>';
                                if (i == item.setInfo.frmPermissionInfo.length - 1) {
                                    _popoverhtml +=
                                        '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;权限分配</div>';
                                    _popoverhtml += '<ul>';
                                    _popoverhtml += _row;
                                    _popoverhtml += '</ul>';
                                }
                            }

                            if (item.setInfo.NodeDataBaseToSQL != "" || item.setInfo.NodeSQL != "") {
                                _popoverhtml +=
                                    '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;执行SQL</div>';
                                _popoverhtml += '<ul>';
                                _popoverhtml += '<li>数据库:' + DataBaseLinkData[item.setInfo.NodeDataBaseToSQL] + '</li>';
                                _popoverhtml += '<li>SQL语句:' + item.setInfo.NodeSQL + '</li>';
                                _popoverhtml += '</ul>';
                            }

                            $('#' + item.id).attr('title', item.name);
                            $('#' + item.id).attr('data-toggle', 'popover');
                            $('#' + item.id).attr('data-placement', 'bottom');
                            $('#' + item.id).attr('data-content', _popoverhtml);
                        } else {
                            $('#' + item.id).attr('title', item.name);
                            $('#' + item.id).attr('data-toggle', 'popover');
                            $('#' + item.id).attr('data-placement', 'bottom');
                            $('#' + item.id).attr('data-content', "该节点未被设置");
                        }
                    });
                //$('.GooFlow_item').popover({ html: true });
            }

            return flowPanel;
        }
        //渲染工作流程
        function loadflow(row) {
            var c = JSON.parse(row.SchemeContent);
            //console.log("activityId是:" + row.ActivityId);
            //console.log("nodeData:" + JSON.stringify(c.nodes));
            //console.log("flowcontent:" + JSON.stringify(c));

            //$("#FlowInstanceId").val(row.KeyId);//给隐藏的工作流实例ID赋值

            flowDesignPanel = $('#flowcontent').flowdesign({
                haveTool: false
                , isprocessing: true
                , activityId: row.ActivityId
                , nodeData: c.nodes
                , flowcontent: c
                , width: 800
                , height: 600
            });

        }



        function viewflow(keyid) {
            $("#viewflow").popup();
            $.showLoading();
            console.log("keyid:" + keyid);
            var query = '{"groupOp":"AND","rules":[],"groups":[]}';//先给一个搜索的空的模板
            var o = JSON.parse(query);//把文本转换为json obj
            o.rules[0] = JSON.parse('{"field":"KeyId","op":"eq","data":"' + keyid + '"}');//cn 就是包含  eq就是等于
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx?filter=' + JSON.stringify(o) + '',
                type: 'POST', //GET
                async: false,  //或false,是否异步 同步则执行完才回执行后面的东西
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    loadflow(d.rows[0]);//这个传两个参数
                    $.hideLoading();
                    $("#FlowInstanceId").val(d.rows[0].KeyId);
                    $("#IsFinish").val(d.rows[0].IsFinish);//是否结束
                    $("#content").html("流程:" + d.rows[0].CustomName + "<br/>当前需要处理:" + d.rows[0].ActivityName);


                    //console.log(d);
                    //console.log(textStatus);
                    //console.log(jqXHR);
                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            $("#full" + keyid).popup();
        }

        function submitflow() {
            if ($("#VerificationOpinion").val() === "") {
                $.toast("请填写备注", "forbidden");
                return false;
            }
            if ($("#IsFinish").val() === "1") {
                $.toast("流程已结束", "forbidden");
                return false;
            }


            $.showLoading();
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    VerificationOpinion: $("#VerificationOpinion").val(),
                    FlowInstanceId: $("#FlowInstanceId").val(),
                    VerificationFinally: $("#VerificationFinally").val(),
                    action: "doflow"
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (d, textStatus, jqXHR) {
                    $.hideLoading();
                    if (parseInt(d) > 0) {
                        $.toast("操作成功");
                        $.closePopup();
                    } else {
                        $.toast("操作失败", "forbidden");
                        $.closePopup();
                    }
                    loadD();//完了重新加载数据
                },

            });
        }


    </script>
    <div class="weui-tab">
        <div class="weui-navbar">
            <a onclick="loadD()" class="weui-navbar__item weui-bar__item--on" href="#tab1">待处理<span id="dcl"></span>
            </a>

            <a onclick="loadD()" class="weui-navbar__item" href="#tab3">已完成<span id="ywc"></span>
            </a>
        </div>
        <div class="weui-tab__bd">
            <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
                <div class="weui-cells" id="tab1content">
                </div>
            </div>
            <div id="tab3" class="weui-tab__bd-item">
                <div class="weui-cells" id="tab3content">
                </div>
            </div>
        </div>
    </div>
    <div id="viewflow" class='weui-popup__container' style="z-index: 999;">
        <div class="weui-popup__overlay"></div>
        <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">
            <input type="hidden" id="FlowInstanceId" name="FlowInstanceId" />
            <input type="hidden" id="IsFinish" name="IsFinish" />
            <article class="weui-article">

                <section>
                    <h3 id="content"></h3>

                </section>
            </article>

            <div id="flowcontent"></div>
            <div class="weui-cells__title">操作</div>

            <div class="weui-cells">
                <div class="weui-cell weui-cell_select">
                    <div class="weui-cell__bd">
                        <select class="weui-select" id="VerificationFinally" name="VerificationFinally">
                            <option selected="" value="1">处理</option>
                            <option value="2">不处理</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="weui-cells__title">备注</div>
            <div class="weui-cells">
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <input id="VerificationOpinion" class="weui-input " type="text" placeholder="请输入备注">
                    </div>
                </div>
            </div>
            <center>
                <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_primary close-popup">关闭</a>
                <a onclick="submitflow()" class="weui-btn weui-btn_mini weui-btn_warn">流程处理</a>
                    </center>
        </div>

    </div>
</body>
</html>
