﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QRCodeView.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.QRCodeView" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <title><%=dr["comname"].ToString() %>-<%=dr["allyspmc"].ToString() %></title>

    <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.3/style/weui.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-weui/1.2.1/css/jquery-weui.min.css">
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="https://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery-weui/1.2.1/js/jquery-weui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/scripts/gooflow1.3/codebase/GooFlow.css" />
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFunc.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/plugin/json2.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFlow.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFlow.color.js"></script>
    <script>

        function showloading(t) {
            if (t) {//如果是true则显示loading
                console.log(t);
                loading = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });
            } else {//如果是false则关闭loading
                console.log("关闭loading层:" + t);
                layer.closeAll('loading');
            }
        }


        function ido(s) {//我做
            var statu = confirm("点击确定之后就要开始加工了哦!!");
            if (!statu) {
                return false;
            }
            console.log("我做" + s);
            $.ajax({
                url: '/jyddo/ashx/JydDoHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    action: 'add',
                    s: s
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function () {
                    showloading(true);
                },

                success: function (d, textStatus, jqXHR) {
                    showloading(false);
                    if (d >= 1) {
                        window.location.reload();
                    }
                }

            });

        }



        function cbr(s) {//别人做
            console.log("催别人做" + s);

            $.ajax({
                url: '/jyddo/ashx/JydDoHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    action: 'cbrz',
                    s: s
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function () {
                    // Handle the beforeSend event
                    //loading层
                    showloading(true);
                },
                success: function (data, textStatus, jqXHR) {
                    showloading(false);
                    layer.msg(data.msg);
                    if (data.status === 1) {
                        layer.msg(data.msg);
                        window.location.reload();
                    }
                }

            });

        }

        //初始化设计流程器
        $.fn.flowdesign = function (options) {
            var $frmpreview = $(this);
            if (!$frmpreview.attr('id')) {
                return false;
            }
            $frmpreview.html("");

            var defaultcnf = {
                width: 500,
                height: 400,

                haveHead: false,
                haveTool: true,
                headLabel: true,
                toolBtns: ["start round mix", "end round", "node", "join", "fork"],
                haveGroup: true,
                useOperStack: true
            };
            if (options != undefined) {
                $.extend(defaultcnf, options);
            }

            var flowPanel = $.createGooFlow($(this), defaultcnf);
            flowPanel.setNodeRemarks({
                cursor: "选择指针",
                direct: "转换连线",
                dashed: "关联虚线",
                start: "开始结点",
                end: "结束结点",
                task: "任务结点",
                node: "任务结点",
                chat: "决策结点",
                state: "状态结点",
                plug: "附加插件",
                fork: "分支结点",
                join: "联合结点",
                complex: "复合结点",
                group: "组织划分框编辑开"
            });
            if (options != undefined
                && options.flowcontent != undefined
                && options.flowcontent != null) {  //加载内容

                //flowPanel.loadData(options.flowcontent);
                flowPanel.loadDataNew(options.flowcontent); //注意已经用了新方法loadDataNew

            }

            //导出数据扩展方法
            //所有节点必须有进出线段
            //必须有开始结束节点（且只能为一个）
            //分流合流节点必须成对出现
            //分流合流节点必须一一对应且中间必须有且只能有一个普通节点
            //分流节点与合流节点之前的审核节点必须有且只能有一条出去和进来节点
            flowPanel.exportDataEx = function () {
                var data = flowPanel.exportData();
                var fromlines = {},
                    tolines = {},
                    nodes = {},
                    fnodes = [],   //会签分流节点
                    hnodes = [],  //会签合流节点
                    startroundFlag = 0,   //开始节点标识
                    endroundFlag = 0;   //结束节点标识
                for (var i in data.lines) {
                    if (fromlines[data.lines[i].from] == undefined) {
                        fromlines[data.lines[i].from] = [];
                    }
                    fromlines[data.lines[i].from].push(data.lines[i].to);

                    if (tolines[data.lines[i].to] == undefined) {
                        tolines[data.lines[i].to] = [];
                    }
                    tolines[data.lines[i].to].push(data.lines[i].from);
                }
                for (var j in data.nodes) {
                    var _node = data.nodes[j];
                    var _flag = false;
                    switch (_node.type) {
                        case "start round mix":
                            startroundFlag++;
                            if (fromlines[_node.id] == undefined) {
                                layer.msg("开始节点无法流转到下一个节点");
                                return -1;
                            }
                            break;
                        case "end round":
                            endroundFlag++;
                            if (tolines[_node.id] == undefined) {
                                layer.msg("无法流转到结束节点");
                                return -1;
                            }
                            break;
                        case "node":
                            if (_node.setInfo == null) {
                                layer.msg("请设置节点【" + _node.name + "】操作人员");
                                return -1;
                            }
                            _flag = true;
                            break;
                        case "fork":
                            _flag = true;
                            fnodes.push(_node.id);
                            break;
                        case "join":
                            hnodes.push(_node.id);
                            _flag = true;
                            break;
                        default:
                            layer.msg("节点数据异常！");
                            return -1;
                            break;
                    }
                    nodes[_node.id] = _node;
                }
                if (startroundFlag == 0) {
                    layer.msg("必须有开始节点");
                    return -1;
                }

                if (endroundFlag == 0) {
                    layer.msg("必须有结束节点");
                    return -1;
                }

                if (fnodes.length != hnodes.length) {
                    layer.msg("分流节点必须等于合流节点");
                    return -1;
                }
                return data;
            }

            flowPanel.SetNodeEx = function (id, data) {
                flowPanel.setName(id, data.NodeName, "node", data);
            }
            flowPanel.SetLineEx = function (id, data) {
                flowPanel.setName(id, data.LineName, "line", data);
            }
            flowPanel.onItemDbClick = function (id, type) {
                var obj = flowPanel.getItemInfo(id, type);
                switch (type) {
                    case "node":
                        options.OpenNode(obj);
                        break;
                    case "line":
                        options.OpenLine(obj);
                        break;
                }
                return false;
            }
            if (options.isprocessing) //如果是显示进程状态
            {
                var tipHtml =
                    '<div style="position:absolute;left:10px;margin-top: 10px;padding:10px;border-radius:5px;background:rgba(0,0,0,0.05);z-index:1000;display:inline-block;">';
                tipHtml +=
                    '<div style="display: inline-block;"><i style="padding-right:5px;color:#5cb85c;" class="layui-icon">&#xe612;</i><span>已处理</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#5bc0de;" class="layui-icon">&#xe612;</i><span>正在处理</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#d9534f;" class="layui-icon">&#xe612;</i><span>不通过</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#f0ad4e;" class="layui-icon">&#xe612;</i><span>驳回</span></div>';
                tipHtml +=
                    '<div style="display: inline-block;margin-left: 10px;"><i style="padding-right:5px;color:#999;" class="layui-icon">&#xe612;</i><span>未处理</span></div></div>';

                $('.GooFlow_work .GooFlow_work_inner').css('background-image', 'none');
                $('td').css('color', '#fff');
                $frmpreview.css('background', '#fff');
                $('.ico').remove();
                $('.GooFlow_item').css('border', '0px');
                $frmpreview.append(tipHtml);
                $.each(options.nodeData,
                    function (i, item) {
                        $("#" + item.id).css("background", "#999");
                        if (item.type == "start round mix") {
                            $("#" + item.id).css("background", "#5cb85c");
                        } else {
                            if (item.id == options.activityId) {
                                $("#" + item.id).css("background", "#5bc0de"); //正在处理
                            }
                            if (item.setInfo != undefined && item.setInfo.Taged != undefined) {
                                if (item.setInfo.Taged == -1) {
                                    $("#" + item.id).css("background", "#d9534f"); //不通过
                                } else if (item.setInfo.Taged == 1) {
                                    $("#" + item.id).css("background", "#5cb85c"); //通过
                                } else {
                                    $("#" + item.id).css("background", "#f0ad4e"); //驳回
                                }
                            }
                        }
                        if (item.setInfo != undefined && item.setInfo.Taged != undefined) {
                            var tips = '<div style="text-align:left">';
                            var tagname = { "-1": "不通过", "1": "通过", "0": "驳回" };
                            tips += "<p>处理人：" + item.setInfo.UserName + "</p>";
                            tips += "<p>结果：" + tagname[item.setInfo.Taged] + "</p>";
                            tips += "<p>处理时间：" + item.setInfo.TagedTime + "</p>";
                            tips += "<p>备注：" + item.setInfo.Description + "</p></div>";

                            $('#' + item.id).click(function () {
                                layer.tips(tips, '#' + item.id);
                            });
                        } else {
                            $('#' + item.id).click(function () {
                                layer.tips('暂无处理信息', '#' + item.id);
                            });
                        }
                    });
            }
            if (options.preview == 1) {
                preview();
            }

            //预览
            function preview() {
                var _frmitems = {};
                for (var i in options.frmData) {
                    var _frmitem = options.frmData[i];
                    _frmitems[_frmitem.control_field] = _frmitem.control_label;
                }
                var DataBaseLinkData = {};


                var _NodeRejectType = { "0": "前一步", "1": "第一步", "2": "某一步", "3": "用户指定", "4": "不处理" };
                var _NodeIsOver = { "0": "不允许", "1": "允许" };
                var _NodeDesignate = {
                    "NodeDesignateType1": "所有成员",
                    "NodeDesignateType2": "指定成员",
                    "NodeDesignateType3": "发起者领导",
                    "NodeDesignateType4": "前一步骤领导",
                    "NodeDesignateType5": "发起者部门领导",
                    "NodeDesignateType6": "发起者公司领导"
                };
                var _NodeConfluenceType = { "0": "所有步骤通过", "1": "一个步骤通过即可", "2": "按百分比计算" };
                if (options.flowcontent == undefined) return;
                $.each(options.flowcontent.nodes,
                    function (i, item) {
                        if (item.setInfo != undefined) {
                            var _popoverhtml = "";
                            _popoverhtml +=
                                '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;基本信息</div>';
                            _popoverhtml += '<ul>';
                            _popoverhtml += '<li>节点标识:' + item.setInfo.NodeCode + '</li>';
                            _popoverhtml += '<li>驳回类型:' + _NodeRejectType[item.setInfo.NodeRejectType] + '</li>';
                            _popoverhtml += '<li>终止流程:' + _NodeIsOver[item.setInfo.NodeIsOver] + '</li>';
                            if (item.setInfo.Description != "") {
                                _popoverhtml += '<li>备注:' + item.setInfo.Description + '</li>';
                            }
                            if (item.setInfo.NodeConfluenceType != "") {
                                _popoverhtml += '<li>会签策略:' +
                                    _NodeConfluenceType[item.setInfo.NodeConfluenceType] +
                                    '</li>';
                                if (item.setInfo.NodeConfluenceType == 2) {
                                    _popoverhtml += '<li>会签比例:' + item.setInfo.NodeConfluenceRate + '</li>';
                                }
                            }
                            if (item.setInfo.NodeDataBase != "") {
                                _popoverhtml += '<li>绑定数据库:' + DataBaseLinkData[item.setInfo.NodeDataBase] + '</li>';
                            }
                            if (item.setInfo.NodeTable != "") {
                                _popoverhtml += '<li>绑定表名:' + item.setInfo.NodeTable + '</li>';
                            }
                            if (item.setInfo.NodePram != "") {
                                _popoverhtml += '<li>绑定字段:' + item.setInfo.NodePram + '</li>';
                            }
                            _popoverhtml += '</ul>';

                            _popoverhtml +=
                                '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;审核者</div>';
                            _popoverhtml += '<ul>';
                            _popoverhtml += '<li>类型:' + _NodeDesignate[item.setInfo.NodeDesignate] + '</li>';
                            if (item.setInfo.NodeDesignateData != undefined) {
                                var _rowstr = "";
                                for (var i in item.setInfo.NodeDesignateData.roles) {
                                    var _postitem = item.setInfo.NodeDesignateData.roles[i];
                                    var _one = top.clientroleData[_postitem];
                                    _rowstr += ' <span class="label label-success">' +
                                        (_one == undefined ? _postitem : _one.FullName) +
                                        '</span>';
                                    if (i == item.setInfo.NodeDesignateData.roles.length - 1) {
                                        _popoverhtml += '<li>角色:' + _rowstr + '</li>';
                                    }
                                }

                                _rowstr = "";
                                for (var i in item.setInfo.NodeDesignateData.users) {
                                    var _postitem = item.setInfo.NodeDesignateData.users[i];
                                    var _one = clientuserData[_postitem];
                                    _rowstr += ' <span class="label label-danger">' +
                                        (_one == undefined ? _postitem : _one.RealName) +
                                        '</span>';
                                    if (i == item.setInfo.NodeDesignateData.users.length - 1) {
                                        _popoverhtml += '<li>用户:' + _rowstr + '</li>';
                                    }
                                }
                            }
                            _popoverhtml += '</ul>';

                            var _row = "";
                            for (var i in item.setInfo.frmPermissionInfo) {
                                var _item = item.setInfo.frmPermissionInfo[i];
                                var _downtext = "";
                                if (_item.down) {
                                    _downtext = ' | 可下载';
                                } else if (_item.down != undefined) {
                                    _downtext = ' | 不可下载';
                                }
                                _row += '<li>' +
                                    _frmitems[_item.fieldid] +
                                    ': ' +
                                    (_item.look ? '可查看' : '不可查看') +
                                    _downtext +
                                    '</li>';
                                if (i == item.setInfo.frmPermissionInfo.length - 1) {
                                    _popoverhtml +=
                                        '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;权限分配</div>';
                                    _popoverhtml += '<ul>';
                                    _popoverhtml += _row;
                                    _popoverhtml += '</ul>';
                                }
                            }

                            if (item.setInfo.NodeDataBaseToSQL != "" || item.setInfo.NodeSQL != "") {
                                _popoverhtml +=
                                    '<div class="flow-portal-panel-title"><i class="fa fa-navicon"></i>&nbsp;&nbsp;执行SQL</div>';
                                _popoverhtml += '<ul>';
                                _popoverhtml += '<li>数据库:' + DataBaseLinkData[item.setInfo.NodeDataBaseToSQL] + '</li>';
                                _popoverhtml += '<li>SQL语句:' + item.setInfo.NodeSQL + '</li>';
                                _popoverhtml += '</ul>';
                            }

                            $('#' + item.id).attr('title', item.name);
                            $('#' + item.id).attr('data-toggle', 'popover');
                            $('#' + item.id).attr('data-placement', 'bottom');
                            $('#' + item.id).attr('data-content', _popoverhtml);
                        } else {
                            $('#' + item.id).attr('title', item.name);
                            $('#' + item.id).attr('data-toggle', 'popover');
                            $('#' + item.id).attr('data-placement', 'bottom');
                            $('#' + item.id).attr('data-content', "该节点未被设置");
                        }
                    });
                //$('.GooFlow_item').popover({ html: true });
            }

            return flowPanel;
        }
        //渲染工作流程
        function loadflow(row, did) {
            var c = JSON.parse(row.SchemeContent);
            //console.log("activityId是:" + row.ActivityId);
            //console.log("nodeData:" + JSON.stringify(c.nodes));
            //console.log("flowcontent:" + JSON.stringify(c));

            //$("#FlowInstanceId").val(row.KeyId);//给隐藏的工作流实例ID赋值

            flowDesignPanel = $('#flowPanel' + did).flowdesign({
                haveTool: false
                , isprocessing: true
                , activityId: row.ActivityId
                , nodeData: c.nodes
                , flowcontent: c
                , width: 800
                , height: 600
            });

        }

        //查看流程
        function openflow(keyid) {
            $.showLoading();
            console.log("keyid:" + keyid);
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx?action=mobileViewFlow&keyid=' + keyid + '',
                type: 'POST', //GET
                async: false,  //或false,是否异步 同步则执行完才回执行后面的东西
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    loadflow(d, keyid);//这个传两个参数
                    $.hideLoading();

                    //console.log(d);
                    //console.log(textStatus);
                    //console.log(jqXHR);
                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            $("#full" + keyid).popup();
        }
        function viewdet(keyid) {
            $.showLoading();
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx?action=mobileViewFlowDit&keyid=' + keyid + '',
                type: 'POST', //GET
                async: false,  //或false,是否异步 同步则执行完才回执行后面的东西
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    //flowdet
                    $.hideLoading();
                    $("#flowdet" + keyid).empty();
                    $.each(d, function (i, v) {
                        var temp = "已处理";
                        if (v.VerificationFinally === "2") {
                            temp = "拒绝";
                        } else if (v.VerificationFinally === "3") {
                            temp = "驳回";
                        }
                        console.log(i);

                        $("#flowdet" + keyid).append(i + "." + v.CreateUserName + "," + v.CreateDate + "," + v.NodeName + "," + temp + "," + v.VerificationOpinion + "<br/>");

                    });

                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            $("#view" + keyid).popup();
        }
        function doflow(keyid) {//手机流程处理
            $.showLoading();
            $("#doflow" + keyid).find(".VerificationOpinion").val("");//打开时设置为空
            $("#doflow" + keyid).find(".FlowInstanceId").val("");//打开事设置为空
            $("#doflow" + keyid).find(".flowcontent").html("");
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx?action=mobileViewFlow&keyid=' + keyid + '',
                type: 'POST', //GET
                async: false,  //或false,是否异步 同步则执行完才回执行后面的东西
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    $.hideLoading();
                    if (d.ActivityType == 4) {//说明流程已结束
                        $.toast("流程已结束", "forbidden");
                        $.closePopup();
                    } else {
                        //flowdet
                        $("#doflow" + keyid).find(".FlowInstanceId").val(d.KeyId);
                        $("#doflow" + keyid).find(".flowcontent").html("流程:" + d.CustomName + "<br/>当前需要处理:" + d.ActivityName);
                        console.log(d.KeyId);
                        $("#doflow" + keyid).popup();
                        //处理文字


                    }

                    //flowdet
                    $("#doflow" + keyid).find(".FlowInstanceId").val(d.KeyId);
                    console.log(d.KeyId);


                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            //FlowInstanceId

        }

        function submitflow(keyid) {
            if ($("#doflow" + keyid).find(".VerificationOpinion").val() == "") {
                $.toast("请填写备注", "forbidden");
                return false;
            }

            $.showLoading();
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    VerificationOpinion: $("#doflow" + keyid).find(".VerificationOpinion").val(),
                    FlowInstanceId: $("#doflow" + keyid).find(".FlowInstanceId").val(),
                    VerificationFinally: $("#doflow" + keyid).find(".VerificationFinally").val(),
                    action: "doflow"
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (d, textStatus, jqXHR) {
                    console.log("123");
                    $.hideLoading();
                    if (parseInt(d) > 0) {
                        $.toast("操作成功");
                        $.closePopup();
                    } else {
                        $.toast("操作失败", "forbidden");
                        $.closePopup();
                    }
                },

            });
        }

    </script>
    <style>
        .weui-btn_mini {
            line-height: 2.8 !important;
            font-size: 14px !important;
        }

        .weui-form-preview__label {
            color: #000;
            width: 90px;
            margin-right: 0;
            padding-right: 1em;
            text-align: right;
            text-align-last: right;
            height: 44px;
            line-height: 44px;
        }

        .weui-form-preview__item {
            border-top: solid 1px #ececec;
        }

        .weui-form-preview__value {
            text-align: left;
            padding-left: 1em;
            border-left: solid 1px #ececec;
            /*padding: 3px 3px 3px 6px;*/
            height: 44px;
            line-height: 44px;
        }

        .weui-pull-to-refresh {
            margin-top: -80px;
            transition: transform .4s;
        }

            .weui-pull-to-refresh.refreshing {
                transform: translate3d(0, 80px, 0);
            }

        .weui-pull-to-refresh__layer {
            padding: 20px 0 0 0;
            height: 60px;
            line-height: 60px;
        }

        .weui-msg {
            width: 100%;
            float: left;
            margin: -50px 0 0 0;
        }

        .weui-cells__title {
            width: 100%;
            font-size: 16px;
            color: #000;
            text-align: center;
            padding: 0 !important;
            margin: 0 !important;
            line-height: 40px;
        }

        .weui-cell__bd {
            width: 90%;
            float: left;
            padding: 3% 5%;
            border: #f4f4f4 1px solid;
        }

            .weui-cell__bd p {
                width: 100%;
                float: left;
                line-height: 30px;
            }

                .weui-cell__bd p span {
                    width: 100%;
                    float: left;
                    font-size: 14px;
                    color: #999;
                    text-align: left;
                }

                .weui-cell__bd p em {
                    width: 100%;
                    float: left;
                    font-size: 16px;
                    color: #333;
                    text-decoration: none;
                    text-align: left;
                    font-style: normal;
                }

        .btn_box {
            width: 100%;
            float: right;
            font-size: 14px;
            color: #f00;
            line-height: 30px;
            text-align: left !important;
        }

        .weui-cell {
            width: 100%;
            float: left; /*height:120px;*/
        }

        .GooFlow_work_inner {
            width: 100% !important;
            margin: 60px 0 0 -20px !important;
            height: 450px !important;
        }

        .close-popup { /*margin:0 !important;*/ /*padding:0 !important;*/ /*border-radius:0 !important;*/
        }

        .weui-article {
            background: #fff !important;
        }

        .flowcontent {
            text-align: left !important;
        }

        .weui-cell {
            padding: 10px 0 10px 10px !important;
            width: 96% !important;
        }

        .weui-cell__bd {
            border: none !important;
            padding: 0 !important;
        }

            .weui-cell__bd input {
                height: 45px !important;
            }

        .weui-select {
            padding-left: 0 !important;
        }

        .weui-popup__modal {
            width: 96% !important;
            padding: 0 2% !important;
            margin: 0px 0 0 0 !important;
            text-align: left;
            font-size: 12px;
            line-height: 24px;
        }
    </style>
</head>
<body ontouchstart>
    <div class="weui-form-preview">
        <%foreach (DataRow de in dt.Rows)
            {%>
        <div class="weui-form-preview__bd" style="color: #000; padding: 0;">
            <div class="weui-form-preview__item" style="display: none;">
                <label class="weui-form-preview__label">订单编号<%=isyg %></label>
                <span class="weui-form-preview__value" id="KeyId"><%=de["KeyId"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">订单编号</label>
                <span class="weui-form-preview__value" id="OrderID"><%=de["OrderID"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">单位名称</label>
                <span class="weui-form-preview__value" id="comname"><%=de["comname"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">电话</label>
                <span class="weui-form-preview__value" id="tel"><%=de["tel"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件时间</label>
                <span class="weui-form-preview__value" id="adddate"><%=de["adddate"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">交货时间</label>
                <span class="weui-form-preview__value" id="deliveryDate"><%=de["deliveryDate"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">印刷品名称</label>
                <span class="weui-form-preview__value" id="allyspmc"><%=de["allyspmc"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">联系人</label>
                <span class="weui-form-preview__value" id="connman"><%=de["connman"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件员</label>
                <span class="weui-form-preview__value" id="jjy"><%=de["jjy"].ToString() %></span>
            </div>

        </div>
        <%} %>
    </div>
    <br />
    <div class="weui-msg">



        <%int i = 1; %>
        <%foreach (DataRow ddr in detailDt.Rows)
            {%>
        <div class="weui-cells__title"><%=i++ %>.<%=ddr["yspmc"].ToString() %></div>

        <div class="button-sp-area">
            <a onclick="openflow(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_primary">查看流程</a>
            <a onclick="viewdet(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_primary">流程明细</a>
            <a onclick="doflow(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_warn">流程处理</a>
        </div>
        <div id="full<%=ddr["keyid"].ToString() %>" class='weui-popup__container'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">

                <div id="flowPanel<%=ddr["keyid"].ToString() %>"></div>
                <a href="javascript:;" class="weui-btn weui-btn_primary close-popup">关闭</a>
            </div>

        </div>
        <div id="view<%=ddr["keyid"].ToString() %>" class='weui-popup__container'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">

                <div id="flowdet<%=ddr["keyid"].ToString() %>"></div>
                <a href="javascript:;" class="weui-btn weui-btn_primary close-popup">关闭</a>
            </div>

        </div>
        <div id="doflow<%=ddr["keyid"].ToString() %>" class='weui-popup__container'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">
                <input type="hidden" class="FlowInstanceId" name="FlowInstanceId" />
                <article class="weui-article">

                    <section>
                        <h3 class="flowcontent"></h3>
                    </section>
                </article>
                <div class="weui-cells__title">结果</div>

                <div class="weui-cells">
                    <div class="weui-cell weui-cell_select">
                        <div class="weui-cell__bd">
                            <select class="weui-select VerificationFinally" name="VerificationFinally">
                                <option selected="" value="1">处理</option>
                                <option value="2">不处理</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="weui-cells__title">备注</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input VerificationOpinion" type="text" placeholder="请输入备注">
                        </div>
                    </div>
                </div>
                <center>
                <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_primary close-popup">关闭</a>
                <a onclick="submitflow(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_warn">流程处理</a>
                    </center>
            </div>




        </div>


        <%} %>
    </div>
</body>
</html>
