﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JydOrderFrom.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.JydOrderFrom" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>客户下单</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <script src="js/ajax_submit.js"></script>
    <link rel="stylesheet" href="dist/lib/weui.min.css">
    <link rel="stylesheet" href="dist/css/jquery-weui.css">
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
</head>
<body ontouchstart>

    <header class='demos-header'>
        <h1 class="demos-title">用户下单</h1>
    </header>
    <div class="weui-cells__title">公司名称</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="comname" name="comname" class="weui-input" type="text" placeholder="请输入公司名称">
            </div>
        </div>
    </div>



    <div class="weui-cells__title">联系人</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="connman" name="connman" class="weui-input" type="text" placeholder="请输入联系人">
            </div>
        </div>
    </div>

    <div class="weui-cell weui-cell_vcode">
        <div class="weui-cell__hd">
            <label class="weui-label">手机号</label>
        </div>
        <div class="weui-cell__bd">
            <input id="tel" name="tel" class="weui-input" type="tel" placeholder="请输入联系人手机号">
        </div>
    </div>





    <%--<div class="weui-cell">
            <div class="weui-cell__hd">
                <label for="" class="weui-label">选择接件日期</label>
            </div>
            <div class="weui-cell__bd">
                <input id="adddate" name="adddate" class="weui-input" type="date" value="">
            </div>
        </div>--%>

    <div class="weui-cells__title">收货人电话(选填)</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="dha" name="dha" class="weui-input" type="text" placeholder="请输入收货人电话">
            </div>
        </div>
    </div>

    <div class="weui-cells__title">常用微信电话(选填)</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="dhb" name="dhb" class="weui-input" type="text" placeholder="请输入常用联系微信电话">
            </div>
        </div>
    </div>

    <%--    <div class="weui-cell">
        <div class="weui-cell__hd"><label for="" class="weui-label">交货日期</label></div>
        <div class="weui-cell__bd">
          <input id="deliveryDate" name="deliveryDate" class="weui-input" type="date" value="">
        </div>
      </div>--%>
    <div class="weui-cells__title">印刷品名称</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="yspmc" name="yspmc" class="weui-input" type="text" placeholder="请输入印刷品名称">
            </div>
        </div>
    </div>

    <div class="weui-cell weui-cell_select">
        <div class="weui-cell__bd">
            <select class="weui-select" name="PrintType" id="PrintType">
                <option value="">请选择类型</option>
                <%=PrintTyy %>
            </select>
        </div>
    </div>

    <div class="weui-cells__title">单位</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="danwei_m" name="danwei_m" class="weui-input" type="text" placeholder="请输入物品单位">
            </div>
        </div>
    </div>

    <div class="weui-cells__title">成品尺寸</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="cpcc" name="cpcc" class="weui-input" type="text" placeholder="请输入成品尺寸">
            </div>
        </div>
    </div>

    <div class="weui-cells__title">纸张</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="zzf" name="zzf" class="weui-input" type="text" placeholder="请输入印刷纸张">
            </div>
        </div>
    </div>

    <div class="weui-cells__title">页码</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="ym" name="ym" class="weui-input" type="text" placeholder="请输入页码">
            </div>
        </div>
    </div>

    <div class="weui-cells__title">颜色</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="sj" name="sj" class="weui-input" type="text" placeholder="请输入印刷颜色">
            </div>
        </div>
    </div>

    <div class="weui-cell weui-cell_select">
        <div class="weui-cell__bd">
            <select class="weui-select" name="zidaihz" id="zidaihz">
                <option value="">请选择客户自带</option>
                <%=zidai %>
            </select>
        </div>
    </div>

    <div class="weui-cells__title">数量</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="sl" name="sl" class="weui-input" type="text" placeholder="请输入数量">
            </div>
        </div>
    </div>

    <div class="weui-cells__title">货发(选填)</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input id="huofa" name="huofa" class="weui-input" type="text" placeholder="请输入货发地址">
            </div>
        </div>
    </div>

    <div class="weui-cell weui-cell_select">
        <div class="weui-cell__bd">
            <select class="weui-select" name="ys" id="ys">
                <option value="">请选择运输物流</option>
                <%=yunshu %>
            </select>
        </div>
    </div>



    <div class="weui-cell weui-cell_select">
        <div class="weui-cell__bd">
            <select class="weui-select" name="fy" id="fy">
                <option value="">请选择费用分种</option>
                <%=feiyon %>
            </select>
        </div>
    </div>



    <div class="weui-cells__title">其他要求</div>
    <div class="weui-cells weui-cells_for">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <textarea class="weui-textarea" id="explain" name="explain" placeholder="请输入其他要求" rows="3"></textarea>
                <div class="weui-textarea-counter"><span>0</span>/200</div>
            </div>
        </div>
    </div>





    <div class="weui-btn-area">
        <input type="button" name="btnSubmit" value="提交保存" id="submit" class="weui-btn weui-btn_primary">
    </div>
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="../scripts/layui/layui.all.js"></script>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="dist/lib/fastclick.js"></script>
    <script src="dist/js/jquery-weui.js"></script>
    <script type="text/javascript">

        function showloading(t) {
            if (t) {//如果是true则显示loading
                console.log(t);
                loading = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });
            } else {//如果是false则关闭loading
                console.log("关闭loading层:" + t);
                layer.closeAll('loading');
            }
        }


        $("#submit").click(function () {

            var dh = $("#tel").val();
            var mymc = $("#comname").val();
            var myor = $("#yspmc").val();
            if (dh === null || dh === undefined || dh === '') {
                layer.msg('手机号不能为空!');
                return false;
            }
            if (mymc === null || mymc === undefined || mymc === '') {
                layer.msg('公司名称不能为空');
                return false;
            }
            if (myor === null || myor === undefined || myor === '') {
                layer.msg('请输入印刷品名称');
                return false;
            }



            //var tell = $('#tel').val();
            //    if (!tel || !/1[3|4|5|7|8]\d{9}/.test(tell)) $.toptip('请输入手机号');

            var KeyId = $('#KeyId').val();
            var yspmc = $('#yspmc').val();
            var adddate = $('adddate').val();
            var comname = $('#comname').val();
            var connman = $('#connman').val();
            var tel = $('#tel').val();
            var allyspmc = $('#allyspmc').val();
            var PrintType = $('#PrintType').val();
            var danwei_m = $('#danwei_m').val();
            var cpcc = $('#cpcc').val();
            var zzf = $('#zzf').val();
            var ym = $('#ym').val();
            var sj = $('#sj').val();
            var zidaihz = $('#zidaihz').val();
            var sl = $('#sl').val();
            var huofa = $('#huofa').val();
            var ys = $('#ys').val();
            var fy = $('#fy').val();
            var dha = $('#dha').val();
            var dhb = $('#dhb').val();
            var explain = $('#explain').val();

            document.activeElement.blur();//软键盘收起
            $.ajax({
                url: '/JydModleOrder/ashx/ajax_submit.ashx',
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'wapajaxti',
                    KeyId: KeyId,
                    comname: comname,
                    adddate: adddate,
                    connman: connman,
                    tel: tel,
                    yspmc: yspmc,
                    allyspmc: allyspmc,
                    PrintType: PrintType,
                    danwei_m: danwei_m,
                    cpcc: cpcc,
                    zzf: zzf,
                    ym: ym,
                    sj: sj,
                    zidaihz: zidaihz,
                    sl: sl,
                    huofa: huofa,
                    ys: ys,
                    fy: fy,
                    dha: dha,
                    dhb: dhb,
                    explain: explain
                },
                beforeSend: function () {
                    // Handle the beforeSend event
                    //loading层
                    showloading(true);
                },
                success: function (data, textStatus, jqXHR) {
                    showloading(false);
                    if (data.code === 1) {
                        layer.msg(data.msg);
                        window.location.reload();
                    } else {
                        layer.msg(data.err);
                    }
                },
                
            });
        });

        //单值获取


</script>
</body>
</html>
