﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lopjh.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.lopjh" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <title>我的待办</title>
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="../scripts/layui/layui.all.js"></script>
</head>
<body>
    <div class="weui-tab">
        <div class="weui-navbar">
            <a class="weui-navbar__item weui-bar__item--on" href="#tab1">待完成
            </a>
            <%--<a class="weui-navbar__item" href="#tab2">已完成
            </a>--%>
        </div>
        <div class="weui-tab__bd">
            <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
                <%foreach (DataRow dataRow in datatable.Rows)
                    {%>
                <% DataRow data = SqlEasy.ExecuteDataRow("select * from jydorder where Keyid='"+dataRow["orderkey"].ToString()+"'"); %>
                <div class="weui-form-preview">
                    <div class="weui-form-preview__hd">
                        <label class="weui-form-preview__label">你未完成</label>
                        <em class="weui-form-preview__value"><%=dataRow["ordername"].ToString() %></em>
                    </div>
                    <div class="weui-form-preview__bd">
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">执行步骤</label>
                            <span class="weui-form-preview__value"><%=dataRow["nodename"].ToString() %></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">时间</label>
                            <span class="weui-form-preview__value">交货:<%=data["deliveryDate"].ToString() %></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">备注</label>
                            <span class="weui-form-preview__value"><%=dataRow["completeofth"].ToString() %></span>
                        </div>
                    </div>
                    <div class="weui-form-preview__ft">
                        <a type="submit" class="weui-form-preview__btn weui-form-preview__btn_primary" href="/JydModleOrder/taskQRcode.aspx?KeyID=<%=dataRow["orderkey"].ToString() %>">操作</a>
                    </div>
                </div>
                <%} %>
            </div>
            <div id="tab2" class="weui-tab__bd-item">
               
            </div>

        </div>
    </div>
    <script src="dist/lib/jquery-2.1.4.js"></script>
    <script src="dist/lib/fastclick.js"></script>
    <script src="dist/js/jquery-weui.js"></script>
</body>
</html>
