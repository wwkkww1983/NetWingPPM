﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FlowList.aspx.cs" Inherits="NetWing.BPM.Admin.flow.FlowList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



    <!-- 工具栏按钮 -->
    <div id="toolbar">
        <%= base.BuildToolbar()%><a href="#" plain="true" class="easyui-linkbutton" title="模式">模式</a><input id="selectSwitch" class="easyui-switchbutton" data-options="onText:'多选',offText:'单选'">
        类型:<select id="type" name="type" class="easyui-combobox" style="width: 80px;">
            <option value="">全部</option>
            <option value="disposed">已处理流程</option>
            <option value="wait">待处理流程</option>
            <option value="my">我的流程</option>
        </select>
        <a id="a_getSearch" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
    </div>

    <!-- datagrid 列表 -->
    <table id="list"></table>


    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <script src="../scripts/layui/layui.all.js"></script>

    <!-- 引入js文件 -->
    <script src="js/FlowList.js"></script>
</asp:Content>
