﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetWing.Model;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using Omu.ValueInjecter;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Linq;
using NetWing.Common.EF;

namespace NetWing.BPM.Admin.flow
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //得到泛型列表
            // var dicList = DicDal.Instance.GetListBy(69);
            // var list = dicList as List<Dic> ?? dicList.ToList();

            // //枚举填充实体类
            // IEnumerable<FlowInstanceModel> f = DbUtils.GetWhere<FlowInstanceModel>(new { KeyId = 16 });
            // FlowInstanceModel m = new FlowInstanceModel();
            // m.InjectFrom(f);

            // //根据keyid 得到一个实体类
            // FlowInstanceModel nm = DbUtils.Get<FlowInstanceModel>(16);
            // //实体类ToString() 其实这里是重写了C#里ToString()的方法，这里其实是ToJson
            // string js = nm.ToString();
            // //统计表里有几条
            // int count = DbUtils.Count<FlowInstanceModel>();
            // //计算页数
            // int page = DbUtils.GetPageCount(20, count);

            // //存储过程分页
            // ProcCustomPage p = new ProcCustomPage();
            // p.KeyFields = "keyid";
            // p.OrderFields = "keyid desc";
            // p.PageIndex = 1;//当前页
            // p.PageSize = 20;//每页多少条
            // p.ShowFields = "*";
            // p.Sp_PagerName = "ProcCustomPage";//分页存储过程名称
            // p.TableName = "sys_users";

            // DataTable dt = DbUtils.GetPageWithSp(p, out int rec);


            // //linq
            // DbUtils.GetAll<MJUserModel>().Where(d => d.KeyId > 80);
            // IEnumerable<MJUserModel> u = DbUtils.GetAll<MJUserModel>();
            // var res = from r in u
            //           where r.fullname.Contains("朱") && r.sex.Contains("男")

            //           orderby r.KeyId ascending
            //           select r;
            // //注意res 是在u的基础上二次计算，效率并不高。适合于结果计算
            // //Response.Write(NetWing.Common.JSONhelper.ToJson(res));

            // //带obj where  
            // IEnumerable<MJUserModel> n= DbUtils.GetWhere<MJUserModel>(new { sex = "男",Address="昆明市" });
            // //Response.Write(NetWing.Common.JSONhelper.ToJson(n));
            // //linq没有直接关联连接字符串，而是如创建DataContext db=new DataContext("");则自动在web.config中查找
            // //这里介绍LINQ DataContext类，DataContext 是用来连接到数据库、从中检索对象以及将更改提交回数据库的主要渠道。使用 DataContext 时就像使用 ADO.NET SqlConnection 一样。
            // DataContext db = new DataContext(SqlEasy.connString);
            // Table<FlowInstanceModel> Customers = db.GetTable<FlowInstanceModel>();
            // var cs = from c in Customers
            //          where c.KeyId<999
            //          select c;
            // Response.Write(NetWing.Common.JSONhelper.ToJson(cs));
            //int ccc= Customers.Count(uk => uk.MakerList == "1" || uk.MakerList.Contains("9"));


            decimal a = -100;
            decimal b = -100;
            decimal c = a - b;
            Response.Write(c);















    }
    }
}