﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.CommonAPIs;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using NetWing.BPM.Core;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core.Model;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Bll;

namespace NetWing.Model
{
    public class sendWeixin
    {
        public static bool sendWX(sendWxContent c)
        {

            //为模版中的各属性赋值
            var templateData = new TemplateData()
            {
                first = new TemplateDataItem(c.first, "#FF0000"),
                keyword1 = new TemplateDataItem(c.keyword1, "#000000"),
                keyword2 = new TemplateDataItem(c.keyword2, "#000000"),
                keyword3 = new TemplateDataItem(c.keyword3, "#000000"),
                remark = new TemplateDataItem(c.remark, "#FF0000")
            };
            //因为系统执行到接近最后一个节点时，不抛出执行人信息，所以这里如果执行到接近最后一步，不提示通知
            if (string.IsNullOrEmpty(c.userids))
            {
                c.userids = "0";
            }

            string templateid = NetWing.Common.ConfigHelper.GetValue("templageid");//从web.config 获得模板ID
                                                                                   //通知所有员工就是openid不同
            DataTable dt = SqlEasy.ExecuteDataTable("select * from Sys_Users where openid<>'' and openid is not null and keyid in(0,"+c.userids+")");
            foreach (DataRow ndr in dt.Rows)
            {
                string r = "";//发送微信结果
                try
                {
                    string openid = ndr["openid"].ToString();
                    r = NetWing.BPM.Admin.weixin.wxhelper.sendTemplateMsg(openid, templateid, c.viewUrl, templateData);
                    #region 微信催单
                    LogModel logx = new LogModel();
                    logx.BusinessName = SysVisitor.Instance.cookiesUserId + "微信催单通知:" + c.first;
                    logx.OperationIp = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    logx.OperationTime = DateTime.Now;
                    logx.PrimaryKey = "";
                    logx.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                    logx.SqlText = "";
                    logx.TableName = "";
                    logx.note = r;
                    logx.OperationType = (int)OperationType.Other;
                    LogDal.Instance.Insert(logx);
                    #endregion
                }
                catch (Exception err)
                {
                    #region 微信催单失败
                    LogModel logx = new LogModel();
                    logx.BusinessName = SysVisitor.Instance.cookiesUserId + "微信催单通知:" + c.first;
                    logx.OperationIp = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    logx.OperationTime = DateTime.Now;
                    logx.PrimaryKey = "";
                    logx.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                    logx.SqlText = "";
                    logx.TableName = "";
                    logx.note =r+"用户:"+ ndr["truename"].ToString() + err.Message;
                    logx.OperationType = (int)OperationType.Other;
                    LogDal.Instance.Insert(logx);
                    #endregion
                    //throw; //抛出错误
                    continue;//错误也继续执行
                }
                
                
            }



            return true;
        }
    }

    /// <summary>
    /// 定义模版中的字段属性（需与微信模版中的一致）
    /// </summary>
    public class TemplateData
    {
        public TemplateDataItem first { get; set; }
        public TemplateDataItem keyword1 { get; set; }
        public TemplateDataItem keyword2 { get; set; }
        public TemplateDataItem keyword3 { get; set; }
        public TemplateDataItem keyword4 { get; set; }
        public TemplateDataItem keyword5 { get; set; }
        public TemplateDataItem keyword6 { get; set; }
        public TemplateDataItem keyword7 { get; set; }
        public TemplateDataItem keyword8 { get; set; }
        public TemplateDataItem keyword9 { get; set; }
        public TemplateDataItem keyword10 { get; set; }
        public TemplateDataItem remark { get; set; }
    }

    public class sendWxContent {
        /// <summary>
        /// 标题 如：朱光明您好，陈婷婷已完成烫金工作，您现在急需完成XX工作！
        /// </summary>
        public string first { get; set; }
        /// <summary>
        /// 订单编号:jyd-2018-10-09-2 印刷品明细
        /// </summary>
        public string keyword1 { get; set; }
        /// <summary>
        /// 客户名称：
        /// </summary>
        public string keyword2 { get; set; }
        /// <summary>
        /// 预约时间：现在的时间
        /// </summary>
        public string keyword3 { get; set; }
        /// <summary>
        /// 备注：内容自己去发挥
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 手机上微信查看时的网址
        /// </summary>
        public string viewUrl { get; set; }
        /// <summary>
        /// 用户ids 1,2,3,6,7等格式，如果只是一个id 系统在前面会自动加0，
        /// </summary>
        public string userids { get; set; }
    }
}