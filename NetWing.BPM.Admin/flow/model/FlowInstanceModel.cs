using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;
using System.Data.Linq.Mapping;

namespace NetWing.Model
{
    [TableName("FlowInstance")]
    [Description("工作流实例")]
    [Table(Name = "FlowInstance")]//为了兼容linq
    public class FlowInstanceModel
    {


        /// <summary>
        /// 主键Id
        /// </summary>
        [Description("主键Id")]
        [Column(IsPrimaryKey = true)]
        public int KeyId { get; set; }
        /// <summary>
        /// 流程实例模板Id
        /// </summary>
        [Column(Name = "InstanceSchemeId")]
        [Description("流程实例模板Id")]
        public int InstanceSchemeId { get; set; }
        /// <summary>
        /// 实例编号
        /// </summary>
        [Description("实例编号")]
        [Column(Name = "Code")]
        public string Code { get; set; }
        /// <summary>
        /// 自定义名称
        /// </summary>
        [Description("自定义名称")]
        [Column(Name = "CustomName")]
        public string CustomName { get; set; }
        /// <summary>
        /// 当前节点ID  注意是时间戳式ID 1539402749003  从gooflow生成过来
        /// </summary>
        [Description("当前节点ID")]
        [Column(Name = "ActivityId")]
        public string ActivityId { get; set; }
        /// <summary>
        /// 当前节点类型（0会签节点）
        /// </summary>
        [Description("当前节点类型（0会签节点）")]
        [Column(Name = "ActivityType")]
        public int ActivityType { get; set; }
        /// <summary>
        /// 当前节点名称
        /// </summary>
        [Description("当前节点名称")]
        [Column(Name = "ActivityName")]
        public string ActivityName { get; set; }
        /// <summary>
        /// 前一个ID 注意是时间戳式ID 1539402749003  从gooflow生成过来
        /// </summary>
        [Description("前一个ID")]
        [Column(Name = "PreviousId")]
        public string PreviousId { get; set; }
        /// <summary>
        /// 流程模板内容
        /// </summary>
        [Description("流程模板内容")]
        [Column(Name = "SchemeContent")]
        public string SchemeContent { get; set; }
        /// <summary>
        /// 流程模板ID
        /// </summary>
        [Description("流程模板ID")]
        [Column(Name = "SchemeId")]
        public int SchemeId { get; set; }
        /// <summary>
        /// 数据库名称
        /// </summary>
        [Description("数据库名称")]
        [Column(Name = "DbName")]
        public string DbName { get; set; }
        /// <summary>
        /// 表单数据
        /// </summary>
        [Description("表单数据")]
        [Column(Name = "FrmData")]
        public string FrmData { get; set; }
        /// <summary>
        /// 表单类型
        /// </summary>
        [Description("表单类型")]
        [Column(Name = "FrmType")]
        public int FrmType { get; set; }
        /// <summary>
        /// 表单中的控件属性描述
        /// </summary>
        [Description("表单中的控件属性描述")]
        [Column(Name = "FrmContentData")]
        public string FrmContentData { get; set; }
        /// <summary>
        /// 表单控件位置模板
        /// </summary>
        [Description("表单控件位置模板")]
        [Column(Name = "FrmContentParse")]
        public string FrmContentParse { get; set; }
        /// <summary>
        /// 表单ID
        /// </summary>
        [Description("表单ID")]
        [Column(Name = "FrmId")]
        public int FrmId { get; set; }
        /// <summary>
        /// 流程类型
        /// </summary>
        [Description("流程类型")]
        [Column(Name = "SchemeType")]
        public string SchemeType { get; set; }
        /// <summary>
        /// 有效标志
        /// </summary>
        [Description("有效标志")]
        [Column(Name = "Disabled")]
        public int Disabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Column(Name = "CreateDate")]
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Description("创建用户主键")]
        [Column(Name = "CreateUserId")]
        public int CreateUserId { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Description("创建用户")]
        [Column(Name = "CreateUserName")]
        public string CreateUserName { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        [Description("等级")]
        [Column(Name = "FlowLevel")]
        public int FlowLevel { get; set; }
        /// <summary>
        /// 实例备注
        /// </summary>
        [Description("实例备注")]
        [Column(Name = "Description")]
        public string Description { get; set; }
        /// <summary>
        /// 是否完成
        /// </summary>
        [Description("是否完成")]
        [Column(Name = "IsFinish")]
        public int IsFinish { get; set; }
        /// <summary>
        /// 执行人
        /// </summary>
        [Description("执行人")]
        [Column(Name = "MakerList")]
        public string MakerList { get; set; }

        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}