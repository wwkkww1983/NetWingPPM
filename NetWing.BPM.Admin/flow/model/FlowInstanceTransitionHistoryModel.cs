using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("FlowInstanceTransitionHistory")]
	[Description("工作流转记录")]
	public class FlowInstanceTransitionHistoryModel
	{
				/// <summary>
		/// 主键Id
		/// </summary>
		[Description("主键Id")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 实例Id
		/// </summary>
		[Description("实例Id")]
		public int InstanceId { get; set; }		
		/// <summary>
		/// 开始节点Id
		/// </summary>
		[Description("开始节点Id")]
		public string FromNodeId { get; set; }		
		/// <summary>
		/// 开始节点类型
		/// </summary>
		[Description("开始节点类型")]
		public int FromNodeType { get; set; }		
		/// <summary>
		/// 开始节点名称
		/// </summary>
		[Description("开始节点名称")]
		public string FromNodeName { get; set; }		
		/// <summary>
		/// 结束节点Id
		/// </summary>
		[Description("结束节点Id")]
		public string ToNodeId { get; set; }		
		/// <summary>
		/// 结束节点类型
		/// </summary>
		[Description("结束节点类型")]
		public int ToNodeType { get; set; }		
		/// <summary>
		/// 结束节点名称
		/// </summary>
		[Description("结束节点名称")]
		public string ToNodeName { get; set; }		
		/// <summary>
		/// 转化状态
		/// </summary>
		[Description("转化状态")]
		public int TransitionSate { get; set; }		
		/// <summary>
		/// 是否结束
		/// </summary>
		[Description("是否结束")]
		public int IsFinish { get; set; }		
		/// <summary>
		/// 转化时间
		/// </summary>
		[Description("转化时间")]
		public DateTime CreateDate { get; set; }		
		/// <summary>
		/// 操作人Id
		/// </summary>
		[Description("操作人Id")]
		public int CreateUserId { get; set; }		
		/// <summary>
		/// 操作人名称
		/// </summary>
		[Description("操作人名称")]
		public string CreateUserName { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}