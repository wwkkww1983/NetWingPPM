using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class FlowInstanceTransitionHistoryBll
    {
        public static FlowInstanceTransitionHistoryBll Instance
        {
            get { return SingletonProvider<FlowInstanceTransitionHistoryBll>.Instance; }
        }

        public int Add(FlowInstanceTransitionHistoryModel model)
        {
            return FlowInstanceTransitionHistoryDal.Instance.Insert(model);
        }

        public int Update(FlowInstanceTransitionHistoryModel model)
        {
            return FlowInstanceTransitionHistoryDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return FlowInstanceTransitionHistoryDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return FlowInstanceTransitionHistoryDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
