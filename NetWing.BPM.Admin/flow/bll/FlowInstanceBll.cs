using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class FlowInstanceBll
    {
        public static FlowInstanceBll Instance
        {
            get { return SingletonProvider<FlowInstanceBll>.Instance; }
        }

        public int Add(FlowInstanceModel model)
        {
            return FlowInstanceDal.Instance.Insert(model);
        }

        public int Update(FlowInstanceModel model)
        {
            return FlowInstanceDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return FlowInstanceDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return FlowInstanceDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
