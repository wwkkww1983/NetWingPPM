﻿using NetWing.BPM.Admin.JLDD.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace NetWing.BPM.Admin.JLDD.lib
{
    public class DDApiService
    {
        public static readonly DDApiService Instance = new DDApiService();

        public string CorpId { get; private set; }
        public string CorpSecret { get; private set; }
        public string AgentId { get; private set; }

        private DDApiService()
        {
            CorpId = Operation.GetDataString(85, "corpId");
            CorpSecret = Operation.GetDataString(85, "corpSecret");
            AgentId = Operation.GetDataString(85, "agentId");
        }
        /// <summary>
        /// 发送普通消息
        /// </summary>
        /// <param name="sender">消息发送者员工ID</param>
        /// <param name="cid">群消息或者个人聊天会话Id</param>
        /// <param name="msgtype">消息类型</param>
        /// <param name="text">消息内容</param>
        /// <param name="access_token">接口凭证</param>
        /// <returns></returns>
        public string pickConversation(string sender, string cid, string msgtype, string text, string access_token)
        {
            return DDHelper.pickConversation(sender, cid, msgtype, text, access_token);
        }

        /// <summary>
        /// 获取AccessToken
        /// 开发者在调用开放平台接口前需要通过CorpID和CorpSecret获取AccessToken。
        /// </summary>
        /// <returns></returns>
        public string GetAccessToken()
        {
            return DDHelper.GetAccessToken(CorpId, CorpSecret);
        }

        public string GetJsApiTicket(string accessToken)
        {
            return DDHelper.GetJsApiTicket(accessToken);
        }

        public string GetUserId(string accessToken, string code)
        {
            return DDHelper.GetUserId(accessToken, code);
        }

        public UserDetailInfo GetUserDetail(string accessToken, string userId)
        {
            return DDHelper.GetUserDetail(accessToken, userId);
        }

        public string GetUserDetailJson(string accessToken, string userId)
        {
            return DDHelper.GetUserDetailJson(accessToken, userId);
        }

        public UserDetailInfo GetUserDetailFromJson(string jsonString)
        {
            UserDetailInfo model = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDetailInfo>(jsonString);

            if (model != null)
            {
                if (model.errcode == 0)
                {
                    return model;
                }
            }
            return null;
        }
        public string GetSign(string ticket, string nonceStr, long timeStamp, string url)
        {
            String plain = string.Format("jsapi_ticket={0}&noncestr={1}&timestamp={2}&url={3}", ticket, nonceStr, timeStamp, url);

            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(plain);
                byte[] digest = SHA1.Create().ComputeHash(bytes);
                string digestBytesString = BitConverter.ToString(digest).Replace("-", "");
                return digestBytesString.ToLower();
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}