﻿using NetWing.Common.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NetWing.BPM.Admin.JLDD.lib
{
    public static class Operation
    {
        /// <summary>
        /// 生成随机数
        /// </summary>
        /// <returns></returns>
        public static string randString()
        {
            string str = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+";//75个字符
            Random r = new Random();
            string result = string.Empty;
            //生成一个8位长的随机字符，具体长度可以自己更改
            for (int i = 0; i < 8; i++)
            {
                int m = r.Next(0, 75);//这里下界是0，随机数可以取到，上界应该是75，因为随机数取不到上界，也就是最大74，符合我们的题意
                string s = str.Substring(m, 1);
                result += s;
            }
            return result;
        }

        public static string GetDataString(int _id, string _str)
        {
            DataTable dt = SqlEasy.ExecuteDataTable("select Remark from Sys_Dics where CategoryId=" + _id + " and Code='" + _str + "'");
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["Remark"].ToString();
            }
            else
            {
                return null;
            }
        }
    }
}