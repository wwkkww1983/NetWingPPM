﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JydExportExce.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.JydExportExce" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <title>汇总表导出excel</title>
    <meta charset="utf-8">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            {
                name: '导出Excel',
                callback: function () {
                    ajaxExcel();
                    return false;
                }
            },
            {
                name: '取消'
            }
        );
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
       <div id="print_content">
           <style type="text/css">
                .table {
                    width: 98%;
                    margin: 1%;
                }

                    .table tr th {
                        text-align: center;
                        font-size: 20px;
                        color: #000;
                        font-weight: bold;
                        height: 40px;
                        line-height: 40px;
                    }

                .tb_tr td {
                    text-align: center;
                    font-size: 18px;
                    color: #000;
                    height: 34px;
                    line-height: 34px;
                }

                .tb_tr01 td {
                    text-align: center;
                    font-size: 14px;
                    color: #000;
                    height: 30px;
                    line-height: 30px;
                }

                .tb_tr02 td {
                    text-align: left;
                    font-size: 14px;
                    color: #000;
                    height: 30px;
                    line-height: 30px;
                    text-indent: 20px;
                }

                .tb_tr03 td {
                    text-align: center;
                    font-size: 14px;
                    color: #000;
                    font-weight: bold;
                    height: 50px;
                    line-height: 50px;
                }

                .tb_tr04 td {
                    text-align: left;
                    font-size: 14px;
                    color: #000;
                    height: 30px;
                    line-height: 30px;
                }

                .tb_tr05 td {
                    text-align: center;
                    font-size: 14px;
                    color: #000;
                    height: 30px;
                    line-height: 30px;
                }

                    .tb_tr05 td.xiaoji_color {
                        color: #ff6a00;
                    }

                    .tb_tr05 td.heji_color {
                        color: #ff0000;
                    }
            </style>

       </div>
    </form>
</body>
</html>
