﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetWing.BPM.Admin.JLDD.model
{
    public class GetUserInfoModel
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        /// <summary>
        /// 员工在企业内的UserID
        /// </summary>
        public string userid { get; set; }
        /// <summary>
        /// 手机设备号,由钉钉在安装时随机产生
        /// </summary>
        public string deviceId { get; set; }
        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool is_sys { get; set; }
        /// <summary>
        /// 级别，0：非管理员 1：超级管理员（主管理员） 2：普通管理员（子管理员） 100：老板
        /// </summary>
        public int sys_level { get; set; }
    }
}