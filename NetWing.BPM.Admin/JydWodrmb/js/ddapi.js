﻿//toast弹窗
function msgtoast(icon, msg) {
    dd.device.notification.toast({
        icon: icon, //icon样式，有success和error，默认为空 0.0.2
        text: msg, //提示信息
        duration: 2, //显示持续时间，单位秒，默认按系统规范[android只有两种(<=2s >2s)]
        delay: 0, //延迟显示，单位秒，默认0
        onSuccess: function (result) {
            /*{}*/
        },
        onFail: function (err) { }
    })
}
//开始加载
function ddshowloader(msg) {
    dd.device.notification.showPreloader({
        text: msg, //loading显示的字符，空表示不显示文字
        showIcon: true, //是否显示icon，默认true
        onSuccess: function (result) {
            /*{}*/
        },
        onFail: function (err) { }
    })
}
//结束加载
function ddhideloader() {
    dd.device.notification.hidePreloader({
        onSuccess: function (result) {
            /*{}*/
        },
        onFail: function (err) { }
    })
}
//弹浮层
function ddmodel(image, bannerArray, title, content) {
    dd.device.notification.modal({
        image: image, // 标题图片地址
        banner: bannerArray,   // 图片地址列表
        title: title, //标题
        content: content, //文本内容
        buttonLabels: ["知道了"],// 最多两个按钮，至少有一个按钮。
        onSuccess: function (result) {
            //onSuccess将在点击button之后回调
            /*{
                buttonIndex: 0 //被点击按钮的索引值，Number，从0开始
            }*/
            setStorage('showbool', false);
        },
        onFail: function (err) { }
    })
}
//储存信息
function setStorage(key, value) {
    localStorage.setItem(key, value);
}
//获取信息
function getStorage(key) {
    return localStorage.getItem(key);
}
//删除信息
function delStorage(key) {
    localStorage.removeItem(key);
}
//获取当前时间，格式yyyy-MM-dd
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}
//获取当前时间,格式yyyy.MM.dd 星期天
function getNowDate() {
    var date = new Date();
    var seperator1 = ".";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var weekday = new Array(7);
    weekday[0] = "星期天";
    weekday[1] = "星期一";
    weekday[2] = "星期二";
    weekday[3] = "星期三";
    weekday[4] = "星期四";
    weekday[5] = "星期五";
    weekday[6] = "星期六";
    var currentdate = year + seperator1 + month + seperator1 + strDate + ' ' + weekday[date.getDay()];
    return currentdate;
}
//获取当前时间,格式HH:mm
function getNowTime() {
    var date = new Date();
    var strH = date.getHours();
    var strM = date.getMinutes();
    if (strH >= 0 && strH <= 9) {
        strH = "0" + strH;
    }
    if (strM >= 0 && strM <= 9) {
        strM = "0" + strM;
    }
    var currentdate = strH + ':' + strM;
    return currentdate;
}
//获取当前时间，格式yyyy-MM-dd HH:mm
function getNowFormatDateTime() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    var strH = date.getHours();
    var strM = date.getMinutes();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    if (strH >= 0 && strH <= 9) {
        strH = "0" + strH;
    }
    if (strM >= 0 && strM <= 9) {
        strM = "0" + strM;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate + ' ' + strH + ':' + strM;
    return currentdate;
}
//定义函数：构建要显示的时间日期字符串
function showTime() {
    //创建Date对象
    var today = new Date();
    //分别取出年、月、日、时、分、秒
    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    var day = today.getDate();
    var hours = today.getHours();
    var minutes = today.getMinutes();
    var seconds = today.getSeconds();
    //如果是单个数，则前面补0
    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    //构建要输出的字符串
    var str = hours + ":" + minutes + ":" + seconds;

    //获取对象
    var obj = document.getElementById("showtime");
    obj.innerHTML = str;
    //延时器
    window.setTimeout("showTime()", 1000);
}
//日期+时间选择器
function ddselectDate(idr, format, value) {
    dd.biz.util.datetimepicker({
        format: format,//'yyyy-MM-dd HH:mm',
        value: value,//'2015-04-17 08:00', //默认显示
        onSuccess: function (result) {
            //onSuccess将在点击完成之后回调
            /*{
                value: "2015-06-10 09:50"
            }
            */
            if (result.value < value) {
                msgtoast('error', '所选时间不能低于当前时间!');
            } else {
                $("#" + idr).val(result.value);
            }
            var stime = $("#starttime").val();
            var etime = $("#endtime").val();
            if (etime != "") {
                if (etime < stime) {
                    msgtoast('error', '结束时间不能小于开始时间!');
                } else {
                    stime = stime.replace(/-/g, "/");
                    etime = etime.replace(/-/g, "/");
                    var dates = new Date(stime);
                    var datee = new Date(etime);
                    var ms = datee.getTime() - dates.getTime();
                    $("#usetime").val(fomatFloat(ms / 1000 / 60 / 60, 1));
                }
            }
        },
        onFail: function (err) { }
    })
}
//日期选择器
function dddatepicker(idr, format, value) {
    dd.biz.util.datepicker({
        format: format,
        value: value, //默认显示日期
        onSuccess: function (result) {
            //onSuccess将在点击完成之后回调
            /*{
                value: "2015-02-10"
            }
            */
            $("#" + idr).val(result.value);
        },
        onFail: function (err) { }
    })
}
//保留n位小数并格式化输出（不足的部分补0）
function fomatFloat(value, n) {
    var f = Math.round(value * Math.pow(10, n)) / Math.pow(10, n);
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        s += '.';
    }
    for (var i = s.length - s.indexOf('.'); i <= n; i++) {
        s += "0";
    }
    return s;
}
//获取当前地理位置(单次定位)  开始工作
function ddGetMap(ysid, keyid) {
    dd.device.geolocation.get({
        targetAccuracy: 200,//期望定位精度半径（单位米）建议按照业务需求设置定位精度，推荐采用200m，可获得较好的精度和较短的响应时长；
        coordinate: 1,//1：获取高德坐标， 0：获取标准坐标；推荐使用高德坐标；标准坐标没有address字段
        withReGeocode: true,//是否需要带有逆地理编码信息；该功能需要网络请求，请更具自己的业务场景使用
        useCache: false, //默认是true，如果需要频繁获取地理位置，请设置false
        onSuccess: function (result) {
            /* 高德坐标 result 结构
            {
                longitude : Number,//经度
                latitude : Number,//纬度
                accuracy : Number,//实际的定位精度半径（单位米）
                address : String,//格式化地址，如：北京市朝阳区南磨房镇北京国家广告产业园区
                province : String,//省份，如：北京市
                city : String,//城市，直辖市会返回空
                district : String,//行政区，如：朝阳区
                road : String,//街道，如：西大望路甲12-2号楼
                netType : String,//当前设备网络类型，如：wifi、3g等
                operatorType : String,//当前设备使用移动运营商，如：CMCC等
                errorMessage : String,//对错误码的描述
                errorCode : Number,//错误码
                isWifiEnabled : Boolean,//仅Android支持，wifi设置是否开启，不保证已连接上
                isGpsEnabled : Boolean,//仅Android支持，gps设置是否开启，不保证已经连接上
                isFromMock : Boolean,//仅Android支持，定位返回的经纬度是否是模拟的结果
                provider : wifi|lbs|gps,//仅Android支持，我们使用的是混合定位，具体定位提供者有wifi/lbs/gps" 这三种
                isMobileEnabled : Boolean//仅Android支持，移动网络是设置是否开启，不保证已经连接上
            }
            */
            $.ajax({
                url: '/JLDD/dd_ajax.ashx?action=StartKq&kqtype=' + ysid,
                type: 'POST',
                data: {
                    keyid: keyid,
                    longs: result.longitude,
                    lats: result.latitude,
                    addresss: result.address
                },
                success: function (d) {
                    ddhideloader();
                    var dd = JSON.parse(d);
                    if (dd.code == 0) {
                        var strs = '';
                        if (ysid == "ksgz") {
                            $("#" + ysid).remove();
                            $('#sgzsj').html("开始工作时间&nbsp;&nbsp;" + dd.date);
                            $('#sgzdd').html(result.address);
                            $('#sgzsj').css('display', '');
                            $('#sgzdd').css('display', '');
                            var jsstr = '<div><div class="dktime" id="egzsj" style="display: none;"></div><div class="dkaddress" id="egzdd" style="display: none;"></div>';
                            jsstr = jsstr + '<div id="jsgz"><a href="javascript:Btnkqdk(2);">结束工作<br /><span id="showtime"></span></a></div>';
                            jsstr = jsstr + '<div class="uploadimg" style="width: 45px; height: 45px; background: #0ff" id="imgshow2">';
                            jsstr = jsstr + '<a href="javascript:Btnuploadimg(2);">';
                            jsstr = jsstr + '<img src="img/sc.png" width="100%" height="100%" alt="上传" /></a></div></div>';
                            $('#jsdkbox').append(jsstr);
                            showTime();
                        } else if (ysid == "jsgz") {
                            $("#" + ysid).remove();
                            $('#egzsj').html("结束工作时间&nbsp;&nbsp;" + dd.date);
                            $('#egzdd').html(result.address);
                            $('#egzsj').css('display', '');
                            $('#egzdd').css('display', '');
                        }
                        msgtoast('success', '打卡成功!');
                    } else {
                        msgtoast('error', '网络异常,请重新打卡!');
                    }
                },
                error: function (e) {
                    ddhideloader();
                    msgtoast('error', '网络异常!');
                }
            })
        },
        onFail: function (err) {
            ddhideloader();
            msgtoast('error', '网络异常,请重新加载此页!');
        }
    });
}
//上传图片（仅支持拍照上传）
function dduploadImg(keyid, ldname, num) {
    dd.device.geolocation.get({
        targetAccuracy: 200,//期望定位精度半径（单位米）建议按照业务需求设置定位精度，推荐采用200m，可获得较好的精度和较短的响应时长；
        coordinate: 1,//1：获取高德坐标， 0：获取标准坐标；推荐使用高德坐标；标准坐标没有address字段
        withReGeocode: true,//是否需要带有逆地理编码信息；该功能需要网络请求，请更具自己的业务场景使用
        useCache: false, //默认是true，如果需要频繁获取地理位置，请设置false
        onSuccess: function (result) {
            ddhideloader();
            dd.biz.util.uploadImageFromCamera({
                compression: true,//(是否压缩，默认为true)
                quality: 90, // 图片压缩质量, 
                resize: 90, // 图片缩放率
                stickers: {   // 水印信息
                    time: getNowTime(),
                    dateWeather: getNowDate(),
                    username: ldname,
                    address: result.address
                },
                onSuccess: function (result) {
                    $.ajax({
                        url: '/JLDD/dd_ajax.ashx?action=oploadImg&num=' + num,
                        type: 'POST',
                        data: {
                            keyid: keyid,
                            imgsrc: result[0]
                        },
                        success: function (d) {
                            var dd = JSON.parse(d);
                            if (dd.code == 0) {
                                var strimg = '<a href="javascript:Btnshowimg(\'' + dd.imgsrc + '\');"><img src=' + dd.imgsrc + ' width="100%" height="100%" alt="浏览" /></a>';
                                $('#imgshow' + num).html(strimg);
                                msgtoast('success', '照片上传成功!');
                            } else {
                                msgtoast('error', '照片上传失败,请重新拍照!');
                            }
                        },
                        error: function (e) {
                            msgtoast('error', '照片上传失败,请重新拍照!');
                        }
                    })

                },
                onFail: function (err) { }
            });
        },
        onFail: function (err) {
            ddhideloader();
            msgtoast('error', '网络异常,无法获得地址!');
        }
    });
}
//图片浏览器
function ddshowimg(imgsrc) {
    var imgArr = new Array(1);
    imgArr[0] = imgsrc;
    dd.biz.util.previewImage({
        urls: imgArr,//图片地址列表
        current: imgsrc,//当前显示的图片链接
        onSuccess: function (result) {
            /**/
        },
        onFail: function (err) { }
    })
}
//销毁当前页面替换新页面
function ddreplace(urlstr) {
    dd.biz.navigation.replace({
        url: nowUlr() + urlstr,// 新的页面链接
        onSuccess: function (result) {
            /*
            {}
            */
        },
        onFail: function (err) { }
    });
}
//新窗口打开
function ddopenLink(urlstr) {
    dd.biz.util.openLink({
        url: nowUlr() + urlstr,//要打开链接的地址
        onSuccess: function (result) {
            /**/
        },
        onFail: function (err) { }
    })
}
//获取当前url主机部分
function nowUlr() {
    var host = window.location.host;
    return 'http://' + host;
}
//设置标题
function setTitle(text) {
    dd.biz.navigation.setTitle({
        title: text,//控制标题文本，空字符串表示显示默认文本
        onSuccess: function (result) {
            /*结构
            {
            }*/
        },
        onFail: function (err) { }
    });
}
//身份证格式校验
var Wi = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];    // 加权因子     
var ValideCode = [1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2];            // 身份证验证位值.10代表X     
function IdCardValidate(idCard) {
    idCard = idCard.trim();               //去掉字符串头尾空格                       
    if (idCard.length == 15) {
        return isValidityBrithBy15IdCard(idCard);       //进行15位身份证的验证      
    } else if (idCard.length == 18) {
        var a_idCard = idCard.split("");                // 得到身份证数组     
        if (isValidityBrithBy18IdCard(idCard) && isTrueValidateCodeBy18IdCard(a_idCard)) {   //进行18位身份证的基本验证和第18位的验证  
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
/**   
 * 判断身份证号码为18位时最后的验证位是否正确   
 * @param a_idCard 身份证号码数组   
 * @return   
 */
function isTrueValidateCodeBy18IdCard(a_idCard) {
    var sum = 0;                             // 声明加权求和变量     
    if (a_idCard[17].toLowerCase() == 'x') {
        a_idCard[17] = 10;                    // 将最后位为x的验证码替换为10方便后续操作     
    }
    for (var i = 0; i < 17; i++) {
        sum += Wi[i] * a_idCard[i];            // 加权求和     
    }
    valCodePosition = sum % 11;                // 得到验证码所位置     
    if (a_idCard[17] == ValideCode[valCodePosition]) {
        return true;
    } else {
        return false;
    }
}
/**   
  * 验证18位数身份证号码中的生日是否是有效生日   
  * @param idCard 18位书身份证字符串   
  * @return   
  */
function isValidityBrithBy18IdCard(idCard18) {
    var year = idCard18.substring(6, 10);
    var month = idCard18.substring(10, 12);
    var day = idCard18.substring(12, 14);
    var temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
    // 这里用getFullYear()获取年份，避免千年虫问题     
    if (temp_date.getFullYear() != parseFloat(year)
        || temp_date.getMonth() != parseFloat(month) - 1
        || temp_date.getDate() != parseFloat(day)) {
        return false;
    } else {
        return true;
    }
}
/**   
 * 验证15位数身份证号码中的生日是否是有效生日   
 * @param idCard15 15位书身份证字符串   
 * @return   
 */
function isValidityBrithBy15IdCard(idCard15) {
    var year = idCard15.substring(6, 8);
    var month = idCard15.substring(8, 10);
    var day = idCard15.substring(10, 12);
    var temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
    // 对于老身份证中的你年龄则不需考虑千年虫问题而使用getYear()方法     
    if (temp_date.getYear() != parseFloat(year)
        || temp_date.getMonth() != parseFloat(month) - 1
        || temp_date.getDate() != parseFloat(day)) {
        return false;
    } else {
        return true;
    }
}

//获取会话信息
function ddpickConversation(cids, uid, keyid, nowulr) {
    dd.biz.chat.pickConversation({
        corpId: cids, //企业id
        isConfirm: 'true', //是否弹出确认窗口，默认为true
        onSuccess: function (result) {
            //onSuccess将在选择结束之后调用
            // 该cid和服务端开发文档-普通会话消息接口配合使用，而且只能使用一次，之后将失效
            /*{
                cid: 'xxxx',
                title:'xxx'
            }*/
            ddshowloader('发送中...');
            $.ajax({
                url: '/JLDD/dd_ajax.ashx?action=Tikqunhuihua',
                type: 'POST',
                data: {
                    cid: result.cid,
                    title: result.title,
                    uid: uid,
                    keyid: keyid,
                    nowulr: nowulr
                },
                success: function (d) {
                    ddhideloader();
                    var dd = JSON.parse(d);
                    msgtoast('success', "发送成功!");
                },
                error: function (e) {
                    ddhideloader();
                    msgtoast('error', dd.msg);
                }
            })
        },
        onFail: function () { }
    })
}
//设置左侧导航按钮
function ddsetleftios(sxurl) {
    dd.biz.navigation.setLeft({
        control: true,//是否控制点击事件，true 控制，false 不控制， 默认false
        text: '返回',//控制显示文本，空字符串表示显示默认文本
        onSuccess: function (result) {
            /*
            {}
            */
            //如果control为true，则onSuccess将在发生按钮点击事件被回调
            ddreplace(sxurl);
        },
        onFail: function (err) { }
    });
}
function ddsetleftand(sxurl) {
    document.addEventListener('backbutton', function (e) {
        // 在这里处理你的业务逻辑
        ddreplace(sxurl);
        e.preventDefault(); //backbutton事件的默认行为是回退历史记录，如果你想阻止默认的回退行为，那么可以通过preventDefault()实现
    });
}

