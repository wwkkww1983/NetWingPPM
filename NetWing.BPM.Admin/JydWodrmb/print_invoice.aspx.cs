﻿using NetWing.Bll;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetWing.BPM.Admin.JydWodrmb
{
    public partial class print_invoice : System.Web.UI.Page
    {
        public string comname;
        public string orkeyid;
        public string OrderID;
        public string money;
        public string tel;
        public string connman;
        public string ywy;
        public string jjy;
        public DateTime adddate;
        public DateTime deliveryDate;
        public DataTable kpxxdt;

        protected void Page_Load(object sender, EventArgs e)
        {
            string pid = Request["KeyId"];//接件id
            string dateii = Request["OrderID"];//订单编号
            

            #region 查询接件信息（根据接件id查询）
            string jjxxsql = "select * from jydOrder where KeyId=" + pid + " and OrderID='" + dateii + "' ";
            DataRow jjxxdr = SqlEasy.ExecuteDataRow(jjxxsql);

            if (!string.IsNullOrEmpty(jjxxdr.ToString()))
            {
                comname = jjxxdr["comname"].ToString();
                orkeyid = jjxxdr["KeyId"].ToString();
                OrderID = jjxxdr["OrderID"].ToString();
                money = jjxxdr["jexx"].ToString();
                tel = jjxxdr["tel"].ToString();
                connman = jjxxdr["connman"].ToString();
                ywy = jjxxdr["ywy"].ToString();
                jjy = jjxxdr["jjy"].ToString();
                adddate = DateTime.Parse(jjxxdr["adddate"].ToString());
                deliveryDate = DateTime.Parse(jjxxdr["deliveryDate"].ToString());

                string kpxxsssql = "select * from jydOrderDetail where orderid='"+ OrderID + "' ";
                kpxxdt = SqlEasy.ExecuteDataTable(kpxxsssql);

                
            }
            else
            {
                string StrScript;
                StrScript = ("<script language=javascript>");
                StrScript += ("alert('数据值不存在!');");
                StrScript += ("frameElement.api.close(); ");
                StrScript += ("</script>");
                System.Web.HttpContext.Current.Response.Write(StrScript);
            }

            #endregion

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string Invoice_no = this.txt_Invoice_no.Value;//发票号

            #region 新增开票信息
            JydInvoicetModel model = new JydInvoicetModel();
            model.OrderID = OrderID;
            model.order_keyid = int.Parse(orkeyid);
            model.money = decimal.Parse(money);
            model.Invoice_no = this.txt_Invoice_no.Value;
            model.Invoice_time = DateTime.Now;
            model.Corporate_name = comname;
            model.Invoicetype = "开票";

            int addtj = JydInvoicetBll.Instance.Add(model);

            if (addtj > 0)
            {
                string StrScript;
                StrScript = ("<script language=javascript>");
                StrScript += ("alert('开票成功!');");
                StrScript += ("//frameElement.api.close(); ");
                StrScript += ("</script>");
                System.Web.HttpContext.Current.Response.Write(StrScript);
            }
            else
            {
                string StrScript;
                StrScript = ("<script language=javascript>");
                StrScript += ("alert('开票失败!');");
                StrScript += ("//frameElement.api.close(); ");
                StrScript += ("</script>");
                System.Web.HttpContext.Current.Response.Write(StrScript);
            }

            #endregion
        }
    }
}