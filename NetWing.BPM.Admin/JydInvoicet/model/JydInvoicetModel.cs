using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydInvoicet")]
	[Description("开票信息")]
	public class JydInvoicetModel
	{
				/// <summary>
		/// 开票id
		/// </summary>
		[Description("开票id")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 订单号
		/// </summary>
		[Description("订单号")]
		public int order_keyid { get; set; }		
		/// <summary>
		/// 订单编号
		/// </summary>
		[Description("订单编号")]
		public string OrderID { get; set; }		
		/// <summary>
		/// 金额
		/// </summary>
		[Description("金额")]
		public decimal money { get; set; }		
		/// <summary>
		/// 发票号
		/// </summary>
		[Description("发票号")]
		public string Invoice_no { get; set; }		
		/// <summary>
		/// 开票日期
		/// </summary>
		[Description("开票日期")]
		public DateTime Invoice_time { get; set; }		
		/// <summary>
		/// 公司名称
		/// </summary>
		[Description("公司名称")]
		public string Corporate_name { get; set; }		
		/// <summary>
		/// 开票状态
		/// </summary>
		[Description("开票状态")]
		public string Invoicetype { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}