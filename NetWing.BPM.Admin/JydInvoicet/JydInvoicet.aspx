﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="JydInvoicet.aspx.cs" Inherits="NetWing.BPM.Admin.JydInvoicet.JydInvoicet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



<!-- 工具栏按钮 -->
    <div id="toolbar">
        <%= base.BuildToolbar()%>

        <a id="btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">打印</a>
        <a  href="#" plain="true" class="easyui-linkbutton" title="模式">模式</a>
        <input id="selectSwitch" class="easyui-switchbutton" data-options="onText:'多选',offText:'单选'">

        <a href="#" plain="true" class="easyui-linkbutton" title="发票号">发票号</a><input id="Invoice_no"  name="Invoice_no" class="easyui-textbox" style="width: 80px;" />
        <a href="#" plain="true" class="easyui-linkbutton" title="订单号">订单号</a><input id="OrderID" name="OrderID" class="easyui-textbox" style="width: 80px;" />
         日期：<input type="text" id="dtOpstart" style="width: 120px;" class="easyui-datetimebox" />
                至
            <input type="text" id="dtOpend" style="width: 120px;" class="easyui-datetimebox" />
        <a id="mysearch" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
    </div>

    <!-- datagrid 列表 -->
    <table id="list" ></table>  


	<!--Uploader-->
	<!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
	<!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>


    <!-- 引入js文件 -->
      <script src="js/JydInvoicet.js"></script>

    <!--打印-->
    <script src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
</asp:Content>



