using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydUserBll
    {
        public static JydUserBll Instance
        {
            get { return SingletonProvider<JydUserBll>.Instance; }
        }

        public int Add(JydUserModel model)
        {
            return JydUserDal.Instance.Insert(model);
        }

        public int Update(JydUserModel model)
        {
            return JydUserDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydUserDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydUserDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
