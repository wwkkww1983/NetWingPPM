公众号:
名称:聚源达彩印包装
微信号:kmjydcy
原始ID:gh_a7eb757c7c93
token:kmjydcy187
EncodingAESKey:cDACytvoUE9x8nCFhGhyWcfDf6cyoZmrpWmUJNmkZx0
appid:wxe9096e98f4f9e667
开发者密码(AppSecret):f09b808b524933b8902b22c9322390a5

//测试号
appID
wx9368c33a5a61eb59
appsecret
b1970f5c799e7cb7803f246a60c054a8
token:ynxtab





获得部门json  /sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}
	      或者/sys/ashx/userhandler.ashx?action=deps
得到用户信息  /sys/ashx/userhandler.ashx


银行账户脚本

/****** Object:  Table [dbo].[NetWingCRM_BankAccount]    Script Date: 05/17/2017 16:50:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[NetWingCRM_BankAccount](
	[KeyId] [int] IDENTITY(1,1) NOT NULL,
	[accountName] [varchar](50) NULL,
	[accountNamePinYin] [varchar](50) NULL,
	[accountBalance] [money] NULL,
	[accountBank] [varchar](50) NULL,
	[accountFullName] [varchar](100) NULL,
	[accountNo] [varchar](50) NULL,
	[accountNote] [varchar](255) NULL,
	[is_del] [varchar](10) NULL,
 CONSTRAINT [PK_NetWingCRM_BankAccount] PRIMARY KEY CLUSTERED 
(
	[KeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'KeyId'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户简名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户简名拼音' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountNamePinYin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountBalance'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开户银行' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountBank'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户全名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountFullName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountNo'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'accountNote'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'删除标记' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount', @level2type=N'COLUMN',@level2name=N'is_del'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'银行账户表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_BankAccount'
GO

ALTER TABLE [dbo].[NetWingCRM_BankAccount] ADD  CONSTRAINT [DF__NetWingCR__accou__7E6CC920]  DEFAULT ((0)) FOR [accountBalance]
GO

ALTER TABLE [dbo].[NetWingCRM_BankAccount] ADD  CONSTRAINT [DF_NetWingCRM_BankAccount_is_del]  DEFAULT ('No') FOR [is_del]
GO

费用科目脚本



/****** Object:  Table [dbo].[NetWingCRM_accountingSubjects]    Script Date: 05/17/2017 17:21:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[NetWingCRM_accountingSubjects](
	[KeyId] [int] IDENTITY(1,1) NOT NULL,
	[subjectType] [varchar](50) NULL,
	[subjectName] [varchar](50) NULL,
	[subjectNamePinyin] [varchar](50) NULL,
	[note] [varchar](200) NULL,
	[is_del] [varchar](50) NULL,
 CONSTRAINT [PK_NetWingCRM_accountingSubjects] PRIMARY KEY CLUSTERED 
(
	[KeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'科目编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_accountingSubjects', @level2type=N'COLUMN',@level2name=N'KeyId'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'科目类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_accountingSubjects', @level2type=N'COLUMN',@level2name=N'subjectType'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'科目名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_accountingSubjects', @level2type=N'COLUMN',@level2name=N'subjectName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'科目简拼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_accountingSubjects', @level2type=N'COLUMN',@level2name=N'subjectNamePinyin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_accountingSubjects', @level2type=N'COLUMN',@level2name=N'note'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'停用标记' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NetWingCRM_accountingSubjects', @level2type=N'COLUMN',@level2name=N'is_del'
GO


