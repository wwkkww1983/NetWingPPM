using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydOrderDetailSHBll
    {
        public static JydOrderDetailSHBll Instance
        {
            get { return SingletonProvider<JydOrderDetailSHBll>.Instance; }
        }

        public int Add(JydOrderDetailSHModel model)
        {
            return JydOrderDetailSHDal.Instance.Insert(model);
        }

        public int Update(JydOrderDetailSHModel model)
        {
            return JydOrderDetailSHDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydOrderDetailSHDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydOrderDetailSHDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
