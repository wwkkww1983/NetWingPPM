using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydOrderDetailSH")]
	[Description("送货表")]
	public class JydOrderDetailSHModel
	{
				/// <summary>
		/// 送货主ID
		/// </summary>
		[Description("送货主ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 印刷品名称
		/// </summary>
		[Description("印刷品名称")]
		public string yspmc { get; set; }		
		/// <summary>
		/// 类型
		/// </summary>
		[Description("类型")]
		public string PrintType { get; set; }		
		/// <summary>
		/// 总数量
		/// </summary>
		[Description("总数量")]
		public decimal sl { get; set; }		
		/// <summary>
		/// 单价
		/// </summary>
		[Description("单价")]
		public decimal dj { get; set; }		
		/// <summary>
		/// 金额
		/// </summary>
		[Description("金额")]
		public decimal je { get; set; }		
		/// <summary>
		/// 送货时间
		/// </summary>
		[Description("送货时间")]
		public DateTime songhuo { get; set; }		
		/// <summary>
		/// 送货数量
		/// </summary>
		[Description("送货数量")]
		public decimal songhuos { get; set; }
        /// <summary>
		/// 送货数量
		/// </summary>
		[Description("订单id")]
        public string orderid { get; set; }


        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}