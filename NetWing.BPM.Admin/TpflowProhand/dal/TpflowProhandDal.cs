using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class TpflowProhandDal : BaseRepository<TpflowProhandModel>
    {
        public static TpflowProhandDal Instance
        {
            get { return SingletonProvider<TpflowProhandDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(TpflowProhandModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}