using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJUse")]
	[Description("")]
	public class MJUseModel
	{
				/// <summary>
		/// ID
		/// </summary>
		[Description("ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 用户名
		/// </summary>
		[Description("用户名")]
		public string username { get; set; }		
		/// <summary>
		/// 手机号
		/// </summary>
		[Description("手机号")]
		public string mobile { get; set; }		
		/// <summary>
		/// 房间号
		/// </summary>
		[Description("房间号")]
		public string room_id { get; set; }		
		/// <summary>
		/// 领用物品登记
		/// </summary>
		[Description("领用物品登记")]
		public string ts_context { get; set; }		
		/// <summary>
		/// 部门
		/// </summary>
		[Description("部门")]
		public string dep { get; set; }		
		/// <summary>
		/// 部门编号
		/// </summary>
		[Description("部门编号")]
		public int depid { get; set; }		
		/// <summary>
		/// 使用状态
		/// </summary>
		[Description("使用状态")]
		public string sy_state { get; set; }		
		/// <summary>
		/// 操作员
		/// </summary>
		[Description("操作员")]
		public string cz_user { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
		/// <summary>
		/// 领用时间
		/// </summary>
		[Description("领用时间")]
		public DateTime lysj { get; set; }		
		/// <summary>
		/// 归还时间
		/// </summary>
		[Description("归还时间")]
		public DateTime ghsj { get; set; }		
		/// <summary>
		/// 系统时间
		/// </summary>
		[Description("系统时间")]
		public DateTime add_time { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}