using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJUseDal : BaseRepository<MJUseModel>
    {
        public static MJUseDal Instance
        {
            get { return SingletonProvider<MJUseDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJUseModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}