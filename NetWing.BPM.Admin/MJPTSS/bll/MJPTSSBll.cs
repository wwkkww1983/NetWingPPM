using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJPTSSBll
    {
        public static MJPTSSBll Instance
        {
            get { return SingletonProvider<MJPTSSBll>.Instance; }
        }

        public int Add(MJPTSSModel model)
        {
            return MJPTSSDal.Instance.Insert(model);
        }

        public int Update(MJPTSSModel model)
        {
            return MJPTSSDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJPTSSDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJPTSSDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
