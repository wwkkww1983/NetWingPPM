using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJKaoqing")]
	[Description("考勤管理")]
	public class MJKaoqingModel
	{
				/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 员工ID
		/// </summary>
		[Description("员工ID")]
		public int userid { get; set; }		
		/// <summary>
		/// 微信openid
		/// </summary>
		[Description("微信openid")]
		public string openid { get; set; }		
		/// <summary>
		/// 员工姓名
		/// </summary>
		[Description("员工姓名")]
		public string username { get; set; }		
		/// <summary>
		/// 上班打卡时间
		/// </summary>
		[Description("上班打卡时间")]
		public DateTime sb_time { get; set; }		
		/// <summary>
		/// 下班打卡时间
		/// </summary>
		[Description("下班打卡时间")]
		public DateTime xb_time { get; set; }		
		/// <summary>
		/// 部门权限
		/// </summary>
		[Description("部门权限")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}