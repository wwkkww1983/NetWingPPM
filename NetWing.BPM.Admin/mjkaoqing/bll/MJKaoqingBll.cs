using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJKaoqingBll
    {
        public static MJKaoqingBll Instance
        {
            get { return SingletonProvider<MJKaoqingBll>.Instance; }
        }

        public int Add(MJKaoqingModel model)
        {
            return MJKaoqingDal.Instance.Insert(model);
        }

        public int Update(MJKaoqingModel model)
        {
            return MJKaoqingDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJKaoqingDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJKaoqingDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
