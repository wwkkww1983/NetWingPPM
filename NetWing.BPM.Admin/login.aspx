﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="NetWing.BPM.Admin.login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=appName%></title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" type="text/css" href="css/icon.css" />
    <link href="scripts/easyui/themes/gray/easyui.css" rel="stylesheet" />
     <link href="css/common.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="scripts/easyui/jquery.easyui.min.js"></script>
    <script src="scripts/easyui/NetWing.EasyUI.Extensions.js"></script>
    <link href="scripts/qqmsg/msgbox.css" rel="stylesheet" />
    <script src="scripts/qqmsg/jQuery.qqmsg.js"></script>
    <style type="text/css">
        .login_top{ overflow:hidden; background:#F0F0F0 url('images/sql2008.png') no-repeat center 1px;
                    padding-top:80px; height:197px; width:460px; overflow-x:hidden;overflow-y:hidden; }
					
		#div1{ position:fixed; top:0; left:0; right:0; bottom:0; z-index:-1;}
		#div1 > img{height:100%; width:100%; border:0;}
        .window .window-body { overflow:hidden; }
    </style>
    <script type="text/javascript" src="ashx/globalHandler.ashx"></script>
    

</head>
    <body>
    <div id=div1><img src="/Upload/system/loginpic.png" /></div>
        <script type="text/javascript" src="Scripts/jQuery.Ajax.js"></script>
        
        <script src="scripts/Business/login.js?nguid=3"></script>
    </body>
</html>

