﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
namespace NetWing.Utility
{
    public class NetWingHelper  //abstract 抽象的 抽象类
    {
        //public NetWingHelper() { }
        #region  返回部门自动编码
        public static string returnAutoCode(int parentId, string depType)
        {
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT  Code FROM  Sys_Departments  WHERE (KeyId = " + parentId + ")");
            string newcode = "";
            if (dr.Read())
            {
                string parentCode = dr["code"].ToString();//得到父编码 
                dr.Close();
                //检查部门类型
                switch (depType)
                {
                    case "乡镇/街道": //如果是乡镇的情况
                        newcode = autoCode(parentId, parentCode, "乡镇/街道");
                        break;
                    case "村委会": //如果是乡镇的情况
                        newcode = autoCode(parentId, parentCode, "村委会");
                        break;
                    case "村小组": //如果是乡镇的情况
                        newcode = autoCode(parentId, parentCode, "村小组");
                        break;
                    default:
                        break;
                }

                return newcode;

            }
            else
            {
                dr.Close();//关闭数据库链接
                return "";
            }



        }
        #endregion 返回部门自动编码
        #region 根据类型自动编码
        public static string autoCode(int parentId, string parentCode, string depType)
        {
            string newcode = "";
            SqlDataReader xzdr = SqlEasy.ExecuteDataReader("SELECT top 1 Code FROM  Sys_Departments  WHERE remark='" + depType + "' and code<>'' and parentId=" + parentId + "  order by code desc  ");
            if (xzdr.Read())
            {
                string code = xzdr["code"].ToString();
                xzdr.Close();//关闭数据库链接
                code = code.Substring(code.Length - 2, 2); //取最后两位
                int Intcode = int.Parse(code); //code 转换为int
                if ((Intcode + 1) < 10)//假如小于10
                {
                    newcode = parentCode + "0" + (Intcode + 1).ToString();
                }
                else //假如大于10
                {
                    newcode = parentCode + (Intcode + 1).ToString();
                }


            }
            else
            {
                xzdr.Close();
                newcode = parentCode + "01";//乡镇级别没有的情况下加01
            }
            return newcode;
        }
        #endregion 根据类型自动返回编码
        #region 根据用户名返回所在部门
        public static string returnDep(string userName, string type)
        {
            string depName = "";
            string returnDepName = "";
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT u.UserName,d.DepartmentName,d.Remark,d.Code,d.KeyId,d.ParentId from Sys_Users as u,Sys_Departments as d  where u.DepartmentId=d.KeyId and u.UserName='" + userName + "'");
            if (dr.Read())
            {
                depName = dr["DepartmentName"].ToString() + "[" + dr["Remark"].ToString() + "]";
                dr.Close();
            }
            switch (type)
            {
                case "s":
                    returnDepName = depName;
                    break;
                default:
                    break;
            }

            return returnDepName;
        }

        #endregion 根据用户名返回所在部门
        #region 将DataTable转化为适合jquery easy ui 控件tree ,combotree 的 json


        /// <summary>
        /// 递归将DataTable转化为适合jquery easy ui 控件tree ,combotree 的 json
        /// 该方法最后还要 将结果稍微处理下,将最前面的,"children" 字符去掉.
        /// </summary>
        /// <param name="dt">要转化的表</param>
        /// <param name="pField">表中的父节点字段</param>
        /// <param name="pValue">表中顶层节点的值,没有 可以输入为0</param>
        /// <param name="kField">关键字字段名称</param>
        /// <param name="TextField">要显示的文本 对应的字段</param>
        /// <returns></returns>
        public static string TableToEasyUITreeJson(DataTable dt, string pField, string pValue, string kField, string TextField)
        {

            StringBuilder sb = new StringBuilder();
            string filter = String.Format(" {0}='{1}' ", pField, pValue);//获取顶级目录.
            DataRow[] drs = dt.Select(filter);
            if (drs.Length < 1)
                return "";
            sb.Append(",\"children\":[");
            foreach (DataRow dr in drs)
            {
                string pcv = dr[kField].ToString();
                sb.Append("{");
                //sb.AppendFormat("\"id\":\"{0}\",", dr[kField].ToString());
                sb.AppendFormat("\"id\":\"{0}\",", dr["code"].ToString());//我们自己的写法用code作为id
                sb.AppendFormat("\"text\":\"{0}\"", dr[TextField].ToString());
                sb.Append(TableToEasyUITreeJson(dt, pField, pcv, kField, TextField).TrimEnd(','));
                sb.Append("},");
            }
            if (sb.ToString().EndsWith(","))
            {
                sb.Remove(sb.Length - 1, 1);
            }
            sb.Append("]");
            return sb.ToString();
        }
        #endregion 将DataTable转化为适合jquery easy ui 控件tree ,combotree 的 json
        #region 返回家庭及成员自动编码
        /// <summary>
        /// 返回家庭及家庭成员自动编码
        /// </summary>
        /// <param name="VillageGroupCode">村小组编码</param>
        /// <param name="Type">类型编码 Home家庭编号 Person个人编码</param>
        /// <returns></returns>
        public static string returnHomeAutoCode(string VillageGroupCode, string Type)
        {
            string code = "";
            switch (Type)
            {
                case "Home"://家庭编号
                    SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT top 1  HomeNo FROM    NetWing_Home_Base where HomeNo like '" + VillageGroupCode + "%' ORDER BY HomeNo DESC");//查询数据库里的最大ID
                    if (dr.Read()) //如果有取最后一个后三位
                    {
                        string strHomeNo = dr["HomeNo"].ToString();
                        strHomeNo = strHomeNo.Substring(strHomeNo.Length - 3);//取后三个字符
                        int intHomeNo = int.Parse(strHomeNo);//转换为数字
                        intHomeNo = intHomeNo + 1;//加1
                        string outHomeNo = intHomeNo.ToString();
                        if (outHomeNo.Length == 1) //如果是1位则在前面加00
                        {
                            code = VillageGroupCode + "00" + outHomeNo;
                        }
                        if (outHomeNo.Length == 2)//如果是1位则在前面加00
                        {
                            code = VillageGroupCode + "0" + outHomeNo;
                        }
                        if (outHomeNo.Length > 2) //如果大于2则不管
                        {
                            code = VillageGroupCode + outHomeNo;
                        }
                    }
                    else
                    { //如果没有
                        code = VillageGroupCode + "001";
                    }
                    dr.Close();
                    break;
                default:
                    break;
            }

            return code;

        }
        #endregion 返回家庭及成员自动编码

        #region 根据家庭ID返回家庭编码
        /// <summary>
        /// 根据家庭ID返回家庭编码
        ///  从NetWing_Home_Base表获得
        /// </summary>
        /// <param name="HomeID">家庭数字ID</param>
        /// <returns>String</returns>
        public static string returnHomeCode(int HomeID)
        {
            string code = "";
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT     *  FROM         NetWing_Home_Base where KeyId=" + HomeID + "");
            if (dr.Read())
            {
                code = dr["HomeNo"].ToString();
            }
            dr.Close();
            return code;
        }


        #endregion 根据家庭ID返回家庭编码

        #region 根据家庭ID返回个人编码
        /// <summary>
        /// 根据家庭ID返回个人编码
        ///  从NetWing_Member_File_Info表获得
        ///  原理:
        ///  1.先根据homeid 从homebase 表得到homecode
        ///  2.再从表NetWing_Member_File_Info查询有没有编码，如果有，如果没有
        /// </summary>
        /// <param name="HomeID">家庭数字ID</param>
        /// <returns>String</returns>
        public static string returnPersonCode(int HomeID)
        {
            string code = "";
            string homecode = "";
            SqlDataReader drh = SqlEasy.ExecuteDataReader("SELECT top 1 homeno FROM  NetWing_Home_Base where keyid=" + HomeID + "");
            if (drh.Read())
            {
                homecode = drh["homeno"].ToString() ;
                drh.Close();
            }

            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT     PersonalNumber FROM         NetWing_Member_File_Info  where PersonalNumber like '" + homecode + "%' order BY PersonalNumber DESC");
            if (dr.Read()) //如果有
            {
                string strHomeNo = dr["PersonalNumber"].ToString();
                dr.Close();
                strHomeNo = strHomeNo.Substring(strHomeNo.Length - 2);//取后两个字符
                int intHomeNo = int.Parse(strHomeNo);//转换为数字
                intHomeNo = intHomeNo + 1;//加1
                string outHomeNo = intHomeNo.ToString();
                if (outHomeNo.Length == 1) //如果是1位则在前面加0
                {
                    code = homecode + "0" + outHomeNo;
                }
                if (outHomeNo.Length == 2)//如果是1位则在前面不加0
                {
                    code = homecode + outHomeNo;
                }
            }
            else //如果没有 从homebase表吧id读出来
            {
                code = homecode + "01";
            }
            
            return code;
        }

        #endregion 根据家庭ID返回个人编码


    }
}
