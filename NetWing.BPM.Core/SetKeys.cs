﻿namespace NetWing.BPM.Core
{
    public class SetKeys
    {
        /// <summary>
        /// 短信验证码
        /// </summary>
        public const string SESSION_SMS_CODE = "session_sms_code";
        /// <summary>
        /// 加密密钥
        /// </summary>
        public const string KEYS_CODE = "s8fy2cp0";
        /// <summary>
        /// 对外接口秘钥
        /// </summary>
        public const string OPEN_KEYS_CODE = "gw8h63x4";
    }
}
