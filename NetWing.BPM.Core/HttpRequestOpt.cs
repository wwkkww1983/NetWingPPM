﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace NetWing.BPM.Core
{
    /// <summary>
    /// Http请求辅助类
    /// </summary>
    public class HttpRequestOpt
    {
        /// <summary>
        /// Get方式获取url地址输出内容
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="encoding">返回内容编码方式，例如：Encoding.UTF8</param>
        /// <returns></returns>
        public static String SendRequestByGetMethod(String url, Encoding encoding)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "GET";
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            StreamReader sr = new StreamReader(webResponse.GetResponseStream(), encoding);
            return sr.ReadToEnd();
        }

        /// <summary>
        /// Get方式在指定时间内获取url地址输出内容
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="encoding">返回内容编码方式，例如：Encoding.UTF8</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public static String SendRequestByGetMethod(String url, Encoding encoding, int timeOut)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "GET";
            webRequest.Timeout = timeOut;
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            StreamReader sr = new StreamReader(webResponse.GetResponseStream(), encoding);
            return sr.ReadToEnd();
        }

        /// <summary>
        /// Post方式获取url地址输出内容
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="encoding">返回内容编码方式，例如：Encoding.UTF8</param>
        /// <param name="param">传递参数</param>
        /// <returns></returns>
        public static String SendRequestByPostMethod(String url, Encoding encoding, String param)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            byte[] bs = Encoding.ASCII.GetBytes(param);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = bs.Length;
            using (Stream reqStream = webRequest.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
                reqStream.Close();
            }
            using (HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    return reader.ReadToEnd().ToString();
                }
            }
        }

        /// <summary>
        /// Post方式再指定时间内容获取url地址输出内容
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="encoding">返回内容编码方式，例如：Encoding.UTF8</param>
        /// <param name="timeOut">超时时间</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public static String SendRequestByPostMethod(String url, Encoding encoding, String param, int timeOut)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            byte[] bs = Encoding.ASCII.GetBytes(param);
            webRequest.Method = "POST";
            webRequest.Timeout = timeOut;
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = bs.Length;
            using (Stream reqStream = webRequest.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
                reqStream.Close();
            }
            using (HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    return reader.ReadToEnd().ToString();
                }
            }
        }
    }
}
