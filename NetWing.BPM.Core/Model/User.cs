﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using NetWing.Common.Data;
using NetWing.Common;
using NetWing.BPM.Core.Dal;
using System.Collections;
using System.Data.Linq.Mapping;
namespace NetWing.BPM.Core.Model
{
    [TableName("sys_users")]
    [Description("系统用户")]
    [Table(Name = "sys_users")]//为了兼容linq
    public class User
    {
        [Column]
        public int KeyId { get; set; }
        [Description("用户名")]
        [Column]
        public string UserName { get; set; }
        [Description("密码")]
        [Column]
        public string Password { get; set; }
        [Description("真实姓名")]
        [Column]
        public string TrueName { get; set; }
        [Description("密码佐料")]
        [Column]
        public string PassSalt { get; set; }
        [Column]
        public string Email { get; set; }
        [Description("是否超管")]
        [Column]
        public bool IsAdmin { get; set; }
        [Description("是否禁用")]
        [Column]
        public bool IsDisabled { get; set; }
        [Description("部门Id")]
        [Column]
        public int DepartmentId { get; set; }
        [Description("手机")]
        [Column]
        public string Mobile { get; set; }
        [Description("应急手机")]
        [Column]
        public string otherMobile { get; set; }
        [Description("职务")]
        [Column]
        public string JobTitle{get;set;}
        [Description("短号")]
        [Column]
        public string ShortNo { get; set; }
        [Description("QQ")]
        [Column]
        public string QQ { get; set; }
        [Description("weixin")]
        [Column]
        public string weixin { get; set; }
        [Description("备注")]
        [Column]
        public string Remark { get; set; }
        [Description("个性化设置")]
        [Column]
        public string ConfigJson { get; set; }

        [DbField(false)]
        public Department Department
        {
            get { return DepartmentDal.Instance.Get(DepartmentId); }
        }

        [DbField(false)]
        public List<Navigation> Navigations { get; set; }

        [DbField(false)]
        public IEnumerable<Role> Roles
        {
            get { return UserDal.Instance.GetRolesBy(KeyId); }
        }

        /// <summary>
        /// 用户可以访问的部门列表
        /// </summary>
        [DbField(false)]
        public string Departments
        {
            get { return string.Join(",", UserDal.Instance.GetDepIDs(KeyId)); }
        } 

       
        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}
