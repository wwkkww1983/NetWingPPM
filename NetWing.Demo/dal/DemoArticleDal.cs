using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Demo.Model;
using NetWing.Model;

namespace NetWing.Demo.Dal
{
    public class DemoArticleDal : BaseRepository<DemoArticleModel>
    {
        public static DemoArticleDal Instance
        {
            get { return SingletonProvider<DemoArticleDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(DemoArticleModel)), pageindex, pagesize, filterJson,
                                                  sort, order);
        }
    }
}