using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NetWing.Common.Data;
using NetWing.Common.Provider;

using NetWing.Demo.Model;

namespace NetWing.Demo.Dal
{
    public class DemoUsersDal : BaseRepository<DemoUsersModel>
    {
        public static DemoUsersDal Instance
        {
            get { return SingletonProvider<DemoUsersDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(DemoUsersModel)), pageindex, pagesize, filterJson,
                                                  sort, order);
        }
    }
}