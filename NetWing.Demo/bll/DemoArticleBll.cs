using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Demo.Dal;
using NetWing.Demo.Model;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Demo.Bll
{
    public class DemoArticleBll
    {
        public static DemoArticleBll Instance
        {
            get { return SingletonProvider<DemoArticleBll>.Instance; }
        }

        public int Add(DemoArticleModel model)
        {
            return DemoArticleDal.Instance.Insert(model);
        }

        public int Update(DemoArticleModel model)
        {
            return DemoArticleDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return DemoArticleDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return DemoArticleDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
