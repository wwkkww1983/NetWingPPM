﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;


namespace NetWing.Common.SMS
{
    public class alidayusms
    {
        //protected internal siteconfig siteConfig;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="recnum">必须 接收手机号如13700605160多个用,号隔开</param>
        /// <param name="smstemplatecode">必须 短信模板编码如SMS_4999265</param>
        /// <param name="smsparam">必须 短信模板参数  json 格式"{\"code\":\"1234\",\"product\":\"云南便民\"}"</param>
        /// <param name="extend">可选公共回传参数</param>
        /// <param name="receivetype">可选 响应格式 json xml</param>
        /// <returns></returns>
        public string send(string recnum, string smstemplatecode, string smsparam, string extend = "123456", string receivetype = "json")
        {
            //siteConfig = new DTcms.BLL.siteconfig().loadConfig();
            // url 正式环境	http://gw.api.taobao.com/router/rest	https://eco.taobao.com/router/rest
            string appkey = "LTAIjD3lchlrTknP";
            string secret = "IGu4npcNfTnrjFKuOcD1pzPqCkovL";
            ITopClient client = new DefaultTopClient("http://gw.api.taobao.com/router/rest", appkey, secret, receivetype);//format	String	否	响应格式。默认为xml格式，可选值：xml，json。
            AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
            req.Extend = extend;//可选 公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，用户可以根据该会员ID识别是哪位会员使用了你的应用
            req.SmsType = "normal";//必须 短信类型，传入值请填写normal
            req.SmsFreeSignName = "米佳";//必须 短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名。如“阿里大鱼”已在短信签名管理中通过审核，则可传入”阿里大鱼“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大鱼】欢迎使用阿里大鱼服务。
            req.SmsParam = smsparam;//可选 短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234","product":"alidayu"}
            req.RecNum = recnum;// 必须 短信接收号码。支持单个或多个手机号码，传入号码为11位手机号码，不能加0或+86。群发短信需传入多个号码，以英文逗号分隔，一次调用最多传入200个号码。示例：18600000000,13911111111,13322222222
            req.SmsTemplateCode = smstemplatecode;// 必须 短信模板ID，传入的模板必须是在阿里大鱼“管理中心-短信模板管理”中的可用模板。示例：SMS_585014
            AlibabaAliqinFcSmsNumSendResponse rsp = client.Execute(req);
            //Console.WriteLine(rsp.Body);
            return rsp.Body;
        }
    }
}
