# NetWingPPM

#### 项目介绍
印刷厂管理系统

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明
##### 假如报错，请用NuGet安装
1. NetWing.Common NuGet安装 ValueInjecter 2.3.3
2. NetWing.BPM.Admin 项目NuGet安装 Senparc.Weixin6.1.2
3. NetWing.Common  NuGet安装NVelocity1.1.1
4. NetWing.Common  NuGet安装 NPOI2.3.3
5. NetWing.Common  NuGet安装 System.Data.SQLite1.0.109.2
6. NetWing.Common  NuGet安装Microsoft.International.Converters.PinYinConverter1.0
7. NuGet安装 ZXing.Net0.16.4
8. NuGet安装 SharpZipLib1.0


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)